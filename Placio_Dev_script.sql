USE [master]
GO
/****** Object:  Database [Placio_Dev]    Script Date: 10/23/2018 5:22:31 PM ******/
CREATE DATABASE [Placio_Dev]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Placio_Dev', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Placio_Dev.mdf' , SIZE = 6144KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Placio_Dev_log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Placio_Dev_log.ldf' , SIZE = 164672KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Placio_Dev] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Placio_Dev].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Placio_Dev] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Placio_Dev] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Placio_Dev] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Placio_Dev] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Placio_Dev] SET ARITHABORT OFF 
GO
ALTER DATABASE [Placio_Dev] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Placio_Dev] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Placio_Dev] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Placio_Dev] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Placio_Dev] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Placio_Dev] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Placio_Dev] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Placio_Dev] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Placio_Dev] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Placio_Dev] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Placio_Dev] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Placio_Dev] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Placio_Dev] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Placio_Dev] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Placio_Dev] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Placio_Dev] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Placio_Dev] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Placio_Dev] SET RECOVERY FULL 
GO
ALTER DATABASE [Placio_Dev] SET  MULTI_USER 
GO
ALTER DATABASE [Placio_Dev] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Placio_Dev] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Placio_Dev] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Placio_Dev] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [Placio_Dev]
GO
/****** Object:  User [SHASTER\1519]    Script Date: 10/23/2018 5:22:31 PM ******/
CREATE USER [SHASTER\1519] FOR LOGIN [SHASTER\1519] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[address_id]    Script Date: 10/23/2018 5:22:31 PM ******/
CREATE SEQUENCE [dbo].[address_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[address_type_code]    Script Date: 10/23/2018 5:22:31 PM ******/
CREATE SEQUENCE [dbo].[address_type_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[age_category_code]    Script Date: 10/23/2018 5:22:31 PM ******/
CREATE SEQUENCE [dbo].[age_category_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[agent_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[agent_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[amenity_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[amenity_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[AQ_Bed_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[AQ_Bed_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[AQ_Diesel_Generator_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[AQ_Diesel_Generator_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[AQ_Lift_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[AQ_Lift_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[AQ_Owner_Detail_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[AQ_Owner_Detail_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[AQ_Property_Block_Floor_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[AQ_Property_Block_Floor_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[AQ_Property_Block_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[AQ_Property_Block_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[AQ_Property_Floor_Room_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[AQ_Property_Floor_Room_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[AQ_Property_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[AQ_Property_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[canteen_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[canteen_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[company_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[company_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[contactus_user_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[contactus_user_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[department_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[department_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Diesel_Generator_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Diesel_Generator_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[document_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[document_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[document_type_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[document_type_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Employee_Address_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Employee_Address_ID] 
 AS [int]
 START WITH 100
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[employee_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[employee_id] 
 AS [int]
 START WITH 100
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[facility_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[facility_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[financial_category_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[financial_category_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[followup_action_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[followup_action_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[item_subcategory_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[item_subcategory_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[job_title_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[job_title_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Lift_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Lift_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Owner_Detail_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Owner_Detail_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[price_categories_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[price_categories_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[price_categories_codeproperty_status_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[price_categories_codeproperty_status_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Property_Address_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Property_Address_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Property_Block_Floor_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Property_Block_Floor_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Property_Block_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Property_Block_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Property_Detail_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Property_Detail_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Property_Floor_Room_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Property_Floor_Room_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Property_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Property_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Property_Owner_Address_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Property_Owner_Address_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[property_owner_bank_det_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[property_owner_bank_det_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Property_Owner_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Property_Owner_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[property_status_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[property_status_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[property_type_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[property_type_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[qualification_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[qualification_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[readiness_for_procure_category_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[readiness_for_procure_category_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[readiness_for_procure_checklist_item_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[readiness_for_procure_checklist_item_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[readiness_for_procure_percent_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[readiness_for_procure_percent_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[relationship_type_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[relationship_type_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[restaurant_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[restaurant_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[room_type_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[room_type_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[staff_address_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[staff_address_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[staff_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[staff_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[staff_job_title_id]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[staff_job_title_id] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[star_rating_code]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[star_rating_code] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[Supplier_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[Supplier_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
USE [Placio_Dev]
GO
/****** Object:  Sequence [dbo].[USER_ID]    Script Date: 10/23/2018 5:22:32 PM ******/
CREATE SEQUENCE [dbo].[USER_ID] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO
/****** Object:  UserDefinedFunction [dbo].[USP_SplitString]    Script Date: 10/23/2018 5:22:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[USP_SplitString]
(    
      @Input NVARCHAR(MAX),
      @Character CHAR(1)
)
RETURNS @Output TABLE (
      Item NVARCHAR(1000)
)
AS
BEGIN
      DECLARE @StartIndex INT, @EndIndex INT
 
      SET @StartIndex = 1
      IF SUBSTRING(@Input, LEN(@Input) - 1, LEN(@Input)) <> @Character
      BEGIN
            SET @Input = @Input + @Character
      END
 
      WHILE CHARINDEX(@Character, @Input) > 0
      BEGIN
            SET @EndIndex = CHARINDEX(@Character, @Input)
           
            INSERT INTO @Output(Item)
            SELECT SUBSTRING(@Input, @StartIndex, @EndIndex - 1)
           
            SET @Input = SUBSTRING(@Input, @EndIndex + 1, LEN(@Input))
      END
 
	UPDATE @Output SET Item=LTRIM(RTRIM(Item))
		
      RETURN
END
GO
/****** Object:  Table [dbo].[Address_Types]    Script Date: 10/23/2018 5:22:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address_Types](
	[address_type_code] [nchar](15) NOT NULL,
	[address_type_desc] [nvarchar](50) NULL,
 CONSTRAINT [PK_Address_Types] PRIMARY KEY CLUSTERED 
(
	[address_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Addresses]    Script Date: 10/23/2018 5:22:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Addresses](
	[address_id] [int] NOT NULL,
	[address] [nvarchar](1000) NOT NULL,
	[city] [nvarchar](50) NULL,
	[state] [nvarchar](50) NULL,
	[country] [nvarchar](50) NULL,
	[pincode] [nvarchar](50) NULL,
	[email] [nvarchar](100) NULL,
	[mobile_no] [nvarchar](50) NULL,
	[other_address_details] [nvarchar](250) NULL,
 CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED 
(
	[address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Age_Categories]    Script Date: 10/23/2018 5:22:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Age_Categories](
	[age_category_code] [nchar](15) NOT NULL,
	[age_category_desc] [nvarchar](150) NULL,
 CONSTRAINT [PK_Age_Categories] PRIMARY KEY CLUSTERED 
(
	[age_category_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Amenities]    Script Date: 10/23/2018 5:22:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Amenities](
	[amenity_id] [int] NOT NULL,
	[parent_id] [int] NULL,
	[amenity_name] [nvarchar](200) NOT NULL,
	[amenity_type] [nvarchar](50) NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_Amenities] PRIMARY KEY CLUSTERED 
(
	[amenity_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property]    Script Date: 10/23/2018 5:22:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property](
	[Property_ID] [int] NOT NULL,
	[Property_Name] [varchar](200) NULL,
	[Property_Address] [varchar](2000) NULL,
	[No_of_Floors] [smallint] NULL,
	[Location_Score] [smallint] NULL,
	[Comment_on_Location_Score] [varchar](200) NULL,
	[Floor_Beds] [smallint] NULL,
	[Room_Beds] [smallint] NULL,
	[Room_Size] [smallint] NULL,
	[Room_Quality_Score_ID] [smallint] NULL,
	[Room_Cleanliness_Score_ID] [smallint] NULL,
	[Water_Source_ID] [smallint] NULL,
	[Water_Storage_Capacity_ID] [smallint] NULL,
	[Emergency_Staircase] [bit] NULL,
	[Lift_ID] [smallint] NULL,
	[Diesel_Generator_ID] [smallint] NULL,
	[Common_Area_Cleanliness_ID] [smallint] NULL,
	[Parking_Availability] [bit] NULL,
	[Parking_Vehicle_Count] [smallint] NULL,
	[House_Keeping_staff_Rooms] [smallint] NULL,
	[Age_of_the_Property] [smallint] NULL,
	[Owner_Detail_ID] [smallint] NULL,
	[longitute] [nvarchar](100) NULL,
	[latitute] [nvarchar](100) NULL,
	[IsCompetatorProperty] [bit] NULL,
	[IsNewProperty] [bit] NULL,
	[Gender] [smallint] NULL,
	[Rent_Deposit_ID] [smallint] NULL,
	[No_of_Beds] [smallint] NULL,
	[Property_Occupancy_Estimate] [varchar](20) NULL,
	[Room_wise_Power_Meter_ID] [bit] NULL,
	[Room_Cleanliness_ID] [smallint] NULL,
	[Reception_Area_yn] [bit] NULL,
	[Wifi_Reception] [smallint] NULL,
	[Security_Feature_IDs] [varchar](20) NULL,
	[Fire_Fighting_Equipment_IDs] [varchar](20) NULL,
	[ISP_Provider_Name] [varchar](50) NULL,
	[IsCanteenExists] [bit] NULL,
	[Canteen_Cleanliness_ID] [smallint] NULL,
	[Food_Availability_ID] [smallint] NULL,
	[Is_Exisiting_Hostel] [bit] NULL,
	[Food_Score] [smallint] NULL,
	[Operator_Number] [varchar](100) NULL,
	[Available_Placio] [bit] NULL,
	[Property_Alias_Name] [varchar](200) NULL,
	[IsCheckListDone] [bit] NULL,
	[CheckListApprovedDate] [datetime] NULL,
	[Property_Amenities_ID] [nvarchar](2000) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](100) NULL,
	[Pincode] [varchar](10) NULL,
	[Published_yn] [bit] NOT NULL,
	[Published_on] [date] NULL,
	[Published_by] [int] NULL,
	[Placio_Property_ID] [int] NULL,
 CONSTRAINT [PK_AQ_PROPERTY] PRIMARY KEY CLUSTERED 
(
	[Property_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Block]    Script Date: 10/23/2018 5:22:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Block](
	[property_block_id] [int] NOT NULL,
	[property_id] [int] NOT NULL,
	[block_name] [nvarchar](300) NULL,
	[no_of_rooms] [int] NULL,
 CONSTRAINT [PK_AQ_Property_block] PRIMARY KEY CLUSTERED 
(
	[property_block_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Block_Floor]    Script Date: 10/23/2018 5:22:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Block_Floor](
	[Property_block_floor_id] [int] NOT NULL,
	[property_id] [int] NULL,
	[property_block_id] [int] NULL,
	[floor_id] [int] NULL,
	[floor_name] [nvarchar](200) NOT NULL,
	[no_of_rooms] [int] NULL,
 CONSTRAINT [PK_AQ_Property_Block_floor] PRIMARY KEY CLUSTERED 
(
	[Property_block_floor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Canteen_Photos]    Script Date: 10/23/2018 5:22:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Canteen_Photos](
	[Property_Canteen_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[PhotoLocation] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_AQ_Property_Canteen_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Canteen_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Common_Area_Cleanliness_Photos]    Script Date: 10/23/2018 5:22:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Common_Area_Cleanliness_Photos](
	[Property_Common_Area_Cleanliness_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[PhotoLocation] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_AQ_Property_Common_Area_Cleanliness_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Common_Area_Cleanliness_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Diesel_Generator]    Script Date: 10/23/2018 5:22:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Diesel_Generator](
	[Diesel_Generator_ID] [int] NOT NULL,
	[Property_ID] [int] NULL,
	[Age] [smallint] NULL,
	[Brand] [varchar](20) NULL,
	[Warranty] [varchar](100) NULL,
	[AMC_Cost] [decimal](15, 2) NULL,
	[storage_capacity] [varchar](20) NULL,
 CONSTRAINT [PK_AQ_Property_Diesel_Generator] PRIMARY KEY CLUSTERED 
(
	[Diesel_Generator_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Diesel_Generator_Photos]    Script Date: 10/23/2018 5:22:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Diesel_Generator_Photos](
	[Property_Diesel_Generator_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[Diesel_Generator_ID] [int] NULL,
	[PhotoLocation] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_AQ_Property_Diesel_Generator_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Diesel_Generator_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Floor_Room_Bed]    Script Date: 10/23/2018 5:22:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Floor_Room_Bed](
	[bed_id] [int] NOT NULL,
	[property_id] [int] NOT NULL,
	[block_id] [int] NULL,
	[floor_id] [int] NOT NULL,
	[room_no] [nvarchar](100) NOT NULL,
	[bed_no] [nvarchar](100) NOT NULL,
	[price] [int] NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_AQ_property_floor_room_bed] PRIMARY KEY CLUSTERED 
(
	[bed_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Floor_Rooms]    Script Date: 10/23/2018 5:22:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Floor_Rooms](
	[property_floor_room_id] [int] NOT NULL,
	[property_id] [int] NOT NULL,
	[property_block_id] [int] NULL,
	[property_block_floor_id] [int] NULL,
	[room_no] [nvarchar](100) NOT NULL,
	[room_name] [nvarchar](100) NULL,
	[no_of_beds] [int] NULL,
 CONSTRAINT [PK_AQ_Property_Floor_Rooms] PRIMARY KEY CLUSTERED 
(
	[room_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_House_Keeping_staff_Rooms_Photos]    Script Date: 10/23/2018 5:22:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_House_Keeping_staff_Rooms_Photos](
	[Property_House_Keeping_staff_Rooms_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[PhotoLocation] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_AQ_Property_House_Keeping_staff_Rooms_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_House_Keeping_staff_Rooms_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Lift_Inputs]    Script Date: 10/23/2018 5:22:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Lift_Inputs](
	[Lift_ID] [int] NOT NULL,
	[Property_ID] [int] NULL,
	[Age] [smallint] NULL,
	[Brand] [varchar](20) NULL,
	[Warranty] [varchar](100) NULL,
	[AMC_Cost] [decimal](15, 2) NULL,
 CONSTRAINT [PK_AQ_Property_Lift_Inputs] PRIMARY KEY CLUSTERED 
(
	[Lift_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Lift_Photos]    Script Date: 10/23/2018 5:22:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Lift_Photos](
	[Property_Lift_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[Lift_ID] [int] NULL,
	[PhotoLocation] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_AQ_Property_Lift_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Lift_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Owner_Details]    Script Date: 10/23/2018 5:22:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Owner_Details](
	[Owner_Detail_ID] [int] NOT NULL,
	[Property_ID] [int] NULL,
	[Owner_Name] [varchar](100) NULL,
	[Phone_Number] [varchar](20) NULL,
	[Is_owner_Operated] [bit] NULL,
 CONSTRAINT [PK_AQ_Property_Owner_Details] PRIMARY KEY CLUSTERED 
(
	[Owner_Detail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Photos]    Script Date: 10/23/2018 5:22:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Photos](
	[Property_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[PhotoLocation] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_AQ_Property_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Readiness_Checklist]    Script Date: 10/23/2018 5:22:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Readiness_Checklist](
	[Property_ID] [int] NOT NULL,
	[VisibilityonGoogle] [bit] NULL,
	[CCTVInstallation] [bit] NULL,
	[HouseKeepingStaff] [bit] NULL,
	[Security] [bit] NULL,
	[Foodpatner] [bit] NULL,
	[ServiceProviders] [bit] NULL,
	[BiometricInstallation] [bit] NULL,
	[receptionistInduction] [bit] NULL,
	[DetailsOfTieupAndBranding] [varchar](20) NULL,
	[Internet] [bit] NULL,
	[wifi] [bit] NULL,
	[DTH] [bit] NULL,
	[WaterCooler] [bit] NULL,
	[WaterPurificationPlant] [bit] NULL,
	[DrinkingWaterSupply] [bit] NULL,
	[BTLActivities] [varchar](20) NULL,
	[PhotoShoot] [varchar](20) NULL,
	[KioskBranding] [bit] NULL,
	[Landscaping] [bit] NULL,
	[Comments] [varchar](1000) NULL,
	[UserId] [varchar](20) NULL,
	[Createddate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Room_Bed_Photos]    Script Date: 10/23/2018 5:22:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Room_Bed_Photos](
	[Property_Room_Bed_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[PhotoLocation] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_AQ_Property_Room_Bed_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Room_Bed_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Room_Photos]    Script Date: 10/23/2018 5:22:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Room_Photos](
	[Property_Room_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[PhotoLocation] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_AQ_Property_Room_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Room_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AQ_Property_Security_Features]    Script Date: 10/23/2018 5:22:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AQ_Property_Security_Features](
	[Property_Security_Feature_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[Security_Feature_ID] [int] NOT NULL,
 CONSTRAINT [PK_AQ_Property_Security_Features] PRIMARY KEY CLUSTERED 
(
	[Property_Security_Feature_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Booking_Status]    Script Date: 10/23/2018 5:22:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Booking_Status](
	[booking_status_code] [nchar](15) NOT NULL,
	[booking_status_desc] [nvarchar](255) NULL,
 CONSTRAINT [PK_Booking_Status] PRIMARY KEY CLUSTERED 
(
	[booking_status_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Brands]    Script Date: 10/23/2018 5:22:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Brands](
	[brand_id] [int] NOT NULL,
	[brand_short_name] [nvarchar](20) NOT NULL,
	[brand_full_name] [nvarchar](100) NULL,
 CONSTRAINT [PK_Brands] PRIMARY KEY CLUSTERED 
(
	[brand_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Property]    Script Date: 10/23/2018 5:22:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Property](
	[C_Property_ID] [int] NOT NULL,
	[C_Property_Name] [varchar](200) NULL,
	[C_Property_Alias_Name] [varchar](200) NULL,
	[C_Property_Address] [varchar](2000) NULL,
	[C_Gender] [smallint] NULL,
	[C_Start_of_Operation] [date] NULL,
	[C_Rent_Deposit_ID] [smallint] NULL,
	[C_Property_Occupancy_Estimate] [varchar](20) NULL,
	[C_Location_Score] [smallint] NULL,
	[C_longitute] [nvarchar](100) NULL,
	[C_latitute] [nvarchar](100) NULL,
	[C_Property_Amenities_ID] [nvarchar](2000) NULL,
	[C_Property_Category_ID] [smallint] NULL,
	[C_Comment_on_Location_Score] [varchar](200) NULL,
	[C_No_of_Floors] [smallint] NULL,
	[C_No_of_Beds] [smallint] NULL,
	[C_Floor_Beds] [smallint] NULL,
	[C_Room_Quality_Score_ID] [smallint] NULL,
	[C_Room_Cleanliness_Score_ID] [smallint] NULL,
	[C_Backup_Power_ID] [bit] NULL,
	[C_Diesel_Generator_ID] [bit] NULL,
	[C_Emergency_Staircase] [bit] NULL,
	[C_Common_Area_Cleanliness_ID] [smallint] NULL,
	[C_Age_of_the_Property] [smallint] NULL,
	[C_Owner_Detail_ID] [smallint] NULL,
	[C_Reception_Area_yn] [bit] NULL,
	[C_Fire_Fighting_Equipment_IDs] [varchar](20) NULL,
	[C_IsCanteenExists] [bit] NULL,
	[C_Canteen_Cleanliness_ID] [smallint] NULL,
	[C_Food_Availability_ID] [smallint] NULL,
	[C_Food_Score] [smallint] NULL,
	[C_Operator_By_ID] [smallint] NULL,
	[C_Operator_Number] [varchar](100) NULL,
	[C_Available_Placio] [bit] NULL,
	[IsCompetatorProperty] [bit] NULL,
	[C_Main_Property_ID] [int] NOT NULL,
 CONSTRAINT [PK_C_PROPERTY] PRIMARY KEY CLUSTERED 
(
	[C_Property_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Property_Canteen_Photos]    Script Date: 10/23/2018 5:22:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Property_Canteen_Photos](
	[C_Property_Canteen_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[C_Property_ID] [int] NULL,
	[C_Photo_Location] [nvarchar](200) NULL,
	[C_Seq_ID] [smallint] NULL,
 CONSTRAINT [C_PK_Property_Canteen_Photos] PRIMARY KEY CLUSTERED 
(
	[C_Property_Canteen_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Property_Common_Area_Cleanliness_Photos]    Script Date: 10/23/2018 5:22:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Property_Common_Area_Cleanliness_Photos](
	[C_Property_Common_Area_Cleanliness_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[C_Property_ID] [int] NULL,
	[C_Photo_Location] [nvarchar](200) NULL,
	[C_Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_C_Property_Common_Area_Cleanliness_Photos] PRIMARY KEY CLUSTERED 
(
	[C_Property_Common_Area_Cleanliness_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Property_Diesel_Generator_Photos]    Script Date: 10/23/2018 5:22:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Property_Diesel_Generator_Photos](
	[C_Property_Diesel_Generator_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[C_Property_ID] [int] NULL,
	[C_Diesel_Generator_ID] [int] NULL,
	[C_Photo_Location] [nvarchar](200) NULL,
	[C_Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_C_Property_Diesel_Generator_Photos] PRIMARY KEY CLUSTERED 
(
	[C_Property_Diesel_Generator_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Property_Lift_Photos]    Script Date: 10/23/2018 5:22:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Property_Lift_Photos](
	[C_Property_Lift_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[C_Property_ID] [int] NULL,
	[C_Lift_ID] [int] NULL,
	[C_Photo_Location] [nvarchar](200) NULL,
	[C_Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_C_Property_Lift_Photos] PRIMARY KEY CLUSTERED 
(
	[C_Property_Lift_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Property_Owner_Details]    Script Date: 10/23/2018 5:22:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Property_Owner_Details](
	[C_Owner_Detail_ID] [int] NOT NULL,
	[C_Property_ID] [int] NULL,
	[C_Owner_Name] [varchar](100) NULL,
	[C_Phone_Number] [varchar](20) NULL,
	[C_Is_owner_Operated] [bit] NULL,
 CONSTRAINT [PK_C_Property_Owner_Details] PRIMARY KEY CLUSTERED 
(
	[C_Owner_Detail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Property_Photos]    Script Date: 10/23/2018 5:22:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Property_Photos](
	[C_Property_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[C_Property_ID] [int] NULL,
	[C_Photo_Location] [nvarchar](200) NULL,
	[C_Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_C_Property_Photos] PRIMARY KEY CLUSTERED 
(
	[C_Property_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Property_Room_Photos]    Script Date: 10/23/2018 5:22:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Property_Room_Photos](
	[C_Property_Room_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[C_Property_ID] [int] NULL,
	[C_Photo_Location] [nvarchar](200) NULL,
	[C_Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_C_Property_Room_Photos] PRIMARY KEY CLUSTERED 
(
	[C_Property_Room_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Property_Security_Features]    Script Date: 10/23/2018 5:22:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Property_Security_Features](
	[C_Property_Security_Feature_ID] [int] IDENTITY(1,1) NOT NULL,
	[C_Property_ID] [int] NULL,
	[C_Security_Feature_ID] [int] NOT NULL,
 CONSTRAINT [PK_Property_Security_Features] PRIMARY KEY CLUSTERED 
(
	[C_Property_Security_Feature_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category_Items]    Script Date: 10/23/2018 5:22:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category_Items](
	[CATEGORY_ITEM_ID] [int] IDENTITY(1,1) NOT NULL,
	[CATEGORY_ITEM_CODE] [nchar](15) NULL,
	[ITEM_CATEGORY_ID] [int] NOT NULL,
	[ITEM_CATEGORY_SUB_ID] [int] NOT NULL,
	[CATEGORY_ITEM_NAME] [nvarchar](200) NULL,
 CONSTRAINT [PK_CATEGORY_ITEMS] PRIMARY KEY CLUSTERED 
(
	[CATEGORY_ITEM_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 10/23/2018 5:22:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[company_id] [int] NOT NULL,
	[logo] [nvarchar](200) NOT NULL,
	[fevicon] [nvarchar](100) NOT NULL,
	[admin_name] [nvarchar](100) NOT NULL,
	[site_name] [nvarchar](300) NOT NULL,
	[site_url] [nvarchar](300) NOT NULL,
	[front_url] [nvarchar](1000) NOT NULL,
	[email] [nvarchar](100) NOT NULL,
	[mobile_no] [nvarchar](50) NOT NULL,
	[sms_api] [nvarchar](1000) NOT NULL,
	[sms_send_status] [nchar](10) NOT NULL,
	[meta_title] [nvarchar](100) NOT NULL,
	[meta_keywords] [nvarchar](1000) NOT NULL,
	[meta_description] [nvarchar](255) NOT NULL,
	[google_analytics_code] [nvarchar](1000) NOT NULL,
	[html_meta_code] [nvarchar](100) NOT NULL,
	[footer_copyright] [nvarchar](100) NOT NULL,
	[facebook_url] [nvarchar](100) NOT NULL,
	[twitter_url] [nvarchar](100) NOT NULL,
	[youtube_url] [nvarchar](100) NOT NULL,
	[google_plus_url] [nvarchar](100) NOT NULL,
	[pinterest_url] [nvarchar](100) NOT NULL,
	[linkedin_url] [nvarchar](100) NOT NULL,
	[instagram_url] [nvarchar](100) NOT NULL,
	[facebook_app_id] [nvarchar](100) NOT NULL,
	[facebook_app_secret] [nvarchar](100) NOT NULL,
	[google_secret_key] [nvarchar](100) NOT NULL,
	[google_developer_key] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_company_settings] PRIMARY KEY CLUSTERED 
(
	[company_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company_Employees]    Script Date: 10/23/2018 5:22:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company_Employees](
	[company_employee_id] [int] IDENTITY(1,1) NOT NULL,
	[employee_id] [int] NOT NULL,
	[company_id] [int] NOT NULL,
	[job_title_code] [nchar](15) NULL,
	[date_from] [datetime] NULL,
	[date_to] [datetime] NULL,
 CONSTRAINT [PK_Company_Employees] PRIMARY KEY CLUSTERED 
(
	[employee_id] ASC,
	[company_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Complaint_Types]    Script Date: 10/23/2018 5:22:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Complaint_Types](
	[complaint_type_id] [int] NOT NULL,
	[complaint_type_cd] [nchar](15) NULL,
	[complaint_type] [nvarchar](100) NULL,
	[active_yn] [bit] NOT NULL,
 CONSTRAINT [PK_Complaint_Types] PRIMARY KEY CLUSTERED 
(
	[complaint_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Complaints]    Script Date: 10/23/2018 5:22:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Complaints](
	[Complaint_ID] [int] IDENTITY(1,1) NOT NULL,
	[Registration_Number] [nvarchar](50) NULL,
	[Mobile_Number] [nvarchar](20) NULL,
	[Room_No] [nvarchar](50) NULL,
	[Block_Wing] [nvarchar](50) NULL,
	[Hostel_Name] [nvarchar](100) NULL,
	[Complaint_Type_ID] [int] NULL,
	[Complaint] [nvarchar](2000) NULL,
	[Date_of_Submission] [date] NULL,
 CONSTRAINT [PK_Complaints] PRIMARY KEY CLUSTERED 
(
	[Complaint_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Complaints_Details]    Script Date: 10/23/2018 5:22:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Complaints_Details](
	[complaints_Detail_id] [int] IDENTITY(1,1) NOT NULL,
	[complaint_ID] [int] NOT NULL,
	[registration_number] [nvarchar](50) NULL,
	[property_id] [int] NULL,
	[complaints_status_code] [nvarchar](50) NULL,
	[assigned_to] [int] NULL,
	[assigned_on] [date] NULL,
	[escalated_yn] [bit] NULL,
	[escalted_to] [int] NULL,
	[escalated_date] [date] NULL,
	[reason_for_escalation] [nvarchar](2000) NULL,
	[comments_by_asssignee] [nvarchar](2000) NULL,
	[resolved_yn] [bit] NULL,
	[resolved_by] [int] NULL,
	[resolved_date] [date] NULL,
	[additional_comments] [nvarchar](2000) NULL,
 CONSTRAINT [PK_Complaints_Details] PRIMARY KEY CLUSTERED 
(
	[complaints_Detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contact_Followup_Action]    Script Date: 10/23/2018 5:22:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact_Followup_Action](
	[followup_action_code] [nchar](15) NOT NULL,
	[followup_action_desc] [nvarchar](250) NULL,
 CONSTRAINT [PK_Contact_Followup_Action] PRIMARY KEY CLUSTERED 
(
	[followup_action_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contact_US]    Script Date: 10/23/2018 5:22:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact_US](
	[user_id] [int] NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[email] [nvarchar](100) NOT NULL,
	[mobile_no] [nvarchar](100) NOT NULL,
	[referral_name] [nvarchar](100) NULL,
	[referral_email] [nvarchar](150) NULL,
	[referral_mobile_no] [nvarchar](50) NULL,
	[profile] [nvarchar](50) NOT NULL,
	[name_institute] [nvarchar](255) NULL,
	[type_of_property] [nvarchar](50) NULL,
	[no_of_bedroom] [nvarchar](50) NULL,
	[rent] [nvarchar](50) NULL,
	[currently_vacant] [nvarchar](50) NULL,
	[type_of_room] [nvarchar](50) NULL,
	[budget] [nvarchar](50) NULL,
	[city] [nvarchar](50) NULL,
	[area] [nvarchar](100) NULL,
	[address] [nvarchar](100) NULL,
	[message] [nvarchar](2000) NULL,
	[status] [int] NULL,
	[type] [nvarchar](100) NULL,
	[timestamp] [timestamp] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Daily_Room_Rates]    Script Date: 10/23/2018 5:22:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Daily_Room_Rates](
	[property_id] [int] NOT NULL,
	[room_number] [nchar](20) NOT NULL,
	[day_date] [nchar](20) NOT NULL,
	[daily_room_rate_offered] [int] NULL,
	[daily_room_rate_accepted] [int] NULL,
 CONSTRAINT [PK_Daily_Room_Rates] PRIMARY KEY CLUSTERED 
(
	[property_id] ASC,
	[room_number] ASC,
	[day_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Department_Employees]    Script Date: 10/23/2018 5:22:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Department_Employees](
	[department_employees_id] [int] IDENTITY(1,1) NOT NULL,
	[department_id] [int] NOT NULL,
	[employee_id] [int] NOT NULL,
	[from_date] [date] NOT NULL,
	[to_date] [date] NULL,
 CONSTRAINT [PK_Department_Employees] PRIMARY KEY CLUSTERED 
(
	[department_id] ASC,
	[employee_id] ASC,
	[from_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Departments]    Script Date: 10/23/2018 5:22:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Departments](
	[department_id] [int] NOT NULL,
	[department_name] [nvarchar](50) NULL,
	[department_category_code] [nvarchar](20) NULL,
 CONSTRAINT [PK_Departments] PRIMARY KEY CLUSTERED 
(
	[department_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Email_Template]    Script Date: 10/23/2018 5:22:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Email_Template](
	[email_template_id] [int] NOT NULL,
	[email_template_name] [nvarchar](100) NOT NULL,
	[subject] [nvarchar](100) NOT NULL,
	[sender_name] [nvarchar](100) NOT NULL,
	[sender_email] [nvarchar](100) NOT NULL,
	[content] [nvarchar](4000) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee_Addresses]    Script Date: 10/23/2018 5:22:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Addresses](
	[employee_address_id] [int] NOT NULL,
	[address_id] [int] NULL,
	[address_type_code] [nchar](15) NULL,
	[employee_id] [int] NULL,
	[date_address_from] [date] NULL,
	[date_address_to] [date] NULL,
	[active_address_id] [int] NULL,
 CONSTRAINT [PK_Employee_Addresses] PRIMARY KEY CLUSTERED 
(
	[employee_address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee_Qualifications]    Script Date: 10/23/2018 5:22:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Qualifications](
	[employee_qualification_id] [int] IDENTITY(1,1) NOT NULL,
	[employee_id] [int] NOT NULL,
	[qualification_code] [nchar](15) NOT NULL,
	[date_obtained] [date] NULL,
 CONSTRAINT [PK_Employee_Qualifications] PRIMARY KEY CLUSTERED 
(
	[employee_id] ASC,
	[qualification_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee_Source_Types]    Script Date: 10/23/2018 5:22:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Source_Types](
	[employee_source_type_id] [int] IDENTITY(1,1) NOT NULL,
	[employee_source_type] [nvarchar](50) NULL,
	[active_yn] [bit] NULL,
 CONSTRAINT [PK_Employee_Source_Types] PRIMARY KEY CLUSTERED 
(
	[employee_source_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee_Titles]    Script Date: 10/23/2018 5:22:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Titles](
	[employee_job_title_id] [int] IDENTITY(1,1) NOT NULL,
	[employee_id] [int] NULL,
	[job_title_code] [nchar](15) NULL,
	[date_from] [datetime] NULL,
	[date_to] [datetime] NULL,
 CONSTRAINT [PK_Employee_Titles] PRIMARY KEY CLUSTERED 
(
	[employee_job_title_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 10/23/2018 5:22:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[employee_id] [int] NOT NULL,
	[employee_details] [nvarchar](250) NULL,
	[nick_name] [nvarchar](50) NULL,
	[first_name] [nvarchar](50) NULL,
	[middle_name] [nvarchar](50) NULL,
	[last_name] [nvarchar](50) NULL,
	[gender] [nvarchar](10) NULL,
	[date_of_birth] [date] NULL,
	[date_joined] [date] NULL,
	[date_left] [date] NULL,
	[department_id] [int] NULL,
	[job_title_code] [nvarchar](15) NULL,
	[qualification_code] [nvarchar](15) NULL,
	[updated_date] [date] NULL,
	[updated_by] [int] NULL,
 CONSTRAINT [PK_employee_id] PRIMARY KEY CLUSTERED 
(
	[employee_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[expense_category]    Script Date: 10/23/2018 5:22:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[expense_category](
	[expense_category_id] [int] IDENTITY(1,1) NOT NULL,
	[employee_id] [int] NULL,
	[category_name] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[expense_category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[expenses]    Script Date: 10/23/2018 5:22:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[expenses](
	[expenses_id] [int] IDENTITY(1,1) NOT NULL,
	[employee_id] [int] NULL,
	[expense_date] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[amount] [float] NULL,
	[expense_category_id] [int] NULL,
	[upload_receipt] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[expenses_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Facilities]    Script Date: 10/23/2018 5:22:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Facilities](
	[facility_id] [int] NOT NULL,
	[facility_name] [nvarchar](500) NULL,
	[facility_type] [nvarchar](50) NULL,
	[facility_description] [nvarchar](2000) NULL,
 CONSTRAINT [PK_Facilities] PRIMARY KEY CLUSTERED 
(
	[facility_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Financial_Item_Categories]    Script Date: 10/23/2018 5:22:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Financial_Item_Categories](
	[financial_category_id] [int] NOT NULL,
	[financial_category_code] [nvarchar](10) NOT NULL,
	[category_description] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_financial_item_categories] PRIMARY KEY CLUSTERED 
(
	[financial_category_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Food_Menu]    Script Date: 10/23/2018 5:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Food_Menu](
	[food_menu_id] [int] IDENTITY(1,1) NOT NULL,
	[week_day] [nchar](15) NULL,
	[break_fast] [nvarchar](2000) NULL,
	[lunch] [nvarchar](2000) NULL,
	[dinner] [nvarchar](2000) NULL,
	[active_yn] [bit] NOT NULL,
 CONSTRAINT [PK_Food_Menu] PRIMARY KEY CLUSTERED 
(
	[food_menu_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Income]    Script Date: 10/23/2018 5:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Income](
	[income_id] [int] IDENTITY(1,1) NOT NULL,
	[employee_id] [int] NULL,
	[income_date] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[amount] [float] NULL,
	[income_category_id] [int] NULL,
	[upload_receipt] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[income_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Income_Category]    Script Date: 10/23/2018 5:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Income_Category](
	[income_category_id] [int] IDENTITY(1,1) NOT NULL,
	[employee_id] [int] NULL,
	[category_name] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[income_category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventory_Item_Details]    Script Date: 10/23/2018 5:22:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory_Item_Details](
	[Item_Detail_id] [int] IDENTITY(1,1) NOT NULL,
	[item_id] [int] NOT NULL,
	[manufacturer_name] [nvarchar](200) NULL,
	[mfg_date] [date] NULL,
	[item_expiry_date] [date] NULL,
	[item_age] [int] NULL,
	[warranty] [nvarchar](100) NULL,
	[amc_cost] [decimal](15, 2) NULL,
 CONSTRAINT [PK_Inventory_Item_Details] PRIMARY KEY CLUSTERED 
(
	[Item_Detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventory_Items]    Script Date: 10/23/2018 5:22:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory_Items](
	[item_id] [int] IDENTITY(1,1) NOT NULL,
	[item_code] [nvarchar](20) NULL,
	[brand_id] [int] NOT NULL,
	[item_category_code] [nvarchar](20) NOT NULL,
	[item_description] [nvarchar](200) NOT NULL,
	[average_monthly_usage] [nvarchar](20) NULL,
	[reorder_level] [nvarchar](20) NULL,
	[reorder_quantity] [nvarchar](20) NULL,
 CONSTRAINT [PK_Inventory_Items] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item_Categories]    Script Date: 10/23/2018 5:22:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item_Categories](
	[ITEM_CATEGORY_ID] [int] IDENTITY(1,1) NOT NULL,
	[ITEM_CATEOGRY_CODE] [nchar](15) NULL,
	[ITEM_CATEGORY_NAME] [nvarchar](100) NULL,
 CONSTRAINT [PK_ITEM_CATEGORIES_1] PRIMARY KEY CLUSTERED 
(
	[ITEM_CATEGORY_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item_Stock_Levels]    Script Date: 10/23/2018 5:22:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item_Stock_Levels](
	[item_stock_level_id] [int] IDENTITY(1,1) NOT NULL,
	[item_id] [int] NOT NULL,
	[stock_tracking_date] [date] NOT NULL,
	[quantity_in_stock] [int] NULL,
 CONSTRAINT [PK_Item_Stock_Levels] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC,
	[stock_tracking_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item_Sub_Categories]    Script Date: 10/23/2018 5:22:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item_Sub_Categories](
	[ITEM_CATEGORY_SUB_ID] [int] IDENTITY(1,1) NOT NULL,
	[ITEM_CATEOGRY_SUB_CODE] [nchar](15) NULL,
	[ITEM_CATEGORY_ID] [int] NOT NULL,
	[SUB_CATEGORY_NAME] [nvarchar](100) NULL,
 CONSTRAINT [PK_ITEM_SUB_CATEGORIES] PRIMARY KEY CLUSTERED 
(
	[ITEM_CATEGORY_SUB_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item_SubCategory]    Script Date: 10/23/2018 5:22:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item_SubCategory](
	[item_subcategory_id] [int] NOT NULL,
	[financial_category_code] [nvarchar](10) NOT NULL,
	[item_name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_item_subcategory] PRIMARY KEY CLUSTERED 
(
	[financial_category_code] ASC,
	[item_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item_Suppliers]    Script Date: 10/23/2018 5:22:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item_Suppliers](
	[Item_Supplier_id] [int] IDENTITY(1,1) NOT NULL,
	[Item_id] [int] NOT NULL,
	[supplier_code] [nchar](15) NOT NULL,
	[value_supplied_to_date] [int] NULL,
	[total_quantity_supplied_to_date] [int] NULL,
	[first_item_supplied_date] [date] NULL,
	[last_item_supplied_date] [date] NULL,
	[delivery_lead_time] [nvarchar](20) NULL,
	[standard_price] [decimal](15, 2) NULL,
	[percenage_discount] [decimal](15, 2) NULL,
	[minimum_order_quantity] [int] NULL,
	[maximum_order_quantity] [int] NULL,
 CONSTRAINT [PK_Item_Suppliers] PRIMARY KEY CLUSTERED 
(
	[Item_id] ASC,
	[supplier_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Job_Titles]    Script Date: 10/23/2018 5:22:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Job_Titles](
	[job_title_code] [nchar](15) NOT NULL,
	[job_title_desc] [nvarchar](150) NULL,
 CONSTRAINT [PK_Job_Titles] PRIMARY KEY CLUSTERED 
(
	[job_title_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 10/23/2018 5:22:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locations](
	[location_code] [nchar](15) NOT NULL,
	[location_description] [nvarchar](150) NULL,
 CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED 
(
	[location_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mobile_Template]    Script Date: 10/23/2018 5:22:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mobile_Template](
	[mobile_template_id] [int] NOT NULL,
	[mobile_template_name] [nvarchar](200) NOT NULL,
	[message] [nvarchar](2000) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Parents_and_Guardians]    Script Date: 10/23/2018 5:22:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Parents_and_Guardians](
	[person_id] [int] NOT NULL,
	[address_id] [int] NOT NULL,
	[gender] [nchar](10) NULL,
	[first_name] [nvarchar](50) NULL,
	[middle_name] [nvarchar](50) NULL,
	[last_name] [nvarchar](50) NULL,
	[cell_mobile_number] [nchar](20) NULL,
	[email_address] [nvarchar](50) NULL,
	[other_details] [nvarchar](150) NULL,
 CONSTRAINT [PK_Parents_and_Gaurdians] PRIMARY KEY CLUSTERED 
(
	[person_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payment_Methods]    Script Date: 10/23/2018 5:22:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment_Methods](
	[payment_method_code] [nchar](15) NOT NULL,
	[payment_method_desc] [nvarchar](250) NULL,
 CONSTRAINT [PK_Payment_Methods] PRIMARY KEY CLUSTERED 
(
	[payment_method_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PR_HK_Checklist_Status]    Script Date: 10/23/2018 5:22:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PR_HK_Checklist_Status](
	[PR_HK_Checklist_Status_ID] [int] IDENTITY(1,1) NOT NULL,
	[property_id] [int] NOT NULL,
	[property_hk_check_list_id] [int] NULL,
	[category_item_id] [int] NULL,
	[applicable_yn] [bit] NULL,
	[available_yn] [bit] NULL,
	[completed_yn] [bit] NULL,
	[employee_id] [int] NULL,
	[date_of_submission] [date] NULL,
	[remarks] [nvarchar](1000) NULL,
	[updated_by] [int] NULL,
	[updated_on] [date] NULL,
 CONSTRAINT [PK_PR_HK_Checklist_Status] PRIMARY KEY CLUSTERED 
(
	[PR_HK_Checklist_Status_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PR_Procure_Checklist_Status]    Script Date: 10/23/2018 5:22:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PR_Procure_Checklist_Status](
	[property_id] [int] NOT NULL,
	[readiness_for_procure_checklist_item_id] [int] NOT NULL,
	[applicable_yn] [bit] NULL,
	[available_yn] [bit] NULL,
	[completed_yn] [bit] NULL,
	[employee_id] [int] NULL,
	[date_of_visit] [date] NULL,
	[remarks] [nvarchar](1000) NULL,
	[updated_by] [int] NULL,
	[updated_on] [date] NULL,
 CONSTRAINT [PK_PR_Procure_Checklist_Status] PRIMARY KEY CLUSTERED 
(
	[property_id] ASC,
	[readiness_for_procure_checklist_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Price_Categories]    Script Date: 10/23/2018 5:22:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Price_Categories](
	[price_categories_code] [int] NOT NULL,
	[price_category] [nvarchar](50) NULL,
	[min_price_range] [int] NULL,
	[max_price_range] [int] NULL,
	[active_yn] [bit] NULL,
 CONSTRAINT [PK_Price_Categories] PRIMARY KEY CLUSTERED 
(
	[price_categories_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Properties_Readiness_for_Procure_Checklist_Items]    Script Date: 10/23/2018 5:22:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Properties_Readiness_for_Procure_Checklist_Items](
	[property_id] [int] NOT NULL,
	[readiness_for_procure_checklist_item_id] [int] NOT NULL,
	[applicable_yn] [bit] NULL,
	[available_yn] [bit] NULL,
	[completed_yn] [bit] NULL,
 CONSTRAINT [PK_Properties_Readiness_for_Procure_Checklist_Items] PRIMARY KEY CLUSTERED 
(
	[property_id] ASC,
	[readiness_for_procure_checklist_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property]    Script Date: 10/23/2018 5:22:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property](
	[Property_ID] [int] NOT NULL,
	[Property_Name] [varchar](200) NULL,
	[Property_Alias_Name] [varchar](200) NULL,
	[Property_Type_ID] [int] NULL,
	[Property_Address_ID] [int] NULL,
	[Property_Description] [nvarchar](2000) NULL,
	[Property_Status_Code] [nchar](15) NULL,
	[Cluster_Manager_ID] [int] NULL,
	[Property_Manager_ID] [int] NULL,
	[Meta_Title] [nvarchar](200) NULL,
	[Meta_Keywords] [nvarchar](4000) NULL,
	[Meta_Description] [nvarchar](1000) NULL,
	[vr_url] [nvarchar](1000) NULL,
	[Created_at] [datetime] NULL,
	[Updated_at] [datetime] NULL,
	[Created_by] [int] NULL,
	[Updated_by] [int] NULL,
	[longitute] [nvarchar](100) NULL,
	[latitute] [nvarchar](100) NULL,
 CONSTRAINT [PK_PROPERTY] PRIMARY KEY CLUSTERED 
(
	[Property_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Addresses]    Script Date: 10/23/2018 5:22:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Addresses](
	[property_address_id] [int] NOT NULL,
	[address_id] [int] NULL,
	[address_type_code] [nchar](15) NULL,
	[property_id] [int] NULL,
	[date_address_from] [datetime] NULL,
	[date_address_to] [datetime] NULL,
 CONSTRAINT [PK_Property_Addresses] PRIMARY KEY CLUSTERED 
(
	[property_address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Amenities]    Script Date: 10/23/2018 5:22:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Amenities](
	[Property_Amenities_id] [int] IDENTITY(1,1) NOT NULL,
	[property_id] [int] NOT NULL,
	[amenity_id] [int] NOT NULL,
	[parent_id] [int] NULL,
	[crated_date] [date] NULL,
	[crated_by] [int] NULL,
	[active_yn] [bit] NULL,
 CONSTRAINT [PK_Property_Amenities] PRIMARY KEY CLUSTERED 
(
	[Property_Amenities_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Block]    Script Date: 10/23/2018 5:22:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Block](
	[property_block_id] [int] NOT NULL,
	[property_id] [int] NOT NULL,
	[block_name] [nvarchar](300) NULL,
	[no_of_rooms] [int] NULL,
	[display_name] [nvarchar](300) NULL,
 CONSTRAINT [PK_Property_block] PRIMARY KEY CLUSTERED 
(
	[property_block_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Block_Floor]    Script Date: 10/23/2018 5:22:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Block_Floor](
	[Property_block_floor_id] [int] NOT NULL,
	[property_id] [int] NULL,
	[property_block_id] [int] NULL,
	[floor_id] [int] NULL,
	[floor_name] [nvarchar](200) NOT NULL,
	[no_of_rooms] [int] NULL,
	[display_name] [nvarchar](200) NULL,
 CONSTRAINT [PK_Property_Block_floor] PRIMARY KEY CLUSTERED 
(
	[Property_block_floor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Canteen]    Script Date: 10/23/2018 5:22:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Canteen](
	[property_canteen_id] [int] IDENTITY(1,1) NOT NULL,
	[property_id] [int] NOT NULL,
	[canteen_name] [nvarchar](200) NULL,
	[seating_capacity] [int] NULL,
	[canteen_start_time] [time](7) NULL,
	[canteen_end_time] [time](7) NULL,
	[canteen_incharge] [int] NULL,
	[contact_number] [nvarchar](20) NULL,
	[cell_phone] [nvarchar](20) NULL,
	[display_name] [nvarchar](200) NULL,
 CONSTRAINT [PK_Property_Canteen] PRIMARY KEY CLUSTERED 
(
	[property_canteen_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Canteen_Menu]    Script Date: 10/23/2018 5:22:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Canteen_Menu](
	[Property_Canteen_Menu_id] [int] IDENTITY(1,1) NOT NULL,
	[property_id] [int] NOT NULL,
	[food_menu_id] [int] NOT NULL,
	[week_day] [nchar](15) NULL,
	[break_fast] [nvarchar](2000) NULL,
	[lunch] [nvarchar](2000) NULL,
	[dinner] [nvarchar](2000) NULL,
	[active_yn] [bit] NOT NULL,
 CONSTRAINT [PK_Property_Canteen_Menu] PRIMARY KEY CLUSTERED 
(
	[Property_Canteen_Menu_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Canteen_Owner]    Script Date: 10/23/2018 5:22:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Canteen_Owner](
	[property_canteen_owner_id] [int] NOT NULL,
	[property_id] [int] NULL,
	[property_canteen_id] [int] NULL,
	[owner_name] [nvarchar](150) NOT NULL,
	[Gender] [nvarchar](10) NULL,
	[aggrement_tenure] [nvarchar](100) NULL,
	[agreement_expire] [nvarchar](50) NULL,
	[aadhar_file] [nvarchar](255) NULL,
	[aadhar_no] [nvarchar](50) NULL,
	[pan_file] [nvarchar](255) NULL,
	[pan_no] [nvarchar](50) NULL,
	[cancel_check] [nvarchar](100) NULL,
	[lease_agreement] [nvarchar](100) NULL,
	[created_at] [nvarchar](50) NULL,
 CONSTRAINT [PK_Property_Canteen_Owner] PRIMARY KEY CLUSTERED 
(
	[property_canteen_owner_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Canteen_Owner_Addresses]    Script Date: 10/23/2018 5:22:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Canteen_Owner_Addresses](
	[Property_Canteen_Owner_address_id] [int] NOT NULL,
	[address_id] [int] NULL,
	[address_type_code] [nchar](15) NULL,
	[property_id] [int] NULL,
	[property_canteen_id] [int] NULL,
	[proerty_canteen_owner_id] [int] NULL,
	[date_address_from] [date] NULL,
	[date_address_to] [date] NULL,
 CONSTRAINT [PK_Property_Canteen_Owner_Addresses] PRIMARY KEY CLUSTERED 
(
	[Property_Canteen_Owner_address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Canteen_Photos]    Script Date: 10/23/2018 5:22:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Canteen_Photos](
	[Property_Canteen_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[PhotoLocation] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_Property_Canteen_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Canteen_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Details]    Script Date: 10/23/2018 5:22:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Details](
	[Property_Detail_ID] [int] NOT NULL,
	[Property_ID] [int] NOT NULL,
	[No_of_Floors] [smallint] NULL,
	[Location_Score] [smallint] NULL,
	[Comment_on_Location_Score] [varchar](200) NULL,
	[Room_Quality_Score_ID] [smallint] NULL,
	[Room_Cleanliness_Score_ID] [smallint] NULL,
	[Water_Source_ID] [smallint] NULL,
	[Water_Storage_Capacity_ID] [smallint] NULL,
	[Emergency_Staircase] [bit] NULL,
	[Diesel_Generator_ID] [smallint] NULL,
	[Common_Area_Cleanliness_ID] [smallint] NULL,
	[Parking_Availability] [bit] NULL,
	[Parking_Vehicle_Count] [smallint] NULL,
	[House_Keeping_staff_Rooms] [smallint] NULL,
	[Age_of_the_Property] [smallint] NULL,
	[IsNewProperty] [bit] NULL,
	[Gender] [smallint] NULL,
	[No_of_Beds] [smallint] NULL,
	[Property_Occupancy_Estimate] [varchar](20) NULL,
	[Room_wise_Power_Meter_ID] [bit] NULL,
	[Room_Cleanliness_ID] [smallint] NULL,
	[Reception_Area_yn] [bit] NULL,
	[Wifi_Reception] [smallint] NULL,
	[Security_Feature_IDs] [varchar](20) NULL,
	[Fire_Fighting_Equipment_IDs] [varchar](20) NULL,
	[ISP_Provider_Name] [varchar](50) NULL,
	[IsCanteenExists] [bit] NULL,
	[Canteen_Cleanliness_ID] [smallint] NULL,
	[Food_Availability_ID] [smallint] NULL,
	[Food_Score] [smallint] NULL,
	[Operator_Number] [varchar](100) NULL,
	[Lift_ID] [smallint] NULL,
 CONSTRAINT [PK_PROPERTY_Detail_ID] PRIMARY KEY CLUSTERED 
(
	[Property_Detail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Diesel_Generator]    Script Date: 10/23/2018 5:22:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Diesel_Generator](
	[Diesel_Generator_ID] [int] NOT NULL,
	[Property_ID] [int] NULL,
	[Age] [smallint] NULL,
	[Brand] [varchar](20) NULL,
	[Warranty] [varchar](100) NULL,
	[AMC_Cost] [decimal](15, 2) NULL,
	[storage_capacity] [varchar](20) NULL,
 CONSTRAINT [PK_Property_Diesel_Generator] PRIMARY KEY CLUSTERED 
(
	[Diesel_Generator_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Diesel_Generator_Photos]    Script Date: 10/23/2018 5:22:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Diesel_Generator_Photos](
	[Property_Diesel_Generator_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[Diesel_Generator_ID] [int] NULL,
	[Photo_Location] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_P_Property_Diesel_Generator_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Diesel_Generator_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Documents]    Script Date: 10/23/2018 5:22:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Documents](
	[property_document_id] [int] NOT NULL,
	[document_id] [int] NULL,
	[document_type_code] [nchar](15) NULL,
	[property_id] [int] NULL,
	[available_yn] [bit] NULL,
	[quality_hi_med_low] [nchar](10) NULL,
	[other_details] [nvarchar](250) NULL,
 CONSTRAINT [PK_Property_Documents] PRIMARY KEY CLUSTERED 
(
	[property_document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Employees]    Script Date: 10/23/2018 5:22:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Employees](
	[property_employee_id] [int] IDENTITY(1,1) NOT NULL,
	[employee_id] [int] NOT NULL,
	[property_id] [int] NOT NULL,
	[active_yn] [bit] NOT NULL,
	[assigned_date] [date] NULL,
	[assigned_by] [int] NULL,
	[from_period] [date] NULL,
	[to_period] [date] NULL,
 CONSTRAINT [PK_Property_Employees] PRIMARY KEY CLUSTERED 
(
	[employee_id] ASC,
	[property_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[property_feasibility]    Script Date: 10/23/2018 5:22:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[property_feasibility](
	[property_feasibility_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[pg_name] [nvarchar](200) NOT NULL,
	[zone] [nvarchar](200) NOT NULL,
	[micromarket] [nvarchar](200) NOT NULL,
	[score] [nvarchar](50) NOT NULL,
	[question_value] [nvarchar](1000) NOT NULL,
	[create_date] [datetime] NOT NULL,
 CONSTRAINT [PK_property_feasibility] PRIMARY KEY CLUSTERED 
(
	[property_feasibility_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[property_feasibility_settings]    Script Date: 10/23/2018 5:22:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[property_feasibility_settings](
	[property_feasibility_setting_id] [int] NOT NULL,
	[question_name] [nvarchar](200) NOT NULL,
	[input_type] [nvarchar](30) NOT NULL,
	[sub_question_name] [nvarchar](200) NOT NULL,
	[weightage] [nvarchar](50) NOT NULL,
	[option_value] [nvarchar](1000) NOT NULL,
	[status] [int] NOT NULL,
	[type] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_property_feasibility_settings] PRIMARY KEY CLUSTERED 
(
	[property_feasibility_setting_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Floor_Room_Bed]    Script Date: 10/23/2018 5:23:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Floor_Room_Bed](
	[bed_id] [int] NOT NULL,
	[property_id] [int] NOT NULL,
	[block_id] [int] NULL,
	[floor_id] [int] NOT NULL,
	[room_no] [nvarchar](100) NOT NULL,
	[bed_no] [nvarchar](100) NOT NULL,
	[price] [int] NULL,
	[status] [int] NOT NULL,
	[display_name] [nvarchar](100) NULL,
 CONSTRAINT [PK_property_floor_room_bed] PRIMARY KEY CLUSTERED 
(
	[bed_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Floor_Rooms]    Script Date: 10/23/2018 5:23:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Floor_Rooms](
	[property_floor_room_id] [int] NOT NULL,
	[property_id] [int] NOT NULL,
	[property_block_id] [int] NULL,
	[property_block_floor_id] [int] NULL,
	[room_no] [nvarchar](100) NOT NULL,
	[room_name] [nvarchar](100) NULL,
	[no_of_beds] [int] NULL,
	[display_name] [nvarchar](100) NULL,
 CONSTRAINT [PK_P_Property_Floor_Rooms] PRIMARY KEY CLUSTERED 
(
	[room_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_HK_CheckList]    Script Date: 10/23/2018 5:23:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_HK_CheckList](
	[property_hk_check_list_id] [int] IDENTITY(1,1) NOT NULL,
	[property_id] [int] NOT NULL,
	[category_item_id] [int] NOT NULL,
	[item_category_id] [int] NULL,
	[item_category_sub_id] [int] NULL,
	[category_item_name] [nvarchar](200) NULL,
	[applicable_yn] [bit] NULL,
	[available_yn] [bit] NULL,
	[completed_yn] [bit] NULL,
 CONSTRAINT [pk_property_hk_checklist] PRIMARY KEY CLUSTERED 
(
	[property_hk_check_list_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Inventory]    Script Date: 10/23/2018 5:23:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Inventory](
	[property_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[property_id] [int] NOT NULL,
	[item_ID] [int] NOT NULL,
	[item_tagged_by] [int] NULL,
	[item_tagged_date] [date] NULL,
	[item_tagged_level_code] [nchar](20) NULL,
	[item_tagged_to] [int] NULL,
	[active_yn] [bit] NULL,
 CONSTRAINT [PK_Property_Inventory] PRIMARY KEY CLUSTERED 
(
	[property_id] ASC,
	[item_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Lease_Details]    Script Date: 10/23/2018 5:23:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Lease_Details](
	[property_rental_data_id] [int] NULL,
	[property_id] [int] NULL,
	[rentable_size_sqft] [int] NULL,
	[no_of_rooms] [int] NULL,
	[rsf_per_seat] [int] NULL,
	[rent] [decimal](15, 2) NULL,
	[mantainance] [decimal](15, 2) NULL,
	[rate_sqft_2day] [decimal](15, 2) NULL,
	[security_deposit] [decimal](15, 2) NULL,
	[lease_term] [int] NULL,
	[lease_start_date] [date] NULL,
	[notice_required] [int] NULL,
	[lease_expiration_date] [date] NULL,
	[lock_in_period] [int] NULL,
	[available_rooms] [int] NULL,
	[cost_per_room] [int] NULL,
	[cost_per_head] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Lift_Inputs]    Script Date: 10/23/2018 5:23:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Lift_Inputs](
	[Lift_ID] [int] NOT NULL,
	[Property_ID] [int] NULL,
	[Age] [smallint] NULL,
	[Brand] [varchar](20) NULL,
	[Warranty] [varchar](100) NULL,
	[AMC_Cost] [int] NULL,
 CONSTRAINT [PK_P_Property_Lift_Inputs] PRIMARY KEY CLUSTERED 
(
	[Lift_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Lift_Photos]    Script Date: 10/23/2018 5:23:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Lift_Photos](
	[Property_Lift_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[Lift_ID] [int] NULL,
	[Photo_Location] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_P_Property_Lift_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Lift_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Lookup_Details]    Script Date: 10/23/2018 5:23:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Lookup_Details](
	[Property_Lookup_Detail_id] [int] IDENTITY(1,1) NOT NULL,
	[property_lookup_id] [int] NOT NULL,
	[property_lookup_options] [nvarchar](200) NULL,
	[active_yn] [bit] NULL,
 CONSTRAINT [PK_Property_Lookup_Details] PRIMARY KEY CLUSTERED 
(
	[Property_Lookup_Detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Lookups]    Script Date: 10/23/2018 5:23:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Lookups](
	[property_lookup_id] [int] IDENTITY(1,1) NOT NULL,
	[property_lookup_value] [nvarchar](200) NULL,
	[active_yn] [bit] NULL,
 CONSTRAINT [PK_Property_Lookups] PRIMARY KEY CLUSTERED 
(
	[property_lookup_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Onboard_Opex_Capex_FAR]    Script Date: 10/23/2018 5:23:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Onboard_Opex_Capex_FAR](
	[Property_Onboard_item_id] [int] NOT NULL,
	[financial_category_code] [nvarchar](10) NOT NULL,
	[subcategory] [nvarchar](100) NULL,
	[item_name] [nvarchar](100) NULL,
	[Life_of_Product] [int] NULL,
	[Landlord_Placio] [nvarchar](10) NULL,
	[property_id] [int] NULL,
 CONSTRAINT [PK_Property_Onboard_Opex_Capex_FAR] PRIMARY KEY CLUSTERED 
(
	[Property_Onboard_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Onboard_TimeLines]    Script Date: 10/23/2018 5:23:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Onboard_TimeLines](
	[property_timeline_id] [int] NOT NULL,
	[property_id] [int] NOT NULL,
	[item_code] [nvarchar](50) NULL,
	[item_name] [nvarchar](200) NULL,
	[delivery_date] [date] NULL,
	[installation] [date] NULL,
	[commissioning] [date] NULL,
	[dependency] [nvarchar](200) NULL,
	[landlord_completion_date] [date] NULL,
	[comment_1] [nvarchar](200) NULL,
	[comment_2] [nvarchar](200) NULL,
	[placio_targeted_date] [date] NULL,
 CONSTRAINT [PK_Property_Onboard_TimeLines] PRIMARY KEY CLUSTERED 
(
	[property_timeline_id] ASC,
	[property_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Onboard_TimeLines_Template]    Script Date: 10/23/2018 5:23:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Onboard_TimeLines_Template](
	[property_timeline_id] [int] NOT NULL,
	[item_code] [nvarchar](50) NULL,
	[item_name] [nvarchar](200) NULL,
	[delivery_date] [date] NULL,
	[installation] [date] NULL,
	[commissioning] [date] NULL,
	[dependency] [nvarchar](200) NULL,
	[landlord_completion_date] [date] NULL,
	[comment_1] [nvarchar](200) NULL,
	[comment_2] [nvarchar](200) NULL,
	[placio_targeted_date] [date] NULL,
 CONSTRAINT [PK_Property_Onboard_TimeLines_Template] PRIMARY KEY CLUSTERED 
(
	[property_timeline_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Owner]    Script Date: 10/23/2018 5:23:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Owner](
	[property_owner_id] [int] NOT NULL,
	[property_id] [int] NULL,
	[owner_name] [nvarchar](150) NOT NULL,
	[Gender] [nvarchar](10) NULL,
	[aggrement_tenure] [nvarchar](100) NULL,
	[agreement_expire] [nvarchar](50) NULL,
	[aadhar_file] [nvarchar](255) NULL,
	[aadhar_no] [nvarchar](50) NULL,
	[pan_file] [nvarchar](255) NULL,
	[pan_no] [nvarchar](50) NULL,
	[cancel_check] [nvarchar](100) NULL,
	[lease_agreement] [nvarchar](100) NULL,
	[created_at] [nvarchar](50) NULL,
 CONSTRAINT [PK_Property_Owner] PRIMARY KEY CLUSTERED 
(
	[property_owner_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Owner_Addresses]    Script Date: 10/23/2018 5:23:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Owner_Addresses](
	[Property_Owner_address_id] [int] NOT NULL,
	[address_id] [int] NULL,
	[address_type_code] [nchar](15) NULL,
	[property_id] [int] NULL,
	[proerty_owner_id] [int] NULL,
	[date_address_from] [date] NULL,
	[date_address_to] [date] NULL,
 CONSTRAINT [PK_Property_Owner_Addresses] PRIMARY KEY CLUSTERED 
(
	[Property_Owner_address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Owner_Bank_Details]    Script Date: 10/23/2018 5:23:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Owner_Bank_Details](
	[property_owner_bank_det_id] [int] NOT NULL,
	[property_owner_id] [int] NOT NULL,
	[beneficiary_name] [nvarchar](100) NOT NULL,
	[bank_name] [nvarchar](100) NOT NULL,
	[branch_name] [nvarchar](100) NOT NULL,
	[ac_no] [nvarchar](100) NOT NULL,
	[ifsc_code] [nvarchar](50) NOT NULL,
	[city] [nvarchar](50) NOT NULL,
	[state] [nvarchar](50) NOT NULL,
	[country] [nvarchar](50) NOT NULL,
	[pincode] [nvarchar](50) NOT NULL,
	[bank_phone_number] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Property_Owner_Bank_Details] PRIMARY KEY CLUSTERED 
(
	[property_owner_bank_det_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Owner_Companies]    Script Date: 10/23/2018 5:23:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Owner_Companies](
	[property_owner_company_id] [int] NOT NULL,
	[property_owner_id] [int] NOT NULL,
	[property_id] [int] NOT NULL,
	[owner_company_name] [nvarchar](150) NULL,
	[other_details] [nvarchar](250) NULL,
 CONSTRAINT [PK_apartment_Owner_Companies] PRIMARY KEY CLUSTERED 
(
	[property_owner_company_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Photos]    Script Date: 10/23/2018 5:23:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Photos](
	[Property_Photo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[Photo_Location] [nvarchar](200) NULL,
	[Seq_ID] [smallint] NULL,
 CONSTRAINT [PK_P_Property_Photos] PRIMARY KEY CLUSTERED 
(
	[Property_Photo_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Revenue_Sharing]    Script Date: 10/23/2018 5:23:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Revenue_Sharing](
	[property_revenu_sharing_id] [int] IDENTITY(1,1) NOT NULL,
	[property_owner_id] [int] NOT NULL,
	[property_id] [int] NULL,
	[owner_name] [nvarchar](150) NOT NULL,
	[revenue_sharing_Type_ID] [smallint] NOT NULL,
	[percent_of_Sharing] [decimal](6, 2) NULL,
	[number_of_beds] [int] NULL,
 CONSTRAINT [PK_Property_Revenu_Sharing] PRIMARY KEY CLUSTERED 
(
	[property_revenu_sharing_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Revenue_Sharing_Types]    Script Date: 10/23/2018 5:23:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Revenue_Sharing_Types](
	[revenue_sharing_type_id] [smallint] IDENTITY(1,1) NOT NULL,
	[revenue_sharing_type] [nvarchar](150) NOT NULL,
	[active_yn] [bit] NULL,
 CONSTRAINT [PK_Property_Revenue_Sharing_Types] PRIMARY KEY CLUSTERED 
(
	[revenue_sharing_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Security_Features]    Script Date: 10/23/2018 5:23:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Security_Features](
	[Property_Security_Feature_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NULL,
	[Security_Feature_ID] [int] NOT NULL,
 CONSTRAINT [PK_Property_Security_Feature] PRIMARY KEY CLUSTERED 
(
	[Property_Security_Feature_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Status]    Script Date: 10/23/2018 5:23:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Status](
	[property_status_code] [nchar](50) NOT NULL,
	[property_status_desc] [nvarchar](4000) NULL,
 CONSTRAINT [PK_Property_Status] PRIMARY KEY CLUSTERED 
(
	[property_status_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property_Types]    Script Date: 10/23/2018 5:23:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property_Types](
	[property_type_code] [nchar](15) NOT NULL,
	[property_type_desc] [nvarchar](255) NULL,
 CONSTRAINT [PK_Property_Types] PRIMARY KEY CLUSTERED 
(
	[property_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Qualifications]    Script Date: 10/23/2018 5:23:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Qualifications](
	[qualification_code] [nchar](15) NOT NULL,
	[qualification_title] [nvarchar](50) NULL,
	[qualification_desc] [nvarchar](150) NULL,
 CONSTRAINT [PK_Qualifications] PRIMARY KEY CLUSTERED 
(
	[qualification_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Readiness_for_Procure_Categories]    Script Date: 10/23/2018 5:23:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Readiness_for_Procure_Categories](
	[readiness_for_procure_category_code] [nchar](15) NOT NULL,
	[readiness_for_procure_category_name] [nvarchar](250) NULL,
 CONSTRAINT [PK_Readiness_for_Procure_Categories] PRIMARY KEY CLUSTERED 
(
	[readiness_for_procure_category_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Readiness_for_Procure_Checklist_Items]    Script Date: 10/23/2018 5:23:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Readiness_for_Procure_Checklist_Items](
	[readiness_for_procure_checklist_item_id] [int] NOT NULL,
	[readiness_for_procure_category_code] [nchar](15) NULL,
	[readiness_for_procure_checklist_item_name] [nvarchar](250) NULL,
 CONSTRAINT [PK_Readiness_for_Procure_Checklist_Items] PRIMARY KEY CLUSTERED 
(
	[readiness_for_procure_checklist_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Readiness_for_Procure_Percent]    Script Date: 10/23/2018 5:23:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Readiness_for_Procure_Percent](
	[readiness_for_procure_percent_id] [int] NOT NULL,
	[property_id] [int] NULL,
	[readiness_for_procure_percent_details] [nvarchar](250) NULL,
 CONSTRAINT [PK_Readiness_for_Procure_Percent] PRIMARY KEY CLUSTERED 
(
	[readiness_for_procure_percent_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Relationship_Types]    Script Date: 10/23/2018 5:23:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Relationship_Types](
	[relationship_type_code] [nchar](15) NOT NULL,
	[relationship_type_desc] [nvarchar](150) NULL,
 CONSTRAINT [PK_Relationship_Types] PRIMARY KEY CLUSTERED 
(
	[relationship_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Restaurants]    Script Date: 10/23/2018 5:23:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Restaurants](
	[restaurant_id] [int] NOT NULL,
	[food_type_code] [nchar](15) NULL,
	[no_smoking_area_yn] [bit] NULL,
	[phone_number] [nvarchar](50) NULL,
	[restaurant_address] [nvarchar](250) NULL,
	[city_id] [int] NULL,
	[other_details] [nvarchar](250) NULL,
 CONSTRAINT [PK_Restaurants] PRIMARY KEY CLUSTERED 
(
	[restaurant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 10/23/2018 5:23:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[role_name] [nvarchar](50) NOT NULL,
	[active_yn] [bit] NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Room_Types]    Script Date: 10/23/2018 5:23:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room_Types](
	[room_type_code] [nvarchar](15) NOT NULL,
	[room_type] [nvarchar](250) NULL,
 CONSTRAINT [PK_Room_Types] PRIMARY KEY CLUSTERED 
(
	[room_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Standard_Document_Types]    Script Date: 10/23/2018 5:23:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Standard_Document_Types](
	[document_type_code] [nchar](15) NOT NULL,
	[document_type_name] [nvarchar](150) NULL,
	[document_type_desc] [nvarchar](250) NULL,
 CONSTRAINT [PK_Standard_Document_Types] PRIMARY KEY CLUSTERED 
(
	[document_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Standard_Documents]    Script Date: 10/23/2018 5:23:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Standard_Documents](
	[document_id] [int] NOT NULL,
	[document_type_code] [nchar](15) NULL,
	[data_profile_id] [int] NULL,
	[document_details] [varchar](8000) NULL,
 CONSTRAINT [PK_Documents] PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Star_Ratings]    Script Date: 10/23/2018 5:23:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Star_Ratings](
	[star_rating_code] [nchar](15) NOT NULL,
	[star_rating_image_loc] [nvarchar](150) NULL,
 CONSTRAINT [PK_Star_Ratings] PRIMARY KEY CLUSTERED 
(
	[star_rating_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student_Addresses]    Script Date: 10/23/2018 5:23:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_Addresses](
	[student_address_id] [int] NOT NULL,
	[student_id] [int] NOT NULL,
	[address_id] [int] NULL,
	[address_type_code] [nchar](15) NULL,
	[date_address_from] [datetime] NULL,
	[date_address_to] [datetime] NULL,
 CONSTRAINT [PK_Student_Addresses] PRIMARY KEY CLUSTERED 
(
	[student_address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student_Departments]    Script Date: 10/23/2018 5:23:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_Departments](
	[student_department_id] [int] NOT NULL,
	[student_department_code] [nchar](10) NOT NULL,
	[department_name] [nvarchar](200) NOT NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_Student_Departments] PRIMARY KEY CLUSTERED 
(
	[student_department_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student_Education]    Script Date: 10/23/2018 5:23:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_Education](
	[student_id] [int] NOT NULL,
	[degree_type] [nvarchar](50) NULL,
	[degree_level] [nvarchar](50) NULL,
	[degree_time] [nvarchar](50) NULL,
	[startyear] [nchar](10) NULL,
	[endyear] [nchar](10) NULL,
	[department_id] [int] NULL,
	[programname] [nvarchar](50) NULL,
	[stream] [nvarchar](50) NULL,
	[branch] [nvarchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student_Payment_Methods]    Script Date: 10/23/2018 5:23:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_Payment_Methods](
	[student_payment_method_id] [int] NOT NULL,
	[payment_method_code] [nchar](15) NULL,
	[student_id] [int] NULL,
	[bank_details] [nvarchar](150) NULL,
	[card_details] [nvarchar](150) NULL,
	[other_details] [nvarchar](150) NULL,
 CONSTRAINT [PK_Student_Payment_Methods] PRIMARY KEY CLUSTERED 
(
	[student_payment_method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student_Relationships]    Script Date: 10/23/2018 5:23:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_Relationships](
	[student_id] [int] NOT NULL,
	[person_id] [int] NOT NULL,
	[relationship_type_code] [nchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Students]    Script Date: 10/23/2018 5:23:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Students](
	[student_id] [int] NOT NULL,
	[password] [nvarchar](50) NULL,
	[firstname] [nvarchar](50) NULL,
	[middlename] [nvarchar](50) NULL,
	[dateofbirth] [date] NULL,
	[gender] [nchar](2) NULL,
	[place_of_birth] [nvarchar](50) NULL,
	[hometown] [nvarchar](50) NULL,
	[nationality] [nvarchar](50) NULL,
	[mobilenumber] [nvarchar](20) NULL,
	[ssn] [nvarchar](50) NULL,
	[picture] [nvarchar](400) NULL,
	[Isundergrad] [bit] NULL,
	[EnrollDate] [date] NULL,
 CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED 
(
	[student_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Supplier_Addresses]    Script Date: 10/23/2018 5:23:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier_Addresses](
	[supplier_address_id] [int] IDENTITY(1,1) NOT NULL,
	[supplier_code] [nchar](15) NOT NULL,
	[address_id] [int] NULL,
	[address_type_code] [nchar](15) NULL,
	[date_address_from] [datetime] NULL,
	[date_address_to] [datetime] NULL,
 CONSTRAINT [PK_Supplier_Addresses] PRIMARY KEY CLUSTERED 
(
	[supplier_address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Suppliers]    Script Date: 10/23/2018 5:23:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Suppliers](
	[supplier_id] [int] IDENTITY(1,1) NOT NULL,
	[supplier_code] [nchar](15) NOT NULL,
	[supplier_name] [nvarchar](50) NULL,
	[supplier_email] [nvarchar](50) NULL,
	[supplier_phone] [nvarchar](50) NULL,
	[supplier_cellphone] [nvarchar](50) NULL,
 CONSTRAINT [PK_Suppliers] PRIMARY KEY CLUSTERED 
(
	[supplier_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Types_of_Food]    Script Date: 10/23/2018 5:23:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Types_of_Food](
	[food_type_code] [nchar](15) NOT NULL,
	[food_type_description] [nvarchar](250) NULL,
 CONSTRAINT [PK_Types_of_Food] PRIMARY KEY CLUSTERED 
(
	[food_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VW_CATEGORYWISE_ITEMS]    Script Date: 10/23/2018 5:23:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_CATEGORYWISE_ITEMS] AS 
SELECT IC.[ITEM_CATEGORY_ID]
      ,IC.[ITEM_CATEOGRY_CODE]
      ,IC.[ITEM_CATEGORY_NAME]
	  ,ISC.[ITEM_CATEGORY_SUB_ID]
      ,ISC.[ITEM_CATEOGRY_SUB_CODE]
      --,ISC.[ITEM_CATEGORY_ID]
      ,ISC.[SUB_CATEGORY_NAME]
      ,CI.[CATEGORY_ITEM_ID]
      ,CI.[CATEGORY_ITEM_CODE]
      --,CI.[ITEM_CATEGORY_ID]
      --,CI.[ITEM_CATEGORY_SUB_ID]
      ,CI.[CATEGORY_ITEM_NAME]
  
  FROM [dbo].[ITEM_CATEGORIES] IC 
	INNER JOIN [dbo].[ITEM_SUB_CATEGORIES] ISC 
		ON IC.ITEM_CATEGORY_ID = ISC.ITEM_CATEGORY_ID
	INNER JOIN [dbo].[CATEGORY_ITEMS] CI
		ON IC.ITEM_CATEGORY_ID = CI.ITEM_CATEGORY_ID
			AND ISC.ITEM_CATEGORY_SUB_ID = CI.ITEM_CATEGORY_SUB_ID


GO
/****** Object:  View [dbo].[VW_HK_CATEGORYWISE_ITEMS]    Script Date: 10/23/2018 5:23:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_HK_CATEGORYWISE_ITEMS] AS 
SELECT IC.[ITEM_CATEGORY_ID]
      ,IC.[ITEM_CATEOGRY_CODE]
      ,IC.[ITEM_CATEGORY_NAME]
	  ,ISC.[ITEM_CATEGORY_SUB_ID]
      ,ISC.[ITEM_CATEOGRY_SUB_CODE]
      --,ISC.[ITEM_CATEGORY_ID]
      ,ISC.[SUB_CATEGORY_NAME]
      ,CI.[CATEGORY_ITEM_ID]
      ,CI.[CATEGORY_ITEM_CODE]
      --,CI.[ITEM_CATEGORY_ID]
      --,CI.[ITEM_CATEGORY_SUB_ID]
      ,CI.[CATEGORY_ITEM_NAME]
  
  FROM [dbo].[ITEM_CATEGORIES] IC 
	INNER JOIN [dbo].[ITEM_SUB_CATEGORIES] ISC 
		ON IC.ITEM_CATEGORY_ID = ISC.ITEM_CATEGORY_ID
	INNER JOIN [dbo].[CATEGORY_ITEMS] CI
		ON IC.ITEM_CATEGORY_ID = CI.ITEM_CATEGORY_ID
			AND ISC.ITEM_CATEGORY_SUB_ID = CI.ITEM_CATEGORY_SUB_ID
	WHERE IC.[ITEM_CATEGORY_NAME] = 'Housekeeping Material'

GO
INSERT [dbo].[Address_Types] ([address_type_code], [address_type_desc]) VALUES (N'GUEST HOUSE    ', N'GUEST HOUSE ADDRESS')
INSERT [dbo].[Address_Types] ([address_type_code], [address_type_desc]) VALUES (N'HOME           ', N'HOME ADDRESS')
INSERT [dbo].[Address_Types] ([address_type_code], [address_type_desc]) VALUES (N'OFFICE         ', N'OFFICE ADDRESS')
INSERT [dbo].[Addresses] ([address_id], [address], [city], [state], [country], [pincode], [email], [mobile_no], [other_address_details]) VALUES (12, N'PLOT NO 284 STREET NO 8 MAHADIPATNAM', N'HYDERABAD', N'TELANGANA', N'INDIA', N'500034', N'sairaj@gamil.com', N'998989898', N'none')
INSERT [dbo].[Addresses] ([address_id], [address], [city], [state], [country], [pincode], [email], [mobile_no], [other_address_details]) VALUES (13, N'PLOT NO 2284 STREET NO 10', N'SECUNDERABAD', N'TELANGANA', N'INDIA', N'500034', N'tiwarirajendra@gamil.com', N'9999595898', N'none')
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (1, 1, N'Parking', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (2, 2, N'Internet/Wi-Fi', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (3, 3, N'Restaurant/Bar', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (4, 4, N'Business Facilities', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (5, 5, N'Swimming Pool', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (6, 6, N'Spa/Message/Wellness', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (7, 7, N'Gym', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (8, 8, N'Mess', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (9, 9, N'One Story', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (10, 10, N'Two Stories', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (11, 11, N'Three+ Stories', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (12, 12, N'Contractor run Mess', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (13, 13, N'Cooperative Mess', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (14, 14, N'Cafe / Canteens', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (15, 15, N'Snack Bar', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (16, 16, N'Coffee Parlour (Nestle)', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (17, 17, N'Coffee Kiosk', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (18, 18, N'Juice Shop', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (19, 19, N'General Store', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (20, 20, N'Tailor Shop', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (21, 21, N'Barber Shop', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (22, 22, N'Beauty Parlour', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (23, 23, N'Dhobi Shop', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (24, 24, N'STD/ISD/PCO Booth', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (25, 25, N'Photostat Shop', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (26, 26, N'Computer Typing Shop', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (27, 27, N'Book / Stationery Shop', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (28, 28, N'Cable Network', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (29, 29, N'Courier Service', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (30, 30, N'Mother Diary Booth', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (31, 31, N'Mother Diary Ice Cream', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (32, 32, N'Safal Vegetable Kiosk', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (33, 33, N'Delhi Milk Scheme Booth', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (34, 34, N'State Bank of India', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (35, 35, N'Post Office', NULL, 1)
INSERT [dbo].[Amenities] ([amenity_id], [parent_id], [amenity_name], [amenity_type], [status]) VALUES (36, 36, N'Railway Reservation Center', NULL, 1)
INSERT [dbo].[AQ_Property] ([Property_ID], [Property_Name], [Property_Address], [No_of_Floors], [Location_Score], [Comment_on_Location_Score], [Floor_Beds], [Room_Beds], [Room_Size], [Room_Quality_Score_ID], [Room_Cleanliness_Score_ID], [Water_Source_ID], [Water_Storage_Capacity_ID], [Emergency_Staircase], [Lift_ID], [Diesel_Generator_ID], [Common_Area_Cleanliness_ID], [Parking_Availability], [Parking_Vehicle_Count], [House_Keeping_staff_Rooms], [Age_of_the_Property], [Owner_Detail_ID], [longitute], [latitute], [IsCompetatorProperty], [IsNewProperty], [Gender], [Rent_Deposit_ID], [No_of_Beds], [Property_Occupancy_Estimate], [Room_wise_Power_Meter_ID], [Room_Cleanliness_ID], [Reception_Area_yn], [Wifi_Reception], [Security_Feature_IDs], [Fire_Fighting_Equipment_IDs], [ISP_Provider_Name], [IsCanteenExists], [Canteen_Cleanliness_ID], [Food_Availability_ID], [Is_Exisiting_Hostel], [Food_Score], [Operator_Number], [Available_Placio], [Property_Alias_Name], [IsCheckListDone], [CheckListApprovedDate], [Property_Amenities_ID], [City], [State], [Pincode], [Published_yn], [Published_on], [Published_by], [Placio_Property_ID]) VALUES (18, N'cool guys', N'Shaster Building', 5, 6, N'', 0, 0, 10, 14, 17, 20, 23, 1, 18, 18, 32, 1, 0, 1, 53, 18, N'78.44283007085323', N'17.469060978447008', 0, 1, 0, 0, 0, N'', 0, 0, 0, 0, NULL, N'', N'', 0, 0, 0, 0, 0, N'', 0, N'', 0, NULL, N'57,58,60', NULL, NULL, NULL, 0, NULL, NULL, NULL)
INSERT [dbo].[AQ_Property_Block] ([property_block_id], [property_id], [block_name], [no_of_rooms]) VALUES (12, 12, N'Block-1', 50)
INSERT [dbo].[AQ_Property_Block] ([property_block_id], [property_id], [block_name], [no_of_rooms]) VALUES (13, 13, N'Block-1', 50)
INSERT [dbo].[AQ_Property_Block] ([property_block_id], [property_id], [block_name], [no_of_rooms]) VALUES (14, 14, N'Block-1', 50)
INSERT [dbo].[AQ_Property_Block] ([property_block_id], [property_id], [block_name], [no_of_rooms]) VALUES (15, 15, N'Block-1', 50)
INSERT [dbo].[AQ_Property_Block] ([property_block_id], [property_id], [block_name], [no_of_rooms]) VALUES (16, 16, N'Block-1', 50)
INSERT [dbo].[AQ_Property_Block] ([property_block_id], [property_id], [block_name], [no_of_rooms]) VALUES (17, 17, N'Block-1', 50)
INSERT [dbo].[AQ_Property_Block] ([property_block_id], [property_id], [block_name], [no_of_rooms]) VALUES (18, 18, N'Block-1', 50)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (51, 12, 12, NULL, N'1', 2)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (52, 12, 12, NULL, N'2', 2)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (53, 12, 12, NULL, N'3', 2)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (54, 12, 12, NULL, N'4', 2)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (55, 12, 12, NULL, N'5', 2)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (56, 13, 13, NULL, N'1', 8)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (57, 13, 13, NULL, N'2', 6)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (58, 13, 13, NULL, N'3', 4)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (59, 13, 13, NULL, N'4', 2)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (60, 14, 14, NULL, N'1', 5)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (61, 14, 14, NULL, N'2', 4)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (62, 14, 14, NULL, N'3', 3)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (63, 15, 15, NULL, N'1', 10)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (64, 15, 15, NULL, N'2', 10)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (65, 16, 16, NULL, N'1', 5)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (66, 16, 16, NULL, N'2', 5)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (67, 16, 16, NULL, N'3', 5)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (68, 16, 16, NULL, N'4', 5)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (69, 17, 17, NULL, N'0', NULL)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (70, 18, 18, NULL, N'1', 2)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (71, 18, 18, NULL, N'2', 2)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (72, 18, 18, NULL, N'3', 2)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (73, 18, 18, NULL, N'4', 2)
INSERT [dbo].[AQ_Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms]) VALUES (74, 18, 18, NULL, N'5', 2)
SET IDENTITY_INSERT [dbo].[AQ_Property_Canteen_Photos] ON 

INSERT [dbo].[AQ_Property_Canteen_Photos] ([Property_Canteen_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (1, 12, N'', 1)
INSERT [dbo].[AQ_Property_Canteen_Photos] ([Property_Canteen_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (2, 13, N'', 1)
INSERT [dbo].[AQ_Property_Canteen_Photos] ([Property_Canteen_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (3, 14, N'', 1)
INSERT [dbo].[AQ_Property_Canteen_Photos] ([Property_Canteen_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (4, 15, N'', 1)
INSERT [dbo].[AQ_Property_Canteen_Photos] ([Property_Canteen_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (5, 16, N'', 1)
INSERT [dbo].[AQ_Property_Canteen_Photos] ([Property_Canteen_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (6, 17, N'', 1)
INSERT [dbo].[AQ_Property_Canteen_Photos] ([Property_Canteen_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (7, 18, N'', 1)
SET IDENTITY_INSERT [dbo].[AQ_Property_Canteen_Photos] OFF
SET IDENTITY_INSERT [dbo].[AQ_Property_Common_Area_Cleanliness_Photos] ON 

INSERT [dbo].[AQ_Property_Common_Area_Cleanliness_Photos] ([Property_Common_Area_Cleanliness_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (1, 12, N'D:\Kristab NewSource\PlacioServices\NewPropertyImages\AreaDetails\PLACIO123\AreaImages10222018172529', 1)
INSERT [dbo].[AQ_Property_Common_Area_Cleanliness_Photos] ([Property_Common_Area_Cleanliness_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (2, 13, N'', 1)
INSERT [dbo].[AQ_Property_Common_Area_Cleanliness_Photos] ([Property_Common_Area_Cleanliness_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (3, 14, N'', 1)
INSERT [dbo].[AQ_Property_Common_Area_Cleanliness_Photos] ([Property_Common_Area_Cleanliness_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (4, 15, N'', 1)
INSERT [dbo].[AQ_Property_Common_Area_Cleanliness_Photos] ([Property_Common_Area_Cleanliness_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (5, 16, N'', 1)
INSERT [dbo].[AQ_Property_Common_Area_Cleanliness_Photos] ([Property_Common_Area_Cleanliness_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (6, 17, N'', 1)
INSERT [dbo].[AQ_Property_Common_Area_Cleanliness_Photos] ([Property_Common_Area_Cleanliness_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (7, 18, N'D:\Kristab NewSource\PlacioServices\NewPropertyImages\AreaDetails\PLACIO123\AreaImages10232018122217', 1)
SET IDENTITY_INSERT [dbo].[AQ_Property_Common_Area_Cleanliness_Photos] OFF
INSERT [dbo].[AQ_Property_Diesel_Generator] ([Diesel_Generator_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost], [storage_capacity]) VALUES (12, 12, 8, N'giify', N'2', CAST(894646.67 AS Decimal(15, 2)), N'894646')
INSERT [dbo].[AQ_Property_Diesel_Generator] ([Diesel_Generator_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost], [storage_capacity]) VALUES (13, 13, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)), N'')
INSERT [dbo].[AQ_Property_Diesel_Generator] ([Diesel_Generator_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost], [storage_capacity]) VALUES (14, 14, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)), N'')
INSERT [dbo].[AQ_Property_Diesel_Generator] ([Diesel_Generator_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost], [storage_capacity]) VALUES (15, 15, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)), N'')
INSERT [dbo].[AQ_Property_Diesel_Generator] ([Diesel_Generator_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost], [storage_capacity]) VALUES (16, 16, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)), N'')
INSERT [dbo].[AQ_Property_Diesel_Generator] ([Diesel_Generator_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost], [storage_capacity]) VALUES (17, 17, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)), N'')
INSERT [dbo].[AQ_Property_Diesel_Generator] ([Diesel_Generator_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost], [storage_capacity]) VALUES (18, 18, 8, N'giify', N'2', CAST(894646.67 AS Decimal(15, 2)), N'894646')
SET IDENTITY_INSERT [dbo].[AQ_Property_Diesel_Generator_Photos] ON 

INSERT [dbo].[AQ_Property_Diesel_Generator_Photos] ([Property_Diesel_Generator_Photo_ID], [Property_ID], [Diesel_Generator_ID], [PhotoLocation], [Seq_ID]) VALUES (8, 12, 12, N'', 1)
INSERT [dbo].[AQ_Property_Diesel_Generator_Photos] ([Property_Diesel_Generator_Photo_ID], [Property_ID], [Diesel_Generator_ID], [PhotoLocation], [Seq_ID]) VALUES (9, 13, 13, N'', 1)
INSERT [dbo].[AQ_Property_Diesel_Generator_Photos] ([Property_Diesel_Generator_Photo_ID], [Property_ID], [Diesel_Generator_ID], [PhotoLocation], [Seq_ID]) VALUES (10, 14, 14, N'', 1)
INSERT [dbo].[AQ_Property_Diesel_Generator_Photos] ([Property_Diesel_Generator_Photo_ID], [Property_ID], [Diesel_Generator_ID], [PhotoLocation], [Seq_ID]) VALUES (11, 15, 15, N'', 1)
INSERT [dbo].[AQ_Property_Diesel_Generator_Photos] ([Property_Diesel_Generator_Photo_ID], [Property_ID], [Diesel_Generator_ID], [PhotoLocation], [Seq_ID]) VALUES (12, 16, 16, N'', 1)
INSERT [dbo].[AQ_Property_Diesel_Generator_Photos] ([Property_Diesel_Generator_Photo_ID], [Property_ID], [Diesel_Generator_ID], [PhotoLocation], [Seq_ID]) VALUES (13, 17, 17, N'', 1)
INSERT [dbo].[AQ_Property_Diesel_Generator_Photos] ([Property_Diesel_Generator_Photo_ID], [Property_ID], [Diesel_Generator_ID], [PhotoLocation], [Seq_ID]) VALUES (14, 18, 18, N'', 1)
SET IDENTITY_INSERT [dbo].[AQ_Property_Diesel_Generator_Photos] OFF
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280539, 12, 12, 51, N'BLOCK-1-FLOOR-ROOM-33', N'BLOCK-1-FLOOR-ROOM-51BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280540, 12, 12, 51, N'BLOCK-1-FLOOR-ROOM-33', N'BLOCK-1-FLOOR-ROOM-51BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280541, 12, 12, 51, N'BLOCK-1-FLOOR-ROOM-33', N'BLOCK-1-FLOOR-ROOM-51BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280542, 12, 12, 51, N'BLOCK-1-FLOOR-ROOM-34', N'BLOCK-1-FLOOR-ROOM-51BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280543, 12, 12, 51, N'BLOCK-1-FLOOR-ROOM-34', N'BLOCK-1-FLOOR-ROOM-51BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280544, 12, 12, 51, N'BLOCK-1-FLOOR-ROOM-34', N'BLOCK-1-FLOOR-ROOM-51BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280545, 12, 12, 52, N'BLOCK-1-FLOOR-ROOM-35', N'BLOCK-1-FLOOR-ROOM-52BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280546, 12, 12, 52, N'BLOCK-1-FLOOR-ROOM-35', N'BLOCK-1-FLOOR-ROOM-52BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280547, 12, 12, 52, N'BLOCK-1-FLOOR-ROOM-35', N'BLOCK-1-FLOOR-ROOM-52BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280548, 12, 12, 52, N'BLOCK-1-FLOOR-ROOM-36', N'BLOCK-1-FLOOR-ROOM-52BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280549, 12, 12, 52, N'BLOCK-1-FLOOR-ROOM-36', N'BLOCK-1-FLOOR-ROOM-52BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280550, 12, 12, 52, N'BLOCK-1-FLOOR-ROOM-36', N'BLOCK-1-FLOOR-ROOM-52BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280551, 12, 12, 53, N'BLOCK-1-FLOOR-ROOM-37', N'BLOCK-1-FLOOR-ROOM-53BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280552, 12, 12, 53, N'BLOCK-1-FLOOR-ROOM-37', N'BLOCK-1-FLOOR-ROOM-53BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280553, 12, 12, 53, N'BLOCK-1-FLOOR-ROOM-37', N'BLOCK-1-FLOOR-ROOM-53BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280554, 12, 12, 53, N'BLOCK-1-FLOOR-ROOM-38', N'BLOCK-1-FLOOR-ROOM-53BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280555, 12, 12, 53, N'BLOCK-1-FLOOR-ROOM-38', N'BLOCK-1-FLOOR-ROOM-53BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280556, 12, 12, 53, N'BLOCK-1-FLOOR-ROOM-38', N'BLOCK-1-FLOOR-ROOM-53BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280557, 12, 12, 54, N'BLOCK-1-FLOOR-ROOM-39', N'BLOCK-1-FLOOR-ROOM-54BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280558, 12, 12, 54, N'BLOCK-1-FLOOR-ROOM-39', N'BLOCK-1-FLOOR-ROOM-54BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280559, 12, 12, 54, N'BLOCK-1-FLOOR-ROOM-39', N'BLOCK-1-FLOOR-ROOM-54BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280560, 12, 12, 54, N'BLOCK-1-FLOOR-ROOM-40', N'BLOCK-1-FLOOR-ROOM-54BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280561, 12, 12, 54, N'BLOCK-1-FLOOR-ROOM-40', N'BLOCK-1-FLOOR-ROOM-54BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280562, 12, 12, 54, N'BLOCK-1-FLOOR-ROOM-40', N'BLOCK-1-FLOOR-ROOM-54BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280563, 12, 12, 55, N'BLOCK-1-FLOOR-ROOM-41', N'BLOCK-1-FLOOR-ROOM-55BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280564, 12, 12, 55, N'BLOCK-1-FLOOR-ROOM-41', N'BLOCK-1-FLOOR-ROOM-55BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280565, 12, 12, 55, N'BLOCK-1-FLOOR-ROOM-41', N'BLOCK-1-FLOOR-ROOM-55BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280566, 12, 12, 55, N'BLOCK-1-FLOOR-ROOM-42', N'BLOCK-1-FLOOR-ROOM-55BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280567, 12, 12, 55, N'BLOCK-1-FLOOR-ROOM-42', N'BLOCK-1-FLOOR-ROOM-55BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280568, 12, 12, 55, N'BLOCK-1-FLOOR-ROOM-42', N'BLOCK-1-FLOOR-ROOM-55BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280569, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-43', N'BLOCK-1-FLOOR-ROOM-56BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280570, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-43', N'BLOCK-1-FLOOR-ROOM-56BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280571, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-43', N'BLOCK-1-FLOOR-ROOM-56BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280572, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-44', N'BLOCK-1-FLOOR-ROOM-56BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280573, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-44', N'BLOCK-1-FLOOR-ROOM-56BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280574, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-44', N'BLOCK-1-FLOOR-ROOM-56BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280575, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-45', N'BLOCK-1-FLOOR-ROOM-56BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280576, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-45', N'BLOCK-1-FLOOR-ROOM-56BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280577, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-45', N'BLOCK-1-FLOOR-ROOM-56BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280578, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-46', N'BLOCK-1-FLOOR-ROOM-56BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280579, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-46', N'BLOCK-1-FLOOR-ROOM-56BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280580, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-46', N'BLOCK-1-FLOOR-ROOM-56BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280581, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-47', N'BLOCK-1-FLOOR-ROOM-56BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280582, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-47', N'BLOCK-1-FLOOR-ROOM-56BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280583, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-47', N'BLOCK-1-FLOOR-ROOM-56BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280584, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-48', N'BLOCK-1-FLOOR-ROOM-56BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280585, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-48', N'BLOCK-1-FLOOR-ROOM-56BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280586, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-48', N'BLOCK-1-FLOOR-ROOM-56BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280587, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-49', N'BLOCK-1-FLOOR-ROOM-56BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280588, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-49', N'BLOCK-1-FLOOR-ROOM-56BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280589, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-49', N'BLOCK-1-FLOOR-ROOM-56BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280590, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-50', N'BLOCK-1-FLOOR-ROOM-56BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280591, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-50', N'BLOCK-1-FLOOR-ROOM-56BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280592, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-50', N'BLOCK-1-FLOOR-ROOM-56BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280593, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-51', N'BLOCK-1-FLOOR-ROOM-57BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280594, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-51', N'BLOCK-1-FLOOR-ROOM-57BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280595, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-51', N'BLOCK-1-FLOOR-ROOM-57BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280596, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-52', N'BLOCK-1-FLOOR-ROOM-57BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280597, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-52', N'BLOCK-1-FLOOR-ROOM-57BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280598, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-52', N'BLOCK-1-FLOOR-ROOM-57BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280599, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-53', N'BLOCK-1-FLOOR-ROOM-57BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280600, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-53', N'BLOCK-1-FLOOR-ROOM-57BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280601, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-53', N'BLOCK-1-FLOOR-ROOM-57BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280602, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-54', N'BLOCK-1-FLOOR-ROOM-57BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280603, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-54', N'BLOCK-1-FLOOR-ROOM-57BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280604, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-54', N'BLOCK-1-FLOOR-ROOM-57BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280605, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-55', N'BLOCK-1-FLOOR-ROOM-57BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280606, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-55', N'BLOCK-1-FLOOR-ROOM-57BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280607, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-55', N'BLOCK-1-FLOOR-ROOM-57BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280608, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-56', N'BLOCK-1-FLOOR-ROOM-57BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280609, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-56', N'BLOCK-1-FLOOR-ROOM-57BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280610, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-56', N'BLOCK-1-FLOOR-ROOM-57BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280611, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-57', N'BLOCK-1-FLOOR-ROOM-58BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280612, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-57', N'BLOCK-1-FLOOR-ROOM-58BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280613, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-57', N'BLOCK-1-FLOOR-ROOM-58BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280614, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-58', N'BLOCK-1-FLOOR-ROOM-58BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280615, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-58', N'BLOCK-1-FLOOR-ROOM-58BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280616, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-58', N'BLOCK-1-FLOOR-ROOM-58BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280617, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-59', N'BLOCK-1-FLOOR-ROOM-58BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280618, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-59', N'BLOCK-1-FLOOR-ROOM-58BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280619, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-59', N'BLOCK-1-FLOOR-ROOM-58BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280620, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-60', N'BLOCK-1-FLOOR-ROOM-58BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280621, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-60', N'BLOCK-1-FLOOR-ROOM-58BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280622, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-60', N'BLOCK-1-FLOOR-ROOM-58BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280623, 13, 13, 59, N'BLOCK-1-FLOOR-ROOM-61', N'BLOCK-1-FLOOR-ROOM-59BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280624, 13, 13, 59, N'BLOCK-1-FLOOR-ROOM-61', N'BLOCK-1-FLOOR-ROOM-59BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280625, 13, 13, 59, N'BLOCK-1-FLOOR-ROOM-61', N'BLOCK-1-FLOOR-ROOM-59BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280626, 13, 13, 59, N'BLOCK-1-FLOOR-ROOM-62', N'BLOCK-1-FLOOR-ROOM-59BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280627, 13, 13, 59, N'BLOCK-1-FLOOR-ROOM-62', N'BLOCK-1-FLOOR-ROOM-59BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280628, 13, 13, 59, N'BLOCK-1-FLOOR-ROOM-62', N'BLOCK-1-FLOOR-ROOM-59BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280629, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-63', N'BLOCK-1-FLOOR-ROOM-60BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280630, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-63', N'BLOCK-1-FLOOR-ROOM-60BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280631, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-63', N'BLOCK-1-FLOOR-ROOM-60BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280632, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-64', N'BLOCK-1-FLOOR-ROOM-60BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280633, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-64', N'BLOCK-1-FLOOR-ROOM-60BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280634, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-64', N'BLOCK-1-FLOOR-ROOM-60BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280635, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-65', N'BLOCK-1-FLOOR-ROOM-60BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280636, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-65', N'BLOCK-1-FLOOR-ROOM-60BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280637, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-65', N'BLOCK-1-FLOOR-ROOM-60BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280638, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-66', N'BLOCK-1-FLOOR-ROOM-60BED-1', 100, 0)
GO
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280639, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-66', N'BLOCK-1-FLOOR-ROOM-60BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280640, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-66', N'BLOCK-1-FLOOR-ROOM-60BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280641, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-67', N'BLOCK-1-FLOOR-ROOM-60BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280642, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-67', N'BLOCK-1-FLOOR-ROOM-60BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280643, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-67', N'BLOCK-1-FLOOR-ROOM-60BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280644, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-68', N'BLOCK-1-FLOOR-ROOM-61BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280645, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-68', N'BLOCK-1-FLOOR-ROOM-61BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280646, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-68', N'BLOCK-1-FLOOR-ROOM-61BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280647, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-69', N'BLOCK-1-FLOOR-ROOM-61BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280648, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-69', N'BLOCK-1-FLOOR-ROOM-61BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280649, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-69', N'BLOCK-1-FLOOR-ROOM-61BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280650, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-70', N'BLOCK-1-FLOOR-ROOM-61BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280651, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-70', N'BLOCK-1-FLOOR-ROOM-61BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280652, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-70', N'BLOCK-1-FLOOR-ROOM-61BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280653, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-71', N'BLOCK-1-FLOOR-ROOM-61BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280654, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-71', N'BLOCK-1-FLOOR-ROOM-61BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280655, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-71', N'BLOCK-1-FLOOR-ROOM-61BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280656, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-72', N'BLOCK-1-FLOOR-ROOM-62BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280657, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-72', N'BLOCK-1-FLOOR-ROOM-62BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280658, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-72', N'BLOCK-1-FLOOR-ROOM-62BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280659, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-73', N'BLOCK-1-FLOOR-ROOM-62BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280660, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-73', N'BLOCK-1-FLOOR-ROOM-62BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280661, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-73', N'BLOCK-1-FLOOR-ROOM-62BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280662, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-74', N'BLOCK-1-FLOOR-ROOM-62BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280663, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-74', N'BLOCK-1-FLOOR-ROOM-62BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280664, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-74', N'BLOCK-1-FLOOR-ROOM-62BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280665, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-75', N'BLOCK-1-FLOOR-ROOM-63BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280666, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-75', N'BLOCK-1-FLOOR-ROOM-63BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280667, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-75', N'BLOCK-1-FLOOR-ROOM-63BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280668, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-76', N'BLOCK-1-FLOOR-ROOM-63BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280669, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-76', N'BLOCK-1-FLOOR-ROOM-63BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280670, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-76', N'BLOCK-1-FLOOR-ROOM-63BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280671, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-77', N'BLOCK-1-FLOOR-ROOM-63BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280672, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-77', N'BLOCK-1-FLOOR-ROOM-63BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280673, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-77', N'BLOCK-1-FLOOR-ROOM-63BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280674, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-78', N'BLOCK-1-FLOOR-ROOM-63BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280675, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-78', N'BLOCK-1-FLOOR-ROOM-63BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280676, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-78', N'BLOCK-1-FLOOR-ROOM-63BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280677, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-79', N'BLOCK-1-FLOOR-ROOM-63BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280678, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-79', N'BLOCK-1-FLOOR-ROOM-63BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280679, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-79', N'BLOCK-1-FLOOR-ROOM-63BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280680, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-80', N'BLOCK-1-FLOOR-ROOM-63BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280681, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-80', N'BLOCK-1-FLOOR-ROOM-63BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280682, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-80', N'BLOCK-1-FLOOR-ROOM-63BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280683, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-81', N'BLOCK-1-FLOOR-ROOM-63BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280684, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-81', N'BLOCK-1-FLOOR-ROOM-63BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280685, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-81', N'BLOCK-1-FLOOR-ROOM-63BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280686, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-82', N'BLOCK-1-FLOOR-ROOM-63BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280687, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-82', N'BLOCK-1-FLOOR-ROOM-63BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280688, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-82', N'BLOCK-1-FLOOR-ROOM-63BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280689, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-83', N'BLOCK-1-FLOOR-ROOM-63BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280690, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-83', N'BLOCK-1-FLOOR-ROOM-63BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280691, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-83', N'BLOCK-1-FLOOR-ROOM-63BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280692, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-84', N'BLOCK-1-FLOOR-ROOM-63BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280693, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-84', N'BLOCK-1-FLOOR-ROOM-63BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280694, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-84', N'BLOCK-1-FLOOR-ROOM-63BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280695, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-85', N'BLOCK-1-FLOOR-ROOM-64BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280696, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-85', N'BLOCK-1-FLOOR-ROOM-64BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280697, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-85', N'BLOCK-1-FLOOR-ROOM-64BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280698, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-86', N'BLOCK-1-FLOOR-ROOM-64BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280699, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-86', N'BLOCK-1-FLOOR-ROOM-64BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280700, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-86', N'BLOCK-1-FLOOR-ROOM-64BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280701, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-87', N'BLOCK-1-FLOOR-ROOM-64BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280702, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-87', N'BLOCK-1-FLOOR-ROOM-64BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280703, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-87', N'BLOCK-1-FLOOR-ROOM-64BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280704, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-88', N'BLOCK-1-FLOOR-ROOM-64BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280705, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-88', N'BLOCK-1-FLOOR-ROOM-64BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280706, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-88', N'BLOCK-1-FLOOR-ROOM-64BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280707, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-89', N'BLOCK-1-FLOOR-ROOM-64BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280708, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-89', N'BLOCK-1-FLOOR-ROOM-64BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280709, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-89', N'BLOCK-1-FLOOR-ROOM-64BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280710, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-90', N'BLOCK-1-FLOOR-ROOM-64BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280711, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-90', N'BLOCK-1-FLOOR-ROOM-64BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280712, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-90', N'BLOCK-1-FLOOR-ROOM-64BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280713, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-91', N'BLOCK-1-FLOOR-ROOM-64BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280714, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-91', N'BLOCK-1-FLOOR-ROOM-64BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280715, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-91', N'BLOCK-1-FLOOR-ROOM-64BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280716, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-92', N'BLOCK-1-FLOOR-ROOM-64BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280717, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-92', N'BLOCK-1-FLOOR-ROOM-64BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280718, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-92', N'BLOCK-1-FLOOR-ROOM-64BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280719, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-93', N'BLOCK-1-FLOOR-ROOM-64BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280720, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-93', N'BLOCK-1-FLOOR-ROOM-64BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280721, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-93', N'BLOCK-1-FLOOR-ROOM-64BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280722, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-94', N'BLOCK-1-FLOOR-ROOM-64BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280723, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-94', N'BLOCK-1-FLOOR-ROOM-64BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280724, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-94', N'BLOCK-1-FLOOR-ROOM-64BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280725, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-95', N'BLOCK-1-FLOOR-ROOM-65BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280726, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-95', N'BLOCK-1-FLOOR-ROOM-65BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280727, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-95', N'BLOCK-1-FLOOR-ROOM-65BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280728, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-96', N'BLOCK-1-FLOOR-ROOM-65BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280729, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-96', N'BLOCK-1-FLOOR-ROOM-65BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280730, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-96', N'BLOCK-1-FLOOR-ROOM-65BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280731, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-97', N'BLOCK-1-FLOOR-ROOM-65BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280732, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-97', N'BLOCK-1-FLOOR-ROOM-65BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280733, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-97', N'BLOCK-1-FLOOR-ROOM-65BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280734, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-98', N'BLOCK-1-FLOOR-ROOM-65BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280735, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-98', N'BLOCK-1-FLOOR-ROOM-65BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280736, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-98', N'BLOCK-1-FLOOR-ROOM-65BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280737, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-99', N'BLOCK-1-FLOOR-ROOM-65BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280738, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-99', N'BLOCK-1-FLOOR-ROOM-65BED-2', 100, 0)
GO
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280739, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-99', N'BLOCK-1-FLOOR-ROOM-65BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280740, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-100', N'BLOCK-1-FLOOR-ROOM-66BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280741, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-100', N'BLOCK-1-FLOOR-ROOM-66BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280742, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-100', N'BLOCK-1-FLOOR-ROOM-66BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280743, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-101', N'BLOCK-1-FLOOR-ROOM-66BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280744, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-101', N'BLOCK-1-FLOOR-ROOM-66BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280745, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-101', N'BLOCK-1-FLOOR-ROOM-66BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280746, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-102', N'BLOCK-1-FLOOR-ROOM-66BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280747, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-102', N'BLOCK-1-FLOOR-ROOM-66BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280748, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-102', N'BLOCK-1-FLOOR-ROOM-66BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280749, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-103', N'BLOCK-1-FLOOR-ROOM-66BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280750, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-103', N'BLOCK-1-FLOOR-ROOM-66BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280751, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-103', N'BLOCK-1-FLOOR-ROOM-66BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280752, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-104', N'BLOCK-1-FLOOR-ROOM-66BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280753, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-104', N'BLOCK-1-FLOOR-ROOM-66BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280754, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-104', N'BLOCK-1-FLOOR-ROOM-66BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280755, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-105', N'BLOCK-1-FLOOR-ROOM-67BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280756, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-105', N'BLOCK-1-FLOOR-ROOM-67BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280757, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-105', N'BLOCK-1-FLOOR-ROOM-67BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280758, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-106', N'BLOCK-1-FLOOR-ROOM-67BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280759, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-106', N'BLOCK-1-FLOOR-ROOM-67BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280760, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-106', N'BLOCK-1-FLOOR-ROOM-67BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280761, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-107', N'BLOCK-1-FLOOR-ROOM-67BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280762, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-107', N'BLOCK-1-FLOOR-ROOM-67BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280763, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-107', N'BLOCK-1-FLOOR-ROOM-67BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280764, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-108', N'BLOCK-1-FLOOR-ROOM-67BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280765, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-108', N'BLOCK-1-FLOOR-ROOM-67BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280766, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-108', N'BLOCK-1-FLOOR-ROOM-67BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280767, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-109', N'BLOCK-1-FLOOR-ROOM-67BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280768, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-109', N'BLOCK-1-FLOOR-ROOM-67BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280769, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-109', N'BLOCK-1-FLOOR-ROOM-67BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280770, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-110', N'BLOCK-1-FLOOR-ROOM-68BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280771, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-110', N'BLOCK-1-FLOOR-ROOM-68BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280772, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-110', N'BLOCK-1-FLOOR-ROOM-68BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280773, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-111', N'BLOCK-1-FLOOR-ROOM-68BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280774, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-111', N'BLOCK-1-FLOOR-ROOM-68BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280775, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-111', N'BLOCK-1-FLOOR-ROOM-68BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280776, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-112', N'BLOCK-1-FLOOR-ROOM-68BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280777, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-112', N'BLOCK-1-FLOOR-ROOM-68BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280778, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-112', N'BLOCK-1-FLOOR-ROOM-68BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280779, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-113', N'BLOCK-1-FLOOR-ROOM-68BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280780, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-113', N'BLOCK-1-FLOOR-ROOM-68BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280781, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-113', N'BLOCK-1-FLOOR-ROOM-68BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280782, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-114', N'BLOCK-1-FLOOR-ROOM-68BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280783, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-114', N'BLOCK-1-FLOOR-ROOM-68BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280784, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-114', N'BLOCK-1-FLOOR-ROOM-68BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280785, 18, 18, 70, N'BLOCK-1-FLOOR-ROOM-115', N'BLOCK-1-FLOOR-ROOM-70BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280786, 18, 18, 70, N'BLOCK-1-FLOOR-ROOM-115', N'BLOCK-1-FLOOR-ROOM-70BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280787, 18, 18, 70, N'BLOCK-1-FLOOR-ROOM-115', N'BLOCK-1-FLOOR-ROOM-70BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280788, 18, 18, 70, N'BLOCK-1-FLOOR-ROOM-116', N'BLOCK-1-FLOOR-ROOM-70BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280789, 18, 18, 70, N'BLOCK-1-FLOOR-ROOM-116', N'BLOCK-1-FLOOR-ROOM-70BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280790, 18, 18, 70, N'BLOCK-1-FLOOR-ROOM-116', N'BLOCK-1-FLOOR-ROOM-70BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280791, 18, 18, 71, N'BLOCK-1-FLOOR-ROOM-117', N'BLOCK-1-FLOOR-ROOM-71BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280792, 18, 18, 71, N'BLOCK-1-FLOOR-ROOM-117', N'BLOCK-1-FLOOR-ROOM-71BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280793, 18, 18, 71, N'BLOCK-1-FLOOR-ROOM-117', N'BLOCK-1-FLOOR-ROOM-71BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280794, 18, 18, 71, N'BLOCK-1-FLOOR-ROOM-118', N'BLOCK-1-FLOOR-ROOM-71BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280795, 18, 18, 71, N'BLOCK-1-FLOOR-ROOM-118', N'BLOCK-1-FLOOR-ROOM-71BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280796, 18, 18, 71, N'BLOCK-1-FLOOR-ROOM-118', N'BLOCK-1-FLOOR-ROOM-71BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280797, 18, 18, 72, N'BLOCK-1-FLOOR-ROOM-119', N'BLOCK-1-FLOOR-ROOM-72BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280798, 18, 18, 72, N'BLOCK-1-FLOOR-ROOM-119', N'BLOCK-1-FLOOR-ROOM-72BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280799, 18, 18, 72, N'BLOCK-1-FLOOR-ROOM-119', N'BLOCK-1-FLOOR-ROOM-72BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280800, 18, 18, 72, N'BLOCK-1-FLOOR-ROOM-120', N'BLOCK-1-FLOOR-ROOM-72BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280801, 18, 18, 72, N'BLOCK-1-FLOOR-ROOM-120', N'BLOCK-1-FLOOR-ROOM-72BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280802, 18, 18, 72, N'BLOCK-1-FLOOR-ROOM-120', N'BLOCK-1-FLOOR-ROOM-72BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280803, 18, 18, 73, N'BLOCK-1-FLOOR-ROOM-121', N'BLOCK-1-FLOOR-ROOM-73BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280804, 18, 18, 73, N'BLOCK-1-FLOOR-ROOM-121', N'BLOCK-1-FLOOR-ROOM-73BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280805, 18, 18, 73, N'BLOCK-1-FLOOR-ROOM-121', N'BLOCK-1-FLOOR-ROOM-73BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280806, 18, 18, 73, N'BLOCK-1-FLOOR-ROOM-122', N'BLOCK-1-FLOOR-ROOM-73BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280807, 18, 18, 73, N'BLOCK-1-FLOOR-ROOM-122', N'BLOCK-1-FLOOR-ROOM-73BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280808, 18, 18, 73, N'BLOCK-1-FLOOR-ROOM-122', N'BLOCK-1-FLOOR-ROOM-73BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280809, 18, 18, 74, N'BLOCK-1-FLOOR-ROOM-123', N'BLOCK-1-FLOOR-ROOM-74BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280810, 18, 18, 74, N'BLOCK-1-FLOOR-ROOM-123', N'BLOCK-1-FLOOR-ROOM-74BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280811, 18, 18, 74, N'BLOCK-1-FLOOR-ROOM-123', N'BLOCK-1-FLOOR-ROOM-74BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280812, 18, 18, 74, N'BLOCK-1-FLOOR-ROOM-124', N'BLOCK-1-FLOOR-ROOM-74BED-1', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280813, 18, 18, 74, N'BLOCK-1-FLOOR-ROOM-124', N'BLOCK-1-FLOOR-ROOM-74BED-2', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status]) VALUES (280814, 18, 18, 74, N'BLOCK-1-FLOOR-ROOM-124', N'BLOCK-1-FLOOR-ROOM-74BED-3', 100, 0)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (100, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-100', N'ROOM NAME - 100', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (101, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-101', N'ROOM NAME - 101', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (102, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-102', N'ROOM NAME - 102', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (103, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-103', N'ROOM NAME - 103', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (104, 16, 16, 66, N'BLOCK-1-FLOOR-ROOM-104', N'ROOM NAME - 104', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (105, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-105', N'ROOM NAME - 105', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (106, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-106', N'ROOM NAME - 106', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (107, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-107', N'ROOM NAME - 107', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (108, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-108', N'ROOM NAME - 108', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (109, 16, 16, 67, N'BLOCK-1-FLOOR-ROOM-109', N'ROOM NAME - 109', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (110, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-110', N'ROOM NAME - 110', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (111, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-111', N'ROOM NAME - 111', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (112, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-112', N'ROOM NAME - 112', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (113, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-113', N'ROOM NAME - 113', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (114, 16, 16, 68, N'BLOCK-1-FLOOR-ROOM-114', N'ROOM NAME - 114', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (115, 18, 18, 70, N'BLOCK-1-FLOOR-ROOM-115', N'ROOM NAME - 115', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (116, 18, 18, 70, N'BLOCK-1-FLOOR-ROOM-116', N'ROOM NAME - 116', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (117, 18, 18, 71, N'BLOCK-1-FLOOR-ROOM-117', N'ROOM NAME - 117', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (118, 18, 18, 71, N'BLOCK-1-FLOOR-ROOM-118', N'ROOM NAME - 118', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (119, 18, 18, 72, N'BLOCK-1-FLOOR-ROOM-119', N'ROOM NAME - 119', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (120, 18, 18, 72, N'BLOCK-1-FLOOR-ROOM-120', N'ROOM NAME - 120', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (121, 18, 18, 73, N'BLOCK-1-FLOOR-ROOM-121', N'ROOM NAME - 121', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (122, 18, 18, 73, N'BLOCK-1-FLOOR-ROOM-122', N'ROOM NAME - 122', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (123, 18, 18, 74, N'BLOCK-1-FLOOR-ROOM-123', N'ROOM NAME - 123', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (124, 18, 18, 74, N'BLOCK-1-FLOOR-ROOM-124', N'ROOM NAME - 124', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (33, 12, 12, 51, N'BLOCK-1-FLOOR-ROOM-33', N'ROOM NAME - 33', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (34, 12, 12, 51, N'BLOCK-1-FLOOR-ROOM-34', N'ROOM NAME - 34', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (35, 12, 12, 52, N'BLOCK-1-FLOOR-ROOM-35', N'ROOM NAME - 35', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (36, 12, 12, 52, N'BLOCK-1-FLOOR-ROOM-36', N'ROOM NAME - 36', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (37, 12, 12, 53, N'BLOCK-1-FLOOR-ROOM-37', N'ROOM NAME - 37', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (38, 12, 12, 53, N'BLOCK-1-FLOOR-ROOM-38', N'ROOM NAME - 38', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (39, 12, 12, 54, N'BLOCK-1-FLOOR-ROOM-39', N'ROOM NAME - 39', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (40, 12, 12, 54, N'BLOCK-1-FLOOR-ROOM-40', N'ROOM NAME - 40', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (41, 12, 12, 55, N'BLOCK-1-FLOOR-ROOM-41', N'ROOM NAME - 41', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (42, 12, 12, 55, N'BLOCK-1-FLOOR-ROOM-42', N'ROOM NAME - 42', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (43, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-43', N'ROOM NAME - 43', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (44, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-44', N'ROOM NAME - 44', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (45, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-45', N'ROOM NAME - 45', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (46, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-46', N'ROOM NAME - 46', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (47, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-47', N'ROOM NAME - 47', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (48, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-48', N'ROOM NAME - 48', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (49, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-49', N'ROOM NAME - 49', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (50, 13, 13, 56, N'BLOCK-1-FLOOR-ROOM-50', N'ROOM NAME - 50', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (51, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-51', N'ROOM NAME - 51', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (52, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-52', N'ROOM NAME - 52', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (53, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-53', N'ROOM NAME - 53', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (54, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-54', N'ROOM NAME - 54', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (55, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-55', N'ROOM NAME - 55', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (56, 13, 13, 57, N'BLOCK-1-FLOOR-ROOM-56', N'ROOM NAME - 56', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (57, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-57', N'ROOM NAME - 57', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (58, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-58', N'ROOM NAME - 58', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (59, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-59', N'ROOM NAME - 59', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (60, 13, 13, 58, N'BLOCK-1-FLOOR-ROOM-60', N'ROOM NAME - 60', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (61, 13, 13, 59, N'BLOCK-1-FLOOR-ROOM-61', N'ROOM NAME - 61', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (62, 13, 13, 59, N'BLOCK-1-FLOOR-ROOM-62', N'ROOM NAME - 62', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (63, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-63', N'ROOM NAME - 63', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (64, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-64', N'ROOM NAME - 64', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (65, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-65', N'ROOM NAME - 65', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (66, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-66', N'ROOM NAME - 66', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (67, 14, 14, 60, N'BLOCK-1-FLOOR-ROOM-67', N'ROOM NAME - 67', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (68, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-68', N'ROOM NAME - 68', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (69, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-69', N'ROOM NAME - 69', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (70, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-70', N'ROOM NAME - 70', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (71, 14, 14, 61, N'BLOCK-1-FLOOR-ROOM-71', N'ROOM NAME - 71', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (72, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-72', N'ROOM NAME - 72', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (73, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-73', N'ROOM NAME - 73', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (74, 14, 14, 62, N'BLOCK-1-FLOOR-ROOM-74', N'ROOM NAME - 74', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (75, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-75', N'ROOM NAME - 75', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (76, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-76', N'ROOM NAME - 76', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (77, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-77', N'ROOM NAME - 77', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (78, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-78', N'ROOM NAME - 78', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (79, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-79', N'ROOM NAME - 79', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (80, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-80', N'ROOM NAME - 80', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (81, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-81', N'ROOM NAME - 81', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (82, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-82', N'ROOM NAME - 82', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (83, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-83', N'ROOM NAME - 83', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (84, 15, 15, 63, N'BLOCK-1-FLOOR-ROOM-84', N'ROOM NAME - 84', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (85, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-85', N'ROOM NAME - 85', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (86, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-86', N'ROOM NAME - 86', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (87, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-87', N'ROOM NAME - 87', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (88, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-88', N'ROOM NAME - 88', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (89, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-89', N'ROOM NAME - 89', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (90, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-90', N'ROOM NAME - 90', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (91, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-91', N'ROOM NAME - 91', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (92, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-92', N'ROOM NAME - 92', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (93, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-93', N'ROOM NAME - 93', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (94, 15, 15, 64, N'BLOCK-1-FLOOR-ROOM-94', N'ROOM NAME - 94', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (95, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-95', N'ROOM NAME - 95', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (96, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-96', N'ROOM NAME - 96', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (97, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-97', N'ROOM NAME - 97', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (98, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-98', N'ROOM NAME - 98', 3)
INSERT [dbo].[AQ_Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds]) VALUES (99, 16, 16, 65, N'BLOCK-1-FLOOR-ROOM-99', N'ROOM NAME - 99', 3)
INSERT [dbo].[AQ_Property_Lift_Inputs] ([Lift_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost]) VALUES (12, 12, 5, N'hift', N'8', CAST(783773.56 AS Decimal(15, 2)))
INSERT [dbo].[AQ_Property_Lift_Inputs] ([Lift_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost]) VALUES (13, 13, 2, N'tist', N'20', CAST(572828.83 AS Decimal(15, 2)))
INSERT [dbo].[AQ_Property_Lift_Inputs] ([Lift_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost]) VALUES (14, 14, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)))
INSERT [dbo].[AQ_Property_Lift_Inputs] ([Lift_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost]) VALUES (15, 15, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)))
INSERT [dbo].[AQ_Property_Lift_Inputs] ([Lift_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost]) VALUES (16, 16, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)))
INSERT [dbo].[AQ_Property_Lift_Inputs] ([Lift_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost]) VALUES (17, 17, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)))
INSERT [dbo].[AQ_Property_Lift_Inputs] ([Lift_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost]) VALUES (18, 18, 5, N'hift', N'8', CAST(783773.56 AS Decimal(15, 2)))
SET IDENTITY_INSERT [dbo].[AQ_Property_Lift_Photos] ON 

INSERT [dbo].[AQ_Property_Lift_Photos] ([Property_Lift_Photo_ID], [Property_ID], [Lift_ID], [PhotoLocation], [Seq_ID]) VALUES (9, 12, 12, N'', 1)
INSERT [dbo].[AQ_Property_Lift_Photos] ([Property_Lift_Photo_ID], [Property_ID], [Lift_ID], [PhotoLocation], [Seq_ID]) VALUES (10, 13, 13, N'', 1)
INSERT [dbo].[AQ_Property_Lift_Photos] ([Property_Lift_Photo_ID], [Property_ID], [Lift_ID], [PhotoLocation], [Seq_ID]) VALUES (11, 14, 14, N'', 1)
INSERT [dbo].[AQ_Property_Lift_Photos] ([Property_Lift_Photo_ID], [Property_ID], [Lift_ID], [PhotoLocation], [Seq_ID]) VALUES (12, 15, 15, N'', 1)
INSERT [dbo].[AQ_Property_Lift_Photos] ([Property_Lift_Photo_ID], [Property_ID], [Lift_ID], [PhotoLocation], [Seq_ID]) VALUES (13, 16, 16, N'', 1)
INSERT [dbo].[AQ_Property_Lift_Photos] ([Property_Lift_Photo_ID], [Property_ID], [Lift_ID], [PhotoLocation], [Seq_ID]) VALUES (14, 17, 17, N'', 1)
INSERT [dbo].[AQ_Property_Lift_Photos] ([Property_Lift_Photo_ID], [Property_ID], [Lift_ID], [PhotoLocation], [Seq_ID]) VALUES (15, 18, 18, N'', 1)
SET IDENTITY_INSERT [dbo].[AQ_Property_Lift_Photos] OFF
INSERT [dbo].[AQ_Property_Owner_Details] ([Owner_Detail_ID], [Property_ID], [Owner_Name], [Phone_Number], [Is_owner_Operated]) VALUES (12, 12, N'test', N'80948494944', 0)
INSERT [dbo].[AQ_Property_Owner_Details] ([Owner_Detail_ID], [Property_ID], [Owner_Name], [Phone_Number], [Is_owner_Operated]) VALUES (13, 13, N'', N'', 0)
INSERT [dbo].[AQ_Property_Owner_Details] ([Owner_Detail_ID], [Property_ID], [Owner_Name], [Phone_Number], [Is_owner_Operated]) VALUES (14, 14, N'', N'', 0)
INSERT [dbo].[AQ_Property_Owner_Details] ([Owner_Detail_ID], [Property_ID], [Owner_Name], [Phone_Number], [Is_owner_Operated]) VALUES (15, 15, N'', N'', 0)
INSERT [dbo].[AQ_Property_Owner_Details] ([Owner_Detail_ID], [Property_ID], [Owner_Name], [Phone_Number], [Is_owner_Operated]) VALUES (16, 16, N'', N'', 0)
INSERT [dbo].[AQ_Property_Owner_Details] ([Owner_Detail_ID], [Property_ID], [Owner_Name], [Phone_Number], [Is_owner_Operated]) VALUES (17, 17, N'', N'', 0)
INSERT [dbo].[AQ_Property_Owner_Details] ([Owner_Detail_ID], [Property_ID], [Owner_Name], [Phone_Number], [Is_owner_Operated]) VALUES (18, 18, N'test', N'80948494944', 0)
SET IDENTITY_INSERT [dbo].[AQ_Property_Photos] ON 

INSERT [dbo].[AQ_Property_Photos] ([Property_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (10, 12, N'D:\Kristab NewSource\PlacioServices\NewPropertyImages\PropertyDetails\PLACIO123\PropertyImages102220', 1)
INSERT [dbo].[AQ_Property_Photos] ([Property_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (11, 13, N'E:\PlacioServices\NewPropertyImages\PropertyDetails\PLACIO123\PropertyImages10222018172955388.Jpeg', 1)
INSERT [dbo].[AQ_Property_Photos] ([Property_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (12, 14, N'', 1)
INSERT [dbo].[AQ_Property_Photos] ([Property_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (13, 15, N'', 1)
INSERT [dbo].[AQ_Property_Photos] ([Property_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (14, 16, N'', 1)
INSERT [dbo].[AQ_Property_Photos] ([Property_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (15, 17, N'', 1)
INSERT [dbo].[AQ_Property_Photos] ([Property_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (16, 18, N'D:\Kristab NewSource\PlacioServices\NewPropertyImages\PropertyDetails\PLACIO123\PropertyImages102320', 1)
SET IDENTITY_INSERT [dbo].[AQ_Property_Photos] OFF
SET IDENTITY_INSERT [dbo].[AQ_Property_Room_Photos] ON 

INSERT [dbo].[AQ_Property_Room_Photos] ([Property_Room_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (1, 12, N'', 1)
INSERT [dbo].[AQ_Property_Room_Photos] ([Property_Room_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (2, 13, N'E:\PlacioServices\NewPropertyImages\RoomDetails\PLACIO123\RoomImages10222018172955388.Jpeg', 1)
INSERT [dbo].[AQ_Property_Room_Photos] ([Property_Room_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (3, 14, N'', 1)
INSERT [dbo].[AQ_Property_Room_Photos] ([Property_Room_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (4, 15, N'', 1)
INSERT [dbo].[AQ_Property_Room_Photos] ([Property_Room_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (5, 16, N'', 1)
INSERT [dbo].[AQ_Property_Room_Photos] ([Property_Room_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (6, 17, N'', 1)
INSERT [dbo].[AQ_Property_Room_Photos] ([Property_Room_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (7, 18, N'', 1)
SET IDENTITY_INSERT [dbo].[AQ_Property_Room_Photos] OFF
SET IDENTITY_INSERT [dbo].[AQ_Property_Security_Features] ON 

INSERT [dbo].[AQ_Property_Security_Features] ([Property_Security_Feature_ID], [Property_ID], [Security_Feature_ID]) VALUES (1, 12, 0)
INSERT [dbo].[AQ_Property_Security_Features] ([Property_Security_Feature_ID], [Property_ID], [Security_Feature_ID]) VALUES (2, 13, 0)
INSERT [dbo].[AQ_Property_Security_Features] ([Property_Security_Feature_ID], [Property_ID], [Security_Feature_ID]) VALUES (3, 14, 0)
INSERT [dbo].[AQ_Property_Security_Features] ([Property_Security_Feature_ID], [Property_ID], [Security_Feature_ID]) VALUES (4, 15, 0)
INSERT [dbo].[AQ_Property_Security_Features] ([Property_Security_Feature_ID], [Property_ID], [Security_Feature_ID]) VALUES (5, 16, 0)
INSERT [dbo].[AQ_Property_Security_Features] ([Property_Security_Feature_ID], [Property_ID], [Security_Feature_ID]) VALUES (6, 17, 0)
INSERT [dbo].[AQ_Property_Security_Features] ([Property_Security_Feature_ID], [Property_ID], [Security_Feature_ID]) VALUES (7, 18, 0)
SET IDENTITY_INSERT [dbo].[AQ_Property_Security_Features] OFF
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (1, N'BRAND-1000', N'TEST BRAND FOR BRAND-1000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (2, N'BRAND-2000', N'TEST BRAND FOR BRAND-2000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (3, N'BRAND-3000', N'TEST BRAND FOR BRAND-3000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (4, N'BRAND-4000', N'TEST BRAND FOR BRAND-4000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (5, N'BRAND-5000', N'TEST BRAND FOR BRAND-5000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (6, N'BRAND-6000', N'TEST BRAND FOR BRAND-6000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (7, N'BRAND-7000', N'TEST BRAND FOR BRAND-7000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (8, N'BRAND-8000', N'TEST BRAND FOR BRAND-8000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (9, N'BRAND-9000', N'TEST BRAND FOR BRAND-9000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (10, N'BRAND-10000', N'TEST BRAND FOR BRAND-10000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (11, N'BRAND-11000', N'TEST BRAND FOR BRAND-11000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (12, N'BRAND-12000', N'TEST BRAND FOR BRAND-12000')
INSERT [dbo].[Brands] ([brand_id], [brand_short_name], [brand_full_name]) VALUES (13, N'BRAND-13000', N'TEST BRAND FOR BRAND-13000')
SET IDENTITY_INSERT [dbo].[C_Property_Canteen_Photos] ON 

INSERT [dbo].[C_Property_Canteen_Photos] ([C_Property_Canteen_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (1, 7, N'', 1)
INSERT [dbo].[C_Property_Canteen_Photos] ([C_Property_Canteen_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (2, 1, N'', 1)
INSERT [dbo].[C_Property_Canteen_Photos] ([C_Property_Canteen_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (3, 8, N'', 1)
INSERT [dbo].[C_Property_Canteen_Photos] ([C_Property_Canteen_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (4, 9, N'', 1)
SET IDENTITY_INSERT [dbo].[C_Property_Canteen_Photos] OFF
SET IDENTITY_INSERT [dbo].[C_Property_Common_Area_Cleanliness_Photos] ON 

INSERT [dbo].[C_Property_Common_Area_Cleanliness_Photos] ([C_Property_Common_Area_Cleanliness_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (1, 7, N'', 1)
INSERT [dbo].[C_Property_Common_Area_Cleanliness_Photos] ([C_Property_Common_Area_Cleanliness_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (2, 1, N'', 1)
INSERT [dbo].[C_Property_Common_Area_Cleanliness_Photos] ([C_Property_Common_Area_Cleanliness_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (3, 8, N'', 1)
INSERT [dbo].[C_Property_Common_Area_Cleanliness_Photos] ([C_Property_Common_Area_Cleanliness_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (4, 9, N'', 1)
SET IDENTITY_INSERT [dbo].[C_Property_Common_Area_Cleanliness_Photos] OFF
SET IDENTITY_INSERT [dbo].[C_Property_Diesel_Generator_Photos] ON 

INSERT [dbo].[C_Property_Diesel_Generator_Photos] ([C_Property_Diesel_Generator_Photo_ID], [C_Property_ID], [C_Diesel_Generator_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (1, 7, NULL, N'', 1)
INSERT [dbo].[C_Property_Diesel_Generator_Photos] ([C_Property_Diesel_Generator_Photo_ID], [C_Property_ID], [C_Diesel_Generator_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (2, 1, NULL, N'', 1)
INSERT [dbo].[C_Property_Diesel_Generator_Photos] ([C_Property_Diesel_Generator_Photo_ID], [C_Property_ID], [C_Diesel_Generator_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (3, 8, NULL, N'', 1)
INSERT [dbo].[C_Property_Diesel_Generator_Photos] ([C_Property_Diesel_Generator_Photo_ID], [C_Property_ID], [C_Diesel_Generator_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (4, 9, NULL, N'', 1)
SET IDENTITY_INSERT [dbo].[C_Property_Diesel_Generator_Photos] OFF
SET IDENTITY_INSERT [dbo].[C_Property_Lift_Photos] ON 

INSERT [dbo].[C_Property_Lift_Photos] ([C_Property_Lift_Photo_ID], [C_Property_ID], [C_Lift_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (1, 7, NULL, N'', 1)
INSERT [dbo].[C_Property_Lift_Photos] ([C_Property_Lift_Photo_ID], [C_Property_ID], [C_Lift_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (2, 1, NULL, N'', 1)
INSERT [dbo].[C_Property_Lift_Photos] ([C_Property_Lift_Photo_ID], [C_Property_ID], [C_Lift_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (3, 8, NULL, N'', 1)
INSERT [dbo].[C_Property_Lift_Photos] ([C_Property_Lift_Photo_ID], [C_Property_ID], [C_Lift_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (4, 9, NULL, N'', 1)
SET IDENTITY_INSERT [dbo].[C_Property_Lift_Photos] OFF
INSERT [dbo].[C_Property_Owner_Details] ([C_Owner_Detail_ID], [C_Property_ID], [C_Owner_Name], [C_Phone_Number], [C_Is_owner_Operated]) VALUES (1, 1, N'', N'', 0)
INSERT [dbo].[C_Property_Owner_Details] ([C_Owner_Detail_ID], [C_Property_ID], [C_Owner_Name], [C_Phone_Number], [C_Is_owner_Operated]) VALUES (2, 8, N'', N'', 0)
INSERT [dbo].[C_Property_Owner_Details] ([C_Owner_Detail_ID], [C_Property_ID], [C_Owner_Name], [C_Phone_Number], [C_Is_owner_Operated]) VALUES (3, 9, N'', N'', 0)
INSERT [dbo].[C_Property_Owner_Details] ([C_Owner_Detail_ID], [C_Property_ID], [C_Owner_Name], [C_Phone_Number], [C_Is_owner_Operated]) VALUES (7, 7, N'suripapa', N'89084946464', 0)
SET IDENTITY_INSERT [dbo].[C_Property_Photos] ON 

INSERT [dbo].[C_Property_Photos] ([C_Property_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (1, 7, N'', 1)
INSERT [dbo].[C_Property_Photos] ([C_Property_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (2, 1, N'', 1)
INSERT [dbo].[C_Property_Photos] ([C_Property_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (3, 8, N'', 1)
INSERT [dbo].[C_Property_Photos] ([C_Property_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (4, 9, N'', 1)
SET IDENTITY_INSERT [dbo].[C_Property_Photos] OFF
SET IDENTITY_INSERT [dbo].[C_Property_Room_Photos] ON 

INSERT [dbo].[C_Property_Room_Photos] ([C_Property_Room_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (1, 7, N'', 1)
INSERT [dbo].[C_Property_Room_Photos] ([C_Property_Room_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (2, 1, N'', 1)
INSERT [dbo].[C_Property_Room_Photos] ([C_Property_Room_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (3, 8, N'', 1)
INSERT [dbo].[C_Property_Room_Photos] ([C_Property_Room_Photo_ID], [C_Property_ID], [C_Photo_Location], [C_Seq_ID]) VALUES (4, 9, N'', 1)
SET IDENTITY_INSERT [dbo].[C_Property_Room_Photos] OFF
SET IDENTITY_INSERT [dbo].[Category_Items] ON 

INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (1, N'               ', 1, 1, N'Dry or Easy Mops & Micro Fibre Mops & Refiils')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (2, N'               ', 1, 1, N'Range of Wet Mops & Refills')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (3, N'               ', 1, 1, N'Floor Sweeping Brushes')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (4, N'               ', 1, 1, N'Floor Scrubbing Brushes')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (5, N'               ', 1, 1, N'Doodle Bug with Handle')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (6, N'               ', 1, 1, N'Handy Doodle Bug and Scrubbing Pads')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (7, N'               ', 1, 1, N'Mop Trolleys and Wringer Trolleys')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (8, N'               ', 1, 1, N'Dust Pan with Broom')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (9, N'               ', 1, 1, N'Range of Floor Wipers or Squeeges')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (10, N'               ', 1, 1, N'Floor Corner and Kettle Burush Plastic and wooden')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (11, N'               ', 1, 1, N'Floor Cleaning Srcapers')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (12, N'               ', 1, 2, N'General Floor Cleaner')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (13, N'               ', 1, 2, N'Hygenic Surface Cleaner')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (14, N'               ', 1, 2, N'Oil Surface Cleaner')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (15, N'               ', 1, 2, N'Grease Surface Cleaner')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (16, N'               ', 1, 2, N'Lime Scale Surface Remover')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (17, N'               ', 1, 3, N'Glass Cleaning Washer with T - Bar in Differnet Sizes')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (18, N'               ', 1, 3, N'Two in One Glass Wiper in Different Sizes')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (19, N'               ', 1, 3, N'Different Size of Glass Wiper or Glass Squeegee in Plastic or Stainless Steel')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (20, N'               ', 1, 3, N'Glass Cleaning Micro Fibre Cloths')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (21, N'               ', 1, 3, N'Different Sizes of Glass Cleaning Telescopic Pole upto 30 Feet')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (22, N'               ', 1, 4, N'Glass Cleaner Ready to Use')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (23, N'               ', 1, 4, N'Glass Cleaner Concentrated.')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (24, N'               ', 1, 5, N'Toilet Cleaning Brushes')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (25, N'               ', 1, 5, N'Floor & Wall Tiles Scrubber')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (26, N'               ', 1, 5, N'Floor and Wall Tiles Wipers')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (27, N'               ', 1, 5, N'Floor Scrubbing Brushes.')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (28, N'               ', 1, 5, N'2 in 1 Floor Wiper and Scrubber')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (29, N'               ', 1, 6, N'Odonil Air Freshener')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (30, N'               ', 1, 6, N'Flush Matic Double')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (31, N'               ', 1, 6, N'Toilet Roll and Dispenser')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (32, N'               ', 1, 6, N'Tissue Paper and Dispenser')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (33, N'               ', 1, 6, N'Automaitc Air Frehser')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (34, N'               ', 1, 6, N'Napatiline Balls and Uriunal cubes')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (35, N'               ', 1, 6, N'Urinal Screen')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (36, N'               ', 1, 7, N'Toilet Bowl Cleaner')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (37, N'               ', 1, 7, N'Wall and Floor Cleaner')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (38, N'               ', 1, 7, N'Room Freshner')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (39, N'               ', 1, 7, N'S.S Fittings and Floor lime Scale remover')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (40, N'               ', 1, 8, N'Pedal Dust Bins')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (41, N'               ', 1, 8, N'Round Dust Bins')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (42, N'               ', 1, 8, N'Push Bins')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (43, N'               ', 1, 8, N'Swing Bins')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (44, N'               ', 1, 8, N'Plastic Bucket and Mugs')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (45, N'               ', 1, 9, N'Pedal Dustbins')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (46, N'               ', 1, 9, N'Push can Bins')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (47, N'               ', 1, 9, N'Ash can Bins')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (48, N'               ', 1, 9, N'Swing Bings')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (49, N'               ', 1, 10, N'Range of PVC, Rubber, Nitrile Rubber Gloves and Disposbale Gloves')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (50, N'               ', 1, 10, N'Range of Safety Shoes')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (51, N'               ', 1, 10, N'Safty Mask')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (52, N'               ', 1, 10, N'Safty Cap and Disposbale Cap')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (53, N'               ', 1, 10, N'Eye Safty Glasses, Cautin Board')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (54, N'               ', 1, 11, N'Single Disc Machine')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (55, N'               ', 1, 11, N'Scrubber and Dryer')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (56, N'               ', 1, 11, N'Air Blower. Mop Trolleys and All kinds of Pads')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (57, N'               ', 1, 12, N'Pynoil')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (58, N'               ', 1, 12, N'Dish Cleaner')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (59, N'               ', 1, 12, N'Bleaching Power')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (60, N'               ', 1, 12, N'Hand wash Gel')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (61, N'               ', 1, 12, N'Dettol')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (62, N'               ', 1, 12, N'Lizol')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (63, N'               ', 1, 12, N'Harpic')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (64, N'               ', 1, 12, N'Soap Bar')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (65, N'               ', 1, 12, N'Caustic Sosa')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (66, N'               ', 1, 12, N'Mops')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (67, N'               ', 1, 12, N'brushers')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (68, N'               ', 1, 12, N'Food Napkins')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (69, N'               ', 1, 12, N'Garbage Bags and Cover')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (70, N'               ', 1, 12, N'Pitambari Powder')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (71, N'               ', 1, 12, N'Brasso. Floor Mats')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (72, N'               ', 1, 12, N'Scotch Brite')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (73, N'               ', 1, 12, N'Mug')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (74, N'               ', 1, 12, N'bucket')
INSERT [dbo].[Category_Items] ([CATEGORY_ITEM_ID], [CATEGORY_ITEM_CODE], [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_NAME]) VALUES (75, N'               ', 1, 12, N'Dust pans')
SET IDENTITY_INSERT [dbo].[Category_Items] OFF
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (1, NULL, N'Hostel Official', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (2, NULL, N'Hostel Advisory Committee & Staff', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (3, NULL, N'Payments', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (4, NULL, N'Timings', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (5, NULL, N'Hostel Fees', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (6, NULL, N'Penal Rent', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (7, NULL, N'Notices', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (8, NULL, N'Facilities', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (9, NULL, N'Allotment Method', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (10, NULL, N'Hostel Location', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (11, NULL, N'Rules and Regulations', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (12, NULL, N'Security', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (13, NULL, N'Sanitation and Cleanliness', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (14, NULL, N'Engineering services / repairs in the room', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (15, NULL, N'For day to day matters', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (16, NULL, N'Civil', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (17, NULL, N'Cleanness', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (18, NULL, N'Drinking Water', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (19, NULL, N'Electrical', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (20, NULL, N'Internet', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (21, NULL, N'Mess', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (22, NULL, N'Other', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (23, NULL, N'Washroom', 1)
INSERT [dbo].[Complaint_Types] ([complaint_type_id], [complaint_type_cd], [complaint_type], [active_yn]) VALUES (24, NULL, N'Washroom (Toilets/Bathrooms)', 1)
SET IDENTITY_INSERT [dbo].[Department_Employees] ON 

INSERT [dbo].[Department_Employees] ([department_employees_id], [department_id], [employee_id], [from_date], [to_date]) VALUES (1, 7, 101, CAST(N'2018-03-23' AS Date), NULL)
INSERT [dbo].[Department_Employees] ([department_employees_id], [department_id], [employee_id], [from_date], [to_date]) VALUES (2, 8, 102, CAST(N'2018-03-23' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[Department_Employees] OFF
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (1, N'Administration', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (2, N'  Health &  Recreation', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (3, N'  Sanitation & Maintenance', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (4, N'  Maintenance', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (5, N'  Sanitation', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (6, N'  Mess', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (7, N'  Managing Committee', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (8, N'  Hostel Committee', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (9, N'  Mess Committee', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (10, N'  Student(s) Caretaker(s)', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (11, N'  Legal', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (12, N'  Accounts', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (13, N'  Billing', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (14, N'  General', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (15, N'  Complaints & Grievances', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (16, N'  Inventory', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (17, N'  Sales', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (18, N'  Purchases', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (19, N'  House Keeping', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (20, N'  Food & Bewarages', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (21, N'  Stores', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (22, N'  Services', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (23, N'  Auditing', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (24, N'  Human Resource(HR)', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (25, N'Customer Service & Front Office', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (26, N'Catering', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (27, N'Activity Support', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (28, N'Others', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (29, N'Accounting and Budgeting', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (30, N'Quality Control', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (31, N'Business Development & Strategy', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (32, N'Entertainment, Atmosphere, Activites', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (33, N'Public Relations & Industry Relations', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (34, N'Sales & Marketing', NULL)
INSERT [dbo].[Departments] ([department_id], [department_name], [department_category_code]) VALUES (35, N'Staff Training & Management', NULL)
INSERT [dbo].[Employee_Addresses] ([employee_address_id], [address_id], [address_type_code], [employee_id], [date_address_from], [date_address_to], [active_address_id]) VALUES (100, 12, N'HOME           ', 101, CAST(N'2000-10-23' AS Date), CAST(N'1900-01-01' AS Date), NULL)
INSERT [dbo].[Employee_Addresses] ([employee_address_id], [address_id], [address_type_code], [employee_id], [date_address_from], [date_address_to], [active_address_id]) VALUES (101, 13, N'HOME           ', 102, CAST(N'2000-10-23' AS Date), CAST(N'1900-01-01' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[Employee_Qualifications] ON 

INSERT [dbo].[Employee_Qualifications] ([employee_qualification_id], [employee_id], [qualification_code], [date_obtained]) VALUES (1, 101, N'5              ', CAST(N'2012-03-23' AS Date))
INSERT [dbo].[Employee_Qualifications] ([employee_qualification_id], [employee_id], [qualification_code], [date_obtained]) VALUES (2, 102, N'6              ', CAST(N'2012-03-23' AS Date))
SET IDENTITY_INSERT [dbo].[Employee_Qualifications] OFF
SET IDENTITY_INSERT [dbo].[Employee_Source_Types] ON 

INSERT [dbo].[Employee_Source_Types] ([employee_source_type_id], [employee_source_type], [active_yn]) VALUES (1, N'Full Time Employee', 1)
INSERT [dbo].[Employee_Source_Types] ([employee_source_type_id], [employee_source_type], [active_yn]) VALUES (2, N'Part Time Employee', 1)
INSERT [dbo].[Employee_Source_Types] ([employee_source_type_id], [employee_source_type], [active_yn]) VALUES (3, N'Out Source Employees', 1)
INSERT [dbo].[Employee_Source_Types] ([employee_source_type_id], [employee_source_type], [active_yn]) VALUES (4, N'Third Party Employees', 1)
SET IDENTITY_INSERT [dbo].[Employee_Source_Types] OFF
SET IDENTITY_INSERT [dbo].[Employee_Titles] ON 

INSERT [dbo].[Employee_Titles] ([employee_job_title_id], [employee_id], [job_title_code], [date_from], [date_to]) VALUES (1, 102, N'15             ', CAST(N'2018-03-23T00:00:00.000' AS DateTime), NULL)
INSERT [dbo].[Employee_Titles] ([employee_job_title_id], [employee_id], [job_title_code], [date_from], [date_to]) VALUES (2, 101, N'27             ', CAST(N'2018-03-23T00:00:00.000' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Employee_Titles] OFF
INSERT [dbo].[Employees] ([employee_id], [employee_details], [nick_name], [first_name], [middle_name], [last_name], [gender], [date_of_birth], [date_joined], [date_left], [department_id], [job_title_code], [qualification_code], [updated_date], [updated_by]) VALUES (101, N'Having previous experience in this domain, worked 2 years on the similar position', N'Sai Raj', N'Venkata', N'Raju', N'Sai', N'Male', CAST(N'1990-10-23' AS Date), CAST(N'2018-02-23' AS Date), NULL, 7, N'27', N'5', CAST(N'2018-10-23' AS Date), NULL)
INSERT [dbo].[Employees] ([employee_id], [employee_details], [nick_name], [first_name], [middle_name], [last_name], [gender], [date_of_birth], [date_joined], [date_left], [department_id], [job_title_code], [qualification_code], [updated_date], [updated_by]) VALUES (102, N'Had 4 years previous experience in this domain, worked 2 years on the similar position', N'Rajendra', N'Tiwari', N'N', N'Rajendra', N'Male', CAST(N'1985-10-23' AS Date), CAST(N'2018-03-23' AS Date), NULL, 8, N'15', N'6', CAST(N'2018-10-23' AS Date), NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (1, N'24 Hour Reception', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (2, N'Free location map, travel guides and other information on your arrival', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (3, N'Always helpful hostel staff with current information, tips and recommendations', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (4, N'An Easygoing, relaxing and friendly Environment', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (5, N'Free wifi in lobby, Hotspots are available in the Apartments', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (6, N'Taxes are included', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (7, N'Breakfast is included', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (8, N'Use of Kitchen is included', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (9, N'Many varieties of free coffee and tea are availble ', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (10, N'Washing Machine and dryer is available. If you stay at least 6 months, laundry is included', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (11, N'Linen and bedding are included', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (12, N'Towels are available', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (13, N'Ceiling Fan is available in the rooms', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (14, N'Reading Lights are included', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (15, N'Hairdryer is available on reception', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (16, N'Iron and Ironing Board is available upon Request', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (17, N'Free Internet access is available', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (18, N'Free luggage storage before check-in and after check-out', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (19, N'Car-Parking is available nearby the hostel', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (20, N'Check out is by 11 am, sometimes later check-out can be arranged', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (21, N'Luggage storage is available', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (22, N'Elevator is fitted in the building', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (23, N'Airport Transfer Service is available', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (24, N'No curfew, no lockout, no worries', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (25, N'Security Check at the main gate of each Hostel', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (26, N'Lady guard at girls hostels, round the clock', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (27, N'Surveillance video camera at entrance of all the hostel buildings', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (28, N'T.V.Lounge - Common TV lounge with cable connection on the ground floor of each hostel building', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (29, N'Each student is provided with a steel cot, a study table, and chair and a mattress.', NULL, NULL)
INSERT [dbo].[Facilities] ([facility_id], [facility_name], [facility_type], [facility_description]) VALUES (30, N'Each room has an attached toilet and bathroom', NULL, NULL)
INSERT [dbo].[Financial_Item_Categories] ([financial_category_id], [financial_category_code], [category_description]) VALUES (2, N'CAPEX', N'CAPital EXpenditure')
INSERT [dbo].[Financial_Item_Categories] ([financial_category_id], [financial_category_code], [category_description]) VALUES (1, N'OPEX', N'OPerating EXpenditure')
SET IDENTITY_INSERT [dbo].[Food_Menu] ON 

INSERT [dbo].[Food_Menu] ([food_menu_id], [week_day], [break_fast], [lunch], [dinner], [active_yn]) VALUES (1, N'Monday         ', N'Egg/Mix Pakora, Poha, Sprouts, Bournvita', N'Paneer Bhurji, Tadka Arhar Dal, Rice, Roti, Lassi(Sweet)', N'Fried rice, Manuchurian, Mix Parantha, Butter, Dahi Patashe, Cole Coffee/ Cold Drink', 1)
INSERT [dbo].[Food_Menu] ([food_menu_id], [week_day], [break_fast], [lunch], [dinner], [active_yn]) VALUES (2, N'Tuesday        ', N'Halwa, Kala Chana, Corn flakes, Bread Pakora', N'Aloo Shimla Mirch, Urad Chana, Roti, Green Chutney, Mix Veg Rice, Aam Panna', N'Chole, Masala Puri, Sitaphat, Rice, Kheer(Cold)', 1)
INSERT [dbo].[Food_Menu] ([food_menu_id], [week_day], [break_fast], [lunch], [dinner], [active_yn]) VALUES (3, N'Wednesday      ', N'Omlette/Cutlet, Namkeen Sevai, Bournvita, Banana', N'Rajma, Aloo Matar Cabbage, Jeera Rice, Tawa Roti, Mango Achaar, Chaach', N'Aloo Tamatar, Mix Dal, Pulav (Onion), Roti, Robri Imarti', 1)
INSERT [dbo].[Food_Menu] ([food_menu_id], [week_day], [break_fast], [lunch], [dinner], [active_yn]) VALUES (4, N'Thursday       ', N'Aloo Parantha, Daliya, Chutney (Tomoto + Onion), Corn Flakes', N'Chole, Lonki Chana dal, Pyaaz Rice, Roti, Nimbu Achaar, Cuccumber Raita', N'Handi Paneer, Rice, Daal Makhani, Laccha Parantha/Rumali (alt.), Fruit cream + Ice Cream', 1)
INSERT [dbo].[Food_Menu] ([food_menu_id], [week_day], [break_fast], [lunch], [dinner], [active_yn]) VALUES (5, N'Friday         ', N'Sambar Vada / Idli (Alt.)., Coconut Chutney, Bournivita, Papaya.', N'Kadhi Pakoda, Mix Veg., Rice, Roti, Nimbu Achaar, Orange Squash', N'Fry Bhindi, Arhar Dal, Onion Chutney, Roti, Rice, Gulab Jamun/Ras Malai (Alt.)', 1)
INSERT [dbo].[Food_Menu] ([food_menu_id], [week_day], [break_fast], [lunch], [dinner], [active_yn]) VALUES (6, N'Saturday       ', N'Aloor Puri + Aloo Subji, Sprouts, Corn Flakes, Jalebi', N'Chole (Kale), Khichdi, Tawa Roti, Achaar/Chutney, Butter, Papad', N'Rajma, Aloo Parantha, Butter, Raita Boondi, Veg/Egg., Biryani, Rajbhog', 1)
INSERT [dbo].[Food_Menu] ([food_menu_id], [week_day], [break_fast], [lunch], [dinner], [active_yn]) VALUES (7, N'Sunday         ', N'Uttapam/Dosa(alt.), Sambar Chutney, Dhokla, Bourn Vita, Seasonal Fruit', N'Chole Bhature, Jeera Aloo, Rice, Kulche, Dahi Bhalle(Khajur Imli Chutney)', N'Kadai Paneer, Egg Curry, Chana Dal, Tandoori Roti, Rice, Kesar Kulfi (Faluda)', 1)
SET IDENTITY_INSERT [dbo].[Food_Menu] OFF
SET IDENTITY_INSERT [dbo].[Inventory_Items] ON 

INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (1, N'ITEM_00001', 1, N'ITEMC_00001', N'ITEMC_00001ITEM_00001', N'100', N'10', N'10')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (2, N'ITEM_00002', 1, N'ITEMC_00002', N'ITEMC_00002ITEM_00002', N'120', N'15', N'11')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (3, N'ITEM_00003', 1, N'ITEMC_00003', N'ITEMC_00003ITEM_00003', N'140', N'20', N'12')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (4, N'ITEM_00004', 1, N'ITEMC_00004', N'ITEMC_00004ITEM_00004', N'160', N'25', N'13')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (5, N'ITEM_00005', 1, N'ITEMC_00005', N'ITEMC_00005ITEM_00005', N'180', N'30', N'14')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (6, N'ITEM_00006', 1, N'ITEMC_00006', N'ITEMC_00006ITEM_00006', N'200', N'35', N'15')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (7, N'ITEM_00007', 1, N'ITEMC_00007', N'ITEMC_00007ITEM_00007', N'220', N'40', N'16')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (8, N'ITEM_00008', 1, N'ITEMC_00011', N'ITEMC_00011ITEM_00008', N'240', N'45', N'17')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (9, N'ITEM_00009', 1, N'ITEMC_00012', N'ITEMC_00012ITEM_00009', N'260', N'50', N'18')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (10, N'ITEM_00010', 1, N'ITEMC_00013', N'ITEMC_00013ITEM_00010', N'280', N'55', N'19')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (11, N'ITEM_00011', 1, N'ITEMC_00014', N'ITEMC_00014ITEM_00011', N'300', N'60', N'20')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (12, N'ITEM_00012', 1, N'ITEMC_00015', N'ITEMC_00015ITEM_00012', N'320', N'65', N'21')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (13, N'ITEM_00013', 1, N'ITEMC_00016', N'ITEMC_00016ITEM_00013', N'340', N'70', N'22')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (14, N'ITEM_00014', 2, N'ITEMC_00017', N'ITEMC_00017ITEM_00014', N'360', N'75', N'23')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (15, N'ITEM_00015', 2, N'ITEMC_00018', N'ITEMC_00018ITEM_00015', N'380', N'80', N'24')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (16, N'ITEM_00016', 2, N'ITEMC_00019', N'ITEMC_00019ITEM_00016', N'400', N'85', N'25')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (17, N'ITEM_00017', 2, N'ITEMC_00011', N'ITEMC_00011ITEM_00017', N'420', N'90', N'26')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (18, N'ITEM_00018', 2, N'ITEMC_00012', N'ITEMC_00012ITEM_00018', N'440', N'95', N'27')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (19, N'ITEM_00019', 2, N'ITEMC_00013', N'ITEMC_00013ITEM_00019', N'460', N'100', N'28')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (20, N'ITEM_00020', 2, N'ITEMC_00014', N'ITEMC_00014ITEM_00020', N'480', N'105', N'29')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (21, N'ITEM_00021', 2, N'ITEMC_00015', N'ITEMC_00015ITEM_00021', N'500', N'110', N'30')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (22, N'ITEM_00022', 3, N'ITEMC_00001', N'ITEMC_00001ITEM_00022', N'520', N'115', N'31')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (23, N'ITEM_00023', 3, N'ITEMC_00002', N'ITEMC_00002ITEM_00023', N'540', N'120', N'32')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (24, N'ITEM_00024', 3, N'ITEMC_00003', N'ITEMC_00003ITEM_00024', N'560', N'125', N'33')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (25, N'ITEM_00025', 3, N'ITEMC_00004', N'ITEMC_00004ITEM_00025', N'580', N'130', N'34')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (26, N'ITEM_00026', 3, N'ITEMC_00005', N'ITEMC_00005ITEM_00026', N'600', N'135', N'35')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (27, N'ITEM_00027', 3, N'ITEMC_00006', N'ITEMC_00006ITEM_00027', N'620', N'140', N'36')
INSERT [dbo].[Inventory_Items] ([item_id], [item_code], [brand_id], [item_category_code], [item_description], [average_monthly_usage], [reorder_level], [reorder_quantity]) VALUES (28, N'ITEM_00028', 3, N'ITEMC_00007', N'ITEMC_00007ITEM_00028', N'640', N'145', N'37')
SET IDENTITY_INSERT [dbo].[Inventory_Items] OFF
SET IDENTITY_INSERT [dbo].[Item_Categories] ON 

INSERT [dbo].[Item_Categories] ([ITEM_CATEGORY_ID], [ITEM_CATEOGRY_CODE], [ITEM_CATEGORY_NAME]) VALUES (1, N'HKM            ', N'Housekeeping Material')
SET IDENTITY_INSERT [dbo].[Item_Categories] OFF
SET IDENTITY_INSERT [dbo].[Item_Sub_Categories] ON 

INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (1, N'HKM-FCP        ', 1, N'Floor Cleaning Products')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (2, N'HKM-FCC        ', 1, N'Floor Cleaning Chemicals')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (3, N'HKM-GCD        ', 1, N'Glass Cleaning & Dusting')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (4, N'HKM-GCC        ', 1, N'Glass Cleaning Chemicals')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (5, N'HKM-RRCP       ', 1, N'Rest Room Cleaning Products')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (6, N'HKM-ARRCC      ', 1, N'Accessories-Rest Room Cleaning Cloths')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (7, N'HKM-RRCC       ', 1, N'Rest Room Cleaning Chemicals')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (8, N'HKM-PDB        ', 1, N'Plastic Dust Bins')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (9, N'HKM-SSDPDB     ', 1, N'Stainless Steel Dustbins-Peforated Dust Bins')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (10, N'HKM-CSM        ', 1, N'Housekeeping Cleaning Safety Materials')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (11, N'HKM-CM         ', 1, N'Clening Machineries')
INSERT [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID], [ITEM_CATEOGRY_SUB_CODE], [ITEM_CATEGORY_ID], [SUB_CATEGORY_NAME]) VALUES (12, N'HKM-GHKCM      ', 1, N'General HouseKeeping Cleaning Materials')
SET IDENTITY_INSERT [dbo].[Item_Sub_Categories] OFF
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'1              ', N'Clerk')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'10             ', N'Chef')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'100            ', N'Board of Directors (BOD)')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'11             ', N'Cook')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'12             ', N'Food and Beverage Manager')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'13             ', N'Kitchen Manager')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'14             ', N'Pastry Chef')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'15             ', N'Property Manager')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'16             ', N'General Manager')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'17             ', N'Sales and Marketing Manager')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'18             ', N'Shift Manager')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'19             ', N'House Keeper')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'2              ', N'Receptionist')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'20             ', N'Maid')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'200            ', N'CEO/CBO')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'21             ', N'Maintenance Worker')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'22             ', N'Maintenance Supervisor')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'23             ', N'Director of Operations')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'24             ', N'Director of Maintenance')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'25             ', N'Driver')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'26             ', N'Parking Lot Attendant')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'27             ', N'Cluster Manager')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'3              ', N'Relations Manager')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'300            ', N'Country Head (Operations)')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'4              ', N'Services Associate')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'400            ', N'Cluster Head (Operations)')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'5              ', N'Services Supervisor')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'500            ', N'Sr. Manager Operations')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'6              ', N'Reservation Agent')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'600            ', N'Warden / Caretaker')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'7              ', N'Attendant')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'8              ', N'Cafe Manager')
INSERT [dbo].[Job_Titles] ([job_title_code], [job_title_desc]) VALUES (N'9              ', N'Catering Manager')
INSERT [dbo].[Payment_Methods] ([payment_method_code], [payment_method_desc]) VALUES (N'1              ', N'Cash')
INSERT [dbo].[Payment_Methods] ([payment_method_code], [payment_method_desc]) VALUES (N'2              ', N'Checks')
INSERT [dbo].[Payment_Methods] ([payment_method_code], [payment_method_desc]) VALUES (N'3              ', N'Credit Cards')
INSERT [dbo].[Payment_Methods] ([payment_method_code], [payment_method_desc]) VALUES (N'4              ', N'Online Payments')
INSERT [dbo].[Price_Categories] ([price_categories_code], [price_category], [min_price_range], [max_price_range], [active_yn]) VALUES (1, N'Under Rs.1,000', NULL, NULL, NULL)
INSERT [dbo].[Price_Categories] ([price_categories_code], [price_category], [min_price_range], [max_price_range], [active_yn]) VALUES (2, N'Rs. 1,000 to Rs.2000', NULL, NULL, NULL)
INSERT [dbo].[Price_Categories] ([price_categories_code], [price_category], [min_price_range], [max_price_range], [active_yn]) VALUES (3, N'Rs. 2001 to Rs.4,000', NULL, NULL, NULL)
INSERT [dbo].[Price_Categories] ([price_categories_code], [price_category], [min_price_range], [max_price_range], [active_yn]) VALUES (4, N'Rs. 4001 to Rs. 5,000', NULL, NULL, NULL)
INSERT [dbo].[Price_Categories] ([price_categories_code], [price_category], [min_price_range], [max_price_range], [active_yn]) VALUES (5, N'Rs. 6,001 & Above', NULL, NULL, NULL)
INSERT [dbo].[Property] ([Property_ID], [Property_Name], [Property_Alias_Name], [Property_Type_ID], [Property_Address_ID], [Property_Description], [Property_Status_Code], [Cluster_Manager_ID], [Property_Manager_ID], [Meta_Title], [Meta_Keywords], [Meta_Description], [vr_url], [Created_at], [Updated_at], [Created_by], [Updated_by], [longitute], [latitute]) VALUES (12, N'Indo Board', N'', NULL, NULL, NULL, N'PUBLISHED      ', NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2018-10-22T19:00:16.970' AS DateTime), NULL, 10, NULL, N'78.44330113381147', N'17.468748840769482')
INSERT [dbo].[Property] ([Property_ID], [Property_Name], [Property_Alias_Name], [Property_Type_ID], [Property_Address_ID], [Property_Description], [Property_Status_Code], [Cluster_Manager_ID], [Property_Manager_ID], [Meta_Title], [Meta_Keywords], [Meta_Description], [vr_url], [Created_at], [Updated_at], [Created_by], [Updated_by], [longitute], [latitute]) VALUES (13, N'Atlanta', N'', NULL, NULL, NULL, N'PUBLISHED      ', NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2018-10-22T19:00:24.667' AS DateTime), NULL, 10, NULL, N'78.44245489686728', N'17.46902803771327')
INSERT [dbo].[Property_Addresses] ([property_address_id], [address_id], [address_type_code], [property_id], [date_address_from], [date_address_to]) VALUES (9, 9, N'OFFICE         ', 12, NULL, NULL)
INSERT [dbo].[Property_Addresses] ([property_address_id], [address_id], [address_type_code], [property_id], [date_address_from], [date_address_to]) VALUES (10, 10, N'OFFICE         ', 13, NULL, NULL)
INSERT [dbo].[Property_Block] ([property_block_id], [property_id], [block_name], [no_of_rooms], [display_name]) VALUES (14, 13, N'Block-1', 50, NULL)
INSERT [dbo].[Property_Block] ([property_block_id], [property_id], [block_name], [no_of_rooms], [display_name]) VALUES (16, 12, N'Block-1', 50, NULL)
INSERT [dbo].[Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms], [display_name]) VALUES (60, 13, 14, NULL, N'1', 5, NULL)
INSERT [dbo].[Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms], [display_name]) VALUES (61, 13, 14, NULL, N'2', 4, NULL)
INSERT [dbo].[Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms], [display_name]) VALUES (62, 13, 14, NULL, N'3', 3, NULL)
INSERT [dbo].[Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms], [display_name]) VALUES (65, 12, 16, NULL, N'1', 5, NULL)
INSERT [dbo].[Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms], [display_name]) VALUES (66, 12, 16, NULL, N'2', 5, NULL)
INSERT [dbo].[Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms], [display_name]) VALUES (67, 12, 16, NULL, N'3', 5, NULL)
INSERT [dbo].[Property_Block_Floor] ([Property_block_floor_id], [property_id], [property_block_id], [floor_id], [floor_name], [no_of_rooms], [display_name]) VALUES (68, 12, 16, NULL, N'4', 5, NULL)
SET IDENTITY_INSERT [dbo].[Property_Canteen_Photos] ON 

INSERT [dbo].[Property_Canteen_Photos] ([Property_Canteen_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (5, 12, N'', 1)
INSERT [dbo].[Property_Canteen_Photos] ([Property_Canteen_Photo_ID], [Property_ID], [PhotoLocation], [Seq_ID]) VALUES (6, 13, N'', 1)
SET IDENTITY_INSERT [dbo].[Property_Canteen_Photos] OFF
INSERT [dbo].[Property_Details] ([Property_Detail_ID], [Property_ID], [No_of_Floors], [Location_Score], [Comment_on_Location_Score], [Room_Quality_Score_ID], [Room_Cleanliness_Score_ID], [Water_Source_ID], [Water_Storage_Capacity_ID], [Emergency_Staircase], [Diesel_Generator_ID], [Common_Area_Cleanliness_ID], [Parking_Availability], [Parking_Vehicle_Count], [House_Keeping_staff_Rooms], [Age_of_the_Property], [IsNewProperty], [Gender], [No_of_Beds], [Property_Occupancy_Estimate], [Room_wise_Power_Meter_ID], [Room_Cleanliness_ID], [Reception_Area_yn], [Wifi_Reception], [Security_Feature_IDs], [Fire_Fighting_Equipment_IDs], [ISP_Provider_Name], [IsCanteenExists], [Canteen_Cleanliness_ID], [Food_Availability_ID], [Food_Score], [Operator_Number], [Lift_ID]) VALUES (9, 12, 4, 5, N'', 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, NULL, 0, 0, N'', 0, 0, 0, 0, NULL, N'', N'', 0, 0, 0, 0, N'', 8)
INSERT [dbo].[Property_Details] ([Property_Detail_ID], [Property_ID], [No_of_Floors], [Location_Score], [Comment_on_Location_Score], [Room_Quality_Score_ID], [Room_Cleanliness_Score_ID], [Water_Source_ID], [Water_Storage_Capacity_ID], [Emergency_Staircase], [Diesel_Generator_ID], [Common_Area_Cleanliness_ID], [Parking_Availability], [Parking_Vehicle_Count], [House_Keeping_staff_Rooms], [Age_of_the_Property], [IsNewProperty], [Gender], [No_of_Beds], [Property_Occupancy_Estimate], [Room_wise_Power_Meter_ID], [Room_Cleanliness_ID], [Reception_Area_yn], [Wifi_Reception], [Security_Feature_IDs], [Fire_Fighting_Equipment_IDs], [ISP_Provider_Name], [IsCanteenExists], [Canteen_Cleanliness_ID], [Food_Availability_ID], [Food_Score], [Operator_Number], [Lift_ID]) VALUES (10, 13, 3, 6, N'', 14, 17, 20, 23, 1, 10, 31, 1, 0, 0, 53, NULL, 0, 0, N'', 0, 0, 0, 0, NULL, N'', N'', 0, 0, 0, 0, N'', 9)
INSERT [dbo].[Property_Diesel_Generator] ([Diesel_Generator_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost], [storage_capacity]) VALUES (9, 12, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)), N'')
INSERT [dbo].[Property_Diesel_Generator] ([Diesel_Generator_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost], [storage_capacity]) VALUES (10, 13, 0, N'', N'', CAST(0.00 AS Decimal(15, 2)), N'')
SET IDENTITY_INSERT [dbo].[Property_Diesel_Generator_Photos] ON 

INSERT [dbo].[Property_Diesel_Generator_Photos] ([Property_Diesel_Generator_Photo_ID], [Property_ID], [Diesel_Generator_ID], [Photo_Location], [Seq_ID]) VALUES (9, 12, 9, N'', 1)
INSERT [dbo].[Property_Diesel_Generator_Photos] ([Property_Diesel_Generator_Photo_ID], [Property_ID], [Diesel_Generator_ID], [Photo_Location], [Seq_ID]) VALUES (10, 13, 10, N'', 1)
SET IDENTITY_INSERT [dbo].[Property_Diesel_Generator_Photos] OFF
SET IDENTITY_INSERT [dbo].[Property_Employees] ON 

INSERT [dbo].[Property_Employees] ([property_employee_id], [employee_id], [property_id], [active_yn], [assigned_date], [assigned_by], [from_period], [to_period]) VALUES (1, 101, 12, 1, CAST(N'2018-10-23' AS Date), 102, CAST(N'2012-03-23' AS Date), NULL)
INSERT [dbo].[Property_Employees] ([property_employee_id], [employee_id], [property_id], [active_yn], [assigned_date], [assigned_by], [from_period], [to_period]) VALUES (2, 102, 12, 1, CAST(N'2018-10-23' AS Date), 102, CAST(N'2012-03-23' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[Property_Employees] OFF
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280629, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-63', N'BLOCK-1-FLOOR-ROOM-60BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280630, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-63', N'BLOCK-1-FLOOR-ROOM-60BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280631, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-63', N'BLOCK-1-FLOOR-ROOM-60BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280632, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-64', N'BLOCK-1-FLOOR-ROOM-60BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280633, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-64', N'BLOCK-1-FLOOR-ROOM-60BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280634, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-64', N'BLOCK-1-FLOOR-ROOM-60BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280635, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-65', N'BLOCK-1-FLOOR-ROOM-60BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280636, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-65', N'BLOCK-1-FLOOR-ROOM-60BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280637, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-65', N'BLOCK-1-FLOOR-ROOM-60BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280638, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-66', N'BLOCK-1-FLOOR-ROOM-60BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280639, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-66', N'BLOCK-1-FLOOR-ROOM-60BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280640, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-66', N'BLOCK-1-FLOOR-ROOM-60BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280641, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-67', N'BLOCK-1-FLOOR-ROOM-60BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280642, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-67', N'BLOCK-1-FLOOR-ROOM-60BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280643, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-67', N'BLOCK-1-FLOOR-ROOM-60BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280644, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-68', N'BLOCK-1-FLOOR-ROOM-61BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280645, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-68', N'BLOCK-1-FLOOR-ROOM-61BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280646, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-68', N'BLOCK-1-FLOOR-ROOM-61BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280647, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-69', N'BLOCK-1-FLOOR-ROOM-61BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280648, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-69', N'BLOCK-1-FLOOR-ROOM-61BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280649, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-69', N'BLOCK-1-FLOOR-ROOM-61BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280650, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-70', N'BLOCK-1-FLOOR-ROOM-61BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280651, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-70', N'BLOCK-1-FLOOR-ROOM-61BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280652, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-70', N'BLOCK-1-FLOOR-ROOM-61BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280653, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-71', N'BLOCK-1-FLOOR-ROOM-61BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280654, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-71', N'BLOCK-1-FLOOR-ROOM-61BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280655, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-71', N'BLOCK-1-FLOOR-ROOM-61BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280656, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-72', N'BLOCK-1-FLOOR-ROOM-62BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280657, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-72', N'BLOCK-1-FLOOR-ROOM-62BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280658, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-72', N'BLOCK-1-FLOOR-ROOM-62BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280659, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-73', N'BLOCK-1-FLOOR-ROOM-62BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280660, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-73', N'BLOCK-1-FLOOR-ROOM-62BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280661, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-73', N'BLOCK-1-FLOOR-ROOM-62BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280662, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-74', N'BLOCK-1-FLOOR-ROOM-62BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280663, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-74', N'BLOCK-1-FLOOR-ROOM-62BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280664, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-74', N'BLOCK-1-FLOOR-ROOM-62BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280725, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-95', N'BLOCK-1-FLOOR-ROOM-65BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280726, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-95', N'BLOCK-1-FLOOR-ROOM-65BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280727, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-95', N'BLOCK-1-FLOOR-ROOM-65BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280728, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-96', N'BLOCK-1-FLOOR-ROOM-65BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280729, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-96', N'BLOCK-1-FLOOR-ROOM-65BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280730, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-96', N'BLOCK-1-FLOOR-ROOM-65BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280731, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-97', N'BLOCK-1-FLOOR-ROOM-65BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280732, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-97', N'BLOCK-1-FLOOR-ROOM-65BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280733, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-97', N'BLOCK-1-FLOOR-ROOM-65BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280734, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-98', N'BLOCK-1-FLOOR-ROOM-65BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280735, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-98', N'BLOCK-1-FLOOR-ROOM-65BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280736, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-98', N'BLOCK-1-FLOOR-ROOM-65BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280737, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-99', N'BLOCK-1-FLOOR-ROOM-65BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280738, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-99', N'BLOCK-1-FLOOR-ROOM-65BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280739, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-99', N'BLOCK-1-FLOOR-ROOM-65BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280740, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-100', N'BLOCK-1-FLOOR-ROOM-66BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280741, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-100', N'BLOCK-1-FLOOR-ROOM-66BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280742, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-100', N'BLOCK-1-FLOOR-ROOM-66BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280743, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-101', N'BLOCK-1-FLOOR-ROOM-66BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280744, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-101', N'BLOCK-1-FLOOR-ROOM-66BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280745, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-101', N'BLOCK-1-FLOOR-ROOM-66BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280746, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-102', N'BLOCK-1-FLOOR-ROOM-66BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280747, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-102', N'BLOCK-1-FLOOR-ROOM-66BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280748, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-102', N'BLOCK-1-FLOOR-ROOM-66BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280749, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-103', N'BLOCK-1-FLOOR-ROOM-66BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280750, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-103', N'BLOCK-1-FLOOR-ROOM-66BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280751, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-103', N'BLOCK-1-FLOOR-ROOM-66BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280752, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-104', N'BLOCK-1-FLOOR-ROOM-66BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280753, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-104', N'BLOCK-1-FLOOR-ROOM-66BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280754, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-104', N'BLOCK-1-FLOOR-ROOM-66BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280755, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-105', N'BLOCK-1-FLOOR-ROOM-67BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280756, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-105', N'BLOCK-1-FLOOR-ROOM-67BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280757, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-105', N'BLOCK-1-FLOOR-ROOM-67BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280758, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-106', N'BLOCK-1-FLOOR-ROOM-67BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280759, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-106', N'BLOCK-1-FLOOR-ROOM-67BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280760, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-106', N'BLOCK-1-FLOOR-ROOM-67BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280761, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-107', N'BLOCK-1-FLOOR-ROOM-67BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280762, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-107', N'BLOCK-1-FLOOR-ROOM-67BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280763, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-107', N'BLOCK-1-FLOOR-ROOM-67BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280764, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-108', N'BLOCK-1-FLOOR-ROOM-67BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280765, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-108', N'BLOCK-1-FLOOR-ROOM-67BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280766, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-108', N'BLOCK-1-FLOOR-ROOM-67BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280767, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-109', N'BLOCK-1-FLOOR-ROOM-67BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280768, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-109', N'BLOCK-1-FLOOR-ROOM-67BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280769, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-109', N'BLOCK-1-FLOOR-ROOM-67BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280770, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-110', N'BLOCK-1-FLOOR-ROOM-68BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280771, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-110', N'BLOCK-1-FLOOR-ROOM-68BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280772, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-110', N'BLOCK-1-FLOOR-ROOM-68BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280773, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-111', N'BLOCK-1-FLOOR-ROOM-68BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280774, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-111', N'BLOCK-1-FLOOR-ROOM-68BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280775, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-111', N'BLOCK-1-FLOOR-ROOM-68BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280776, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-112', N'BLOCK-1-FLOOR-ROOM-68BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280777, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-112', N'BLOCK-1-FLOOR-ROOM-68BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280778, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-112', N'BLOCK-1-FLOOR-ROOM-68BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280779, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-113', N'BLOCK-1-FLOOR-ROOM-68BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280780, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-113', N'BLOCK-1-FLOOR-ROOM-68BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280781, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-113', N'BLOCK-1-FLOOR-ROOM-68BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280782, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-114', N'BLOCK-1-FLOOR-ROOM-68BED-1', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280783, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-114', N'BLOCK-1-FLOOR-ROOM-68BED-2', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Room_Bed] ([bed_id], [property_id], [block_id], [floor_id], [room_no], [bed_no], [price], [status], [display_name]) VALUES (280784, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-114', N'BLOCK-1-FLOOR-ROOM-68BED-3', 100, 0, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (100, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-100', N'ROOM NAME - 100', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (101, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-101', N'ROOM NAME - 101', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (102, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-102', N'ROOM NAME - 102', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (103, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-103', N'ROOM NAME - 103', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (104, 12, 16, 66, N'BLOCK-1-FLOOR-ROOM-104', N'ROOM NAME - 104', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (105, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-105', N'ROOM NAME - 105', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (106, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-106', N'ROOM NAME - 106', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (107, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-107', N'ROOM NAME - 107', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (108, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-108', N'ROOM NAME - 108', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (109, 12, 16, 67, N'BLOCK-1-FLOOR-ROOM-109', N'ROOM NAME - 109', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (110, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-110', N'ROOM NAME - 110', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (111, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-111', N'ROOM NAME - 111', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (112, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-112', N'ROOM NAME - 112', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (113, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-113', N'ROOM NAME - 113', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (114, 12, 16, 68, N'BLOCK-1-FLOOR-ROOM-114', N'ROOM NAME - 114', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (63, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-63', N'ROOM NAME - 63', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (64, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-64', N'ROOM NAME - 64', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (65, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-65', N'ROOM NAME - 65', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (66, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-66', N'ROOM NAME - 66', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (67, 13, 14, 60, N'BLOCK-1-FLOOR-ROOM-67', N'ROOM NAME - 67', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (68, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-68', N'ROOM NAME - 68', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (69, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-69', N'ROOM NAME - 69', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (70, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-70', N'ROOM NAME - 70', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (71, 13, 14, 61, N'BLOCK-1-FLOOR-ROOM-71', N'ROOM NAME - 71', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (72, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-72', N'ROOM NAME - 72', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (73, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-73', N'ROOM NAME - 73', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (74, 13, 14, 62, N'BLOCK-1-FLOOR-ROOM-74', N'ROOM NAME - 74', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (95, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-95', N'ROOM NAME - 95', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (96, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-96', N'ROOM NAME - 96', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (97, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-97', N'ROOM NAME - 97', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (98, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-98', N'ROOM NAME - 98', 3, NULL)
INSERT [dbo].[Property_Floor_Rooms] ([property_floor_room_id], [property_id], [property_block_id], [property_block_floor_id], [room_no], [room_name], [no_of_beds], [display_name]) VALUES (99, 12, 16, 65, N'BLOCK-1-FLOOR-ROOM-99', N'ROOM NAME - 99', 3, NULL)
INSERT [dbo].[Property_Lift_Inputs] ([Lift_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost]) VALUES (8, 12, 0, N'', N'', 0)
INSERT [dbo].[Property_Lift_Inputs] ([Lift_ID], [Property_ID], [Age], [Brand], [Warranty], [AMC_Cost]) VALUES (9, 13, 0, N'', N'', 0)
SET IDENTITY_INSERT [dbo].[Property_Lift_Photos] ON 

INSERT [dbo].[Property_Lift_Photos] ([Property_Lift_Photo_ID], [Property_ID], [Lift_ID], [Photo_Location], [Seq_ID]) VALUES (8, 12, 8, N'', 1)
INSERT [dbo].[Property_Lift_Photos] ([Property_Lift_Photo_ID], [Property_ID], [Lift_ID], [Photo_Location], [Seq_ID]) VALUES (9, 13, 9, N'', 1)
SET IDENTITY_INSERT [dbo].[Property_Lift_Photos] OFF
SET IDENTITY_INSERT [dbo].[Property_Lookup_Details] ON 

INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (1, 1, N'Select', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (2, 1, N'Standard', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (3, 2, N'Bad Road', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (4, 2, N'Far from demand center', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (5, 2, N'Neutral', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (6, 2, N'Good Road', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (7, 2, N'Close to demand center', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (8, 3, N'Cramped', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (9, 3, N'Small', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (10, 3, N'Neutral', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (11, 3, N'large', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (12, 4, N'Poorly Furnished', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (13, 4, N'Neutral', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (14, 4, N'Well Furnished', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (15, 5, N'Poorly Maintained', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (16, 5, N'Neutral', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (17, 5, N'Well Maintained', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (18, 6, N'Municipal', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (19, 6, N'Borewell', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (20, 6, N'Municipal+Borewell', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (21, 6, N'Only Tanker', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (22, 7, N'UnderGround Sump Capacity', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (23, 7, N'OverHead tank Capacity', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (24, 7, N'Both', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (25, 8, N'No Power backup', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (26, 8, N'Partial(common areas)', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (27, 8, N'Invertor', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (28, 8, N'Full DG Power', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (29, 9, N'Very Bad', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (30, 9, N'Bad', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (31, 9, N'Neutral', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (32, 9, N'Good', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (33, 9, N'Neat&Tidy', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (34, 10, N'Single Room', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (35, 10, N'Double Room', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (36, 10, N'Triple Room', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (37, 10, N'Four Per Room', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (38, 11, N'Men', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (39, 11, N'Women', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (40, 11, N'Unisex', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (41, 12, N'Biometric Main Door Access', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (42, 12, N'Biometric Attendance', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (43, 12, N'Security', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (44, 12, N'CC TV', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (45, 13, N'Fire extinguishers', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (46, 13, N'Smoke Detectors', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (47, 13, N'Water Pipeline', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (48, 14, N'3 Times', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (49, 14, N'2 Times', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (50, 14, N'Not Available', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (51, 15, N'> 10 Years', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (52, 15, N'5-10 Years', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (53, 15, N'2-5 Years', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (54, 15, N'Less than 2 Years', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (55, 16, N'Parking', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (56, 16, N'Internet/Wi-Fi', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (57, 16, N'Restaurant/Bar', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (58, 16, N'Business Facilities', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (59, 16, N'Swimming Pool', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (60, 16, N'Spa/Message/Wellness', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (61, 16, N'Gym', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (62, 16, N'Mess', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (63, 16, N'One Story', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (64, 16, N'Two Stories', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (65, 16, N'Three+ Stories', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (66, 17, N'BAD', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (67, 17, N'GOOD', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (68, 17, N'BEST', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (69, 18, N'Daily need Shops', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (70, 18, N'Auto', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (71, 18, N'Local newspaper supplier', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (72, 18, N'Others', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (73, 18, N'Auto Branding', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (74, 18, N'4-Wheeler Branding', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (75, 19, N'In Room', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (76, 19, N'Public Area', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (77, 19, N'Facade', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (78, 19, N'Vicinity', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (79, 20, N'In Room Marketing Stuff', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (80, 20, N'Facade Visibility', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (81, 20, N'Pamphlates', 1)
INSERT [dbo].[Property_Lookup_Details] ([Property_Lookup_Detail_id], [property_lookup_id], [property_lookup_options], [active_yn]) VALUES (82, 20, N'Directions boards to Property', 1)
SET IDENTITY_INSERT [dbo].[Property_Lookup_Details] OFF
SET IDENTITY_INSERT [dbo].[Property_Lookups] ON 

INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (1, N'PRTY_CATEGORIES', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (2, N'Location_Score', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (3, N'Room_Sizes', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (4, N'Room_Quality_Score', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (5, N'Room_Cleanliness_Score', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (6, N'Water_Source', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (7, N'Water_Storage_Capacity', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (8, N'Backup_Power', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (9, N'Common_Area_Cleanliness', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (10, N'Rent_Deposit_Types', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (11, N'Gender', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (12, N'Security_Features', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (13, N'Fire_Fighting_Equipment_Types', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (14, N'Food_Availability', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (15, N'Property_Age', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (16, N'Amenities', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (17, N'Food_Score', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (18, N'DetailsOfTie-upsAndBranding', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (19, N'PhotoShoot', 1)
INSERT [dbo].[Property_Lookups] ([property_lookup_id], [property_lookup_value], [active_yn]) VALUES (20, N'BTL_Activities', 1)
SET IDENTITY_INSERT [dbo].[Property_Lookups] OFF
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (1, N'Capex', N'AC', N'New AC''s', 4, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (2, N'Opex', N'AC', N'Air conditioning (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (3, N'Capex', N'CCTV', N'CCTV DVR', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (4, N'Capex', N'CCTV', N'Camera', 2, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (5, N'Capex', N'CCTV', N'Bio-Materic', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (6, N'Capex', N'CCTV', N'Door Access', 4, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (7, N'Opex', N'CCTV', N'CCTV DVR with Camera', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (8, N'Opex', N'CCTV', N'Bio-Materic (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (9, N'Opex', N'CCTV', N'Door Access (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (10, N'Capex', N'Chairs', N'New Chairs Purchase', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (11, N'Opex', N'Chairs', N'Chairs Repair', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (12, N'Opex', N'Electrical', N'DG Set (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (13, N'Opex', N'Electrical', N'DG Diesel Expences', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (14, N'Opex', N'Electrical', N'DG Maintence for 125 & 250 KVA(Oil Changes)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (15, N'Opex', N'Electrical', N'Electrical Maintenance - Manpower', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (16, N'Opex', N'Electrical', N'Electrical Matreial', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (17, N'Opex', N'Electrical', N'Electricity  Bills', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (18, N'Opex', N'Electrical', N'Transformer  Maintainance', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (19, N'Capex', N'EO', N'Vaccum Cleaner', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (20, N'Capex', N'Fire', N'Smoke Detectors', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (21, N'Capex', N'Fire', N'New Fire Alarm Panel', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (22, N'Capex', N'Fire', N'Fire Extinguisher', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (23, N'Opex', N'Fire', N'Fire Alarm Panel/ Smoke Detectors  (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (24, N'Opex', N'Fire', N'Fire Extinguisher (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (25, N'Opex', N'HK', N'House Keeping Manpower', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (26, N'Opex', N'HK', N'House Keeping Materials', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (27, N'Opex', N'Lift', N'Lift maintainance and AMC', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (28, N'Opex', N'maintenace', N'Plumbing (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (29, N'Opex', N'maintenace', N'Plumbing Material', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (30, N'Opex', N'maintenace', N'Carpenter (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (31, N'Opex', N'maintenace', N'Carpenter  Material', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (32, N'Opex', N'maintenace', N'HMWS & SB', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (33, N'Opex', N'maintenace', N'Painting & Repairs', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (34, N'Opex', N'maintenace', N'Record Management', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (35, N'Capex', N'Name', N'Name board LED for Corporate office', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (36, N'Opex', N'Pest control', N'Pest Control (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (37, N'Opex', N'Rent', N'Rent ', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (38, N'Opex', N'Rent', N'Maintenance', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (39, N'Opex', N'Security', N'Security Manpower', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (40, N'Opex', N'Staff Welfare', N'Coffee, Tea Premix  powder', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (41, N'Opex', N'Staff Welfare', N'Vending Machines', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (42, N'Opex', N'Staff Welfare', N'Water Dispensers', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (43, N'Opex', N'Staff Welfare', N'Mineral Water (Avg)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (44, N'Opex', N'Staff Welfare', N'Water Tanker ', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (45, N'Opex', N'Staff Welfare', N'Paper Cups (for daily use)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (46, N'Capex', N'Staff Welfare', N'Staff Lunch Plates, Spones, cups &Glass', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (47, N'Opex', N'Staff Welfare', N'Staff Welfare (Visiting Personnel, Guest etc.)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (48, N'Opex', N'stationery', N'Postage & Courier', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (49, N'Opex', N'stationery', N'Printing (visting cards, Id Cards,Tags,Letter Heads and other printing items', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (50, N'Opex', N'stationery', N'Stationery ', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (51, N'Opex', N'stationery', N'Photocopier', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (52, N'Capex', N'Storeage', N'Storage units', 5, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (53, N'Capex', N'Tele', N'New EPABX', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (54, N'Capex', N'Tele', N'Telephone Insturments', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (55, N'Opex', N'Tele', N'EPABX (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (56, N'Opex', N'Tele', N'Data Cards (Airtel & Tata)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (57, N'Opex', N'Tele', N'Video Conference Bill', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (58, N'Opex', N'Tele', N'Telephone Bills (Not Empl Reim)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (59, N'Capex', N'UPS', N'New Batteries', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (60, N'Opex', N'UPS', N'UPS (AMC)', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (61, N'Opex', N'Vechile', N'Vechile Maintance', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (62, N'Opex', N'Vechile', N'Diesel For Innova', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (63, N'Opex', N'Vechile', N'Vehicle Insurance', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (64, N'Opex', N'', N'Liasioning fee for Admin Related ', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_Opex_Capex_FAR] ([Property_Onboard_item_id], [financial_category_code], [subcategory], [item_name], [Life_of_Product], [Landlord_Placio], [property_id]) VALUES (65, N'Opex', N'', N'Miscellaneous ', NULL, N'', NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (1, NULL, N'Carpet tiles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (2, NULL, N'Wooden flooring in the server #', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (3, NULL, N'Workstation - Modularӳ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (4, NULL, N'Reg. Executive chairs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (5, NULL, N'Training # chairs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (6, NULL, N'Conference # furniture', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (7, NULL, N'Meeting # furniture', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (8, NULL, N'COO & Buss.. head  # furniture', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (9, NULL, N'Mgr. cabins furniture', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (10, NULL, N'Front Desk furniture', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (11, NULL, N'Security Table', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (12, NULL, N'Canteen Furniture', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (13, NULL, N'Air-conditioning', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (14, NULL, N'DG set', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (15, NULL, N'Elevation internal college staircase', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (16, NULL, N'Main elevation external ֠stainless steel railing & side cover', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (17, NULL, N'All Access control doors', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (18, NULL, N'Complete Bldg. Glass Fasad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (19, NULL, N'Staff Lockers', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (20, NULL, N'Library racks', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (21, NULL, N'Electrical on the floors, Staircase, Terrace & External', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (22, NULL, N'Fire Extinguishers', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (23, NULL, N'Artifacts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (24, NULL, N'Internal Signages', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (25, NULL, N'Venation blinds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (26, NULL, N'Toilet civil works & plumbing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (27, NULL, N'Toilet tile daddoing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (28, NULL, N'Glazing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (29, NULL, N'Electrical wire conduiting', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (30, NULL, N'Data cable trenching & raceways', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (31, NULL, N'Screeding concrete', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (32, NULL, N'Electrical wiring', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (33, NULL, N'A/C Ducting', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (34, NULL, N'Gypsum wall framing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (35, NULL, N'Glass Door fabrications', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (36, NULL, N'False ceiling Framing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (37, NULL, N'Elect & elect switch mounting', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (38, NULL, N'Gypsum wall finishing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (39, NULL, N'False ceiling board fixing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (40, NULL, N'Ceiling Light fitting fixing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (41, NULL, N'Glass Door fixing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (42, NULL, N'Structured cabling', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (43, NULL, N'Painting & Finishing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (44, NULL, N'Flooring & False flooring', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (45, NULL, N'Workstation & Tables Fit outs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (46, NULL, N'Workstation wiring & cabling', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (47, NULL, N'Data cable crimping & termination', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (48, NULL, N'Final fit outs / d꤯r & handing over', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (49, NULL, N'Desktops', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (50, NULL, N'Server', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (51, NULL, N'Cisco Switches', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (52, NULL, N'Routers', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (53, NULL, N'Fotigate', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (54, NULL, N'Fotigate', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (55, NULL, N'CCTV', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (56, NULL, N'Fire Alarm sensors & hooters', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (57, NULL, N'Access Control', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (58, NULL, N'Bio-metric', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (59, NULL, N'Reliance & Tata ILP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (60, NULL, N'Airtel Telephone lines', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (61, NULL, N'EPABX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (62, NULL, N'Telphone instruments', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (63, NULL, N'Polycom instrument', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (64, NULL, N'Acer Projector', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (65, NULL, N'Drop screen & white board', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (66, NULL, N'Micowave Oven', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (67, NULL, N'Gym equipments', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (68, NULL, N'Bunker beds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (69, NULL, N'Head Phones', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (70, NULL, N'Foot Pedals', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (71, NULL, N'External Signage', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (72, NULL, N'Plasma TV', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (73, NULL, N'Fridge', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (74, NULL, N'Ricoh Printer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Onboard_TimeLines_Template] ([property_timeline_id], [item_code], [item_name], [delivery_date], [installation], [commissioning], [dependency], [landlord_completion_date], [comment_1], [comment_2], [placio_targeted_date]) VALUES (75, NULL, N'passive networking', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Owner] ([property_owner_id], [property_id], [owner_name], [Gender], [aggrement_tenure], [agreement_expire], [aadhar_file], [aadhar_no], [pan_file], [pan_no], [cancel_check], [lease_agreement], [created_at]) VALUES (9, 12, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Property_Owner] ([property_owner_id], [property_id], [owner_name], [Gender], [aggrement_tenure], [agreement_expire], [aadhar_file], [aadhar_no], [pan_file], [pan_no], [cancel_check], [lease_agreement], [created_at]) VALUES (10, 13, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Property_Photos] ON 

INSERT [dbo].[Property_Photos] ([Property_Photo_ID], [Property_ID], [Photo_Location], [Seq_ID]) VALUES (48, 12, N'', 1)
INSERT [dbo].[Property_Photos] ([Property_Photo_ID], [Property_ID], [Photo_Location], [Seq_ID]) VALUES (49, 13, N'', 1)
SET IDENTITY_INSERT [dbo].[Property_Photos] OFF
SET IDENTITY_INSERT [dbo].[Property_Revenue_Sharing_Types] ON 

INSERT [dbo].[Property_Revenue_Sharing_Types] ([revenue_sharing_type_id], [revenue_sharing_type], [active_yn]) VALUES (1, N'Flag Ship', 1)
INSERT [dbo].[Property_Revenue_Sharing_Types] ([revenue_sharing_type_id], [revenue_sharing_type], [active_yn]) VALUES (2, N'Revenue Sharing - On Bed Wise', 1)
INSERT [dbo].[Property_Revenue_Sharing_Types] ([revenue_sharing_type_id], [revenue_sharing_type], [active_yn]) VALUES (3, N'Revenue Sharing - On Property Revenue', 1)
SET IDENTITY_INSERT [dbo].[Property_Revenue_Sharing_Types] OFF
SET IDENTITY_INSERT [dbo].[Property_Security_Features] ON 

INSERT [dbo].[Property_Security_Features] ([Property_Security_Feature_ID], [Property_ID], [Security_Feature_ID]) VALUES (5, 12, 0)
INSERT [dbo].[Property_Security_Features] ([Property_Security_Feature_ID], [Property_ID], [Security_Feature_ID]) VALUES (6, 13, 0)
SET IDENTITY_INSERT [dbo].[Property_Security_Features] OFF
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Active                                            ', N'This property is listed with a Realtor that does not have an accepted contract. This does not mean that there could not be offers any offers on the table it just means that the seller has not accepted an offer.')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Active Contingent                                 ', N'This means that the seller has accepted an offer on the property but certain things must happen for the buyer to be able to close on the property. Buyers who submit contingent offers typically need to secure financing after the sale of their existing home but again the reason for a contingent contract will vary from contract to contract.')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Active Option                                     ', N'here is an accepted contract on this home however the buyers Realtor has a negotiated an amount of time that the buyer has the unrestricted right to walk away from the contract FOR ANY REASON and still receive their earnest money deposit. However the fee that was paid for the unrestricted right to walk away, also know as the option fee, is given to the seller as settlement for the time off the market, should the buyer decided to walk away from the contract during the option period. The number of days that the parties agree to will vary from contract to contract and the amount of money that the buyer will pay for this right will vary as well. The buyer will typically have property inspections done during this time and negotiate repairs.')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Cancelled                                         ', N'The home has been taken off the market. This happens most often when the seller has changed their mind decided not to sell the home and terminated the listing agreement with their listing agent.')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Expired                                           ', N'The general public often times will not see this as an advertised status. Basically this means that the listing agreement that the sellers signed with their listing agent is now expired and the home is off the market until the seller decides to put it back on the market.')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Pending                                           ', N'You will see this status once the seller has an accepted offer and the option period is no longer in effect. All parties involved are waiting for the closing documents to be prepared and signed. Most real estate websites will no longer advertise properties once they are placed into a Pending status. Properties in pending status are no longer allowed to be shown by Realtors unless granted special permission by the owners.')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Sold                                              ', N'The chain of title has passed to the new owners and the home is no longer available for sale.')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Sole Ownership                                    ', N'')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Temporarily off the Market (TOM)                  ', N'We see this status from time to time and it typically has something to do with a sudden unforeseen circumstance but the seller would still like to sell the property. The listing agents contract to list the home is still in effect however they must stop marketing efforts until the time that the seller instructs their agent to actively market the home again or the listing agreement expires.')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Tenancy                                           ', N'')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Transfer Through Probate Process                  ', N'')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Trust Ownership                                   ', N'')
INSERT [dbo].[Property_Status] ([property_status_code], [property_status_desc]) VALUES (N'Withdrawn                                         ', N'Also a status for a home that is no longer available for sale. The seller may withdraw a property from the market because they intend to try a new marketing strategy. The reason will vary from seller to seller however the result is still the same. The property is no longer available')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'1              ', N'Hotel')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'10             ', N'Couple Friendly')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'11             ', N'Hostel')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'12             ', N'Bungalow')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'13             ', N'Cottage')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'14             ', N'Motel')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'15             ', N'Single Family')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'16             ', N'Condo')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'17             ', N'Townhouse')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'18             ', N'Duplex')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'19             ', N'Half Duplex')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'2              ', N'Apartment')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'20             ', N'Multi Family')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'21             ', N'Mobile Home')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'22             ', N'Residential Lease')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'23             ', N'Lots/Acreage')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'24             ', N'Farm/Hobby')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'25             ', N'Commercial')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'26             ', N'Commercial Lease')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'3              ', N'Guest House')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'4              ', N'Homestay')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'5              ', N'Lodge')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'6              ', N'Resort')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'7              ', N'Villa')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'8              ', N'Farm House')
INSERT [dbo].[Property_Types] ([property_type_code], [property_type_desc]) VALUES (N'9              ', N'BnB')
INSERT [dbo].[Qualifications] ([qualification_code], [qualification_title], [qualification_desc]) VALUES (N'1              ', N'business studies', NULL)
INSERT [dbo].[Qualifications] ([qualification_code], [qualification_title], [qualification_desc]) VALUES (N'2              ', N'health and safety', NULL)
INSERT [dbo].[Qualifications] ([qualification_code], [qualification_title], [qualification_desc]) VALUES (N'3              ', N'hospitality management', NULL)
INSERT [dbo].[Qualifications] ([qualification_code], [qualification_title], [qualification_desc]) VALUES (N'4              ', N'hospitality and tourism management', NULL)
INSERT [dbo].[Qualifications] ([qualification_code], [qualification_title], [qualification_desc]) VALUES (N'5              ', N'hotel and catering', NULL)
INSERT [dbo].[Qualifications] ([qualification_code], [qualification_title], [qualification_desc]) VALUES (N'6              ', N'human resources', NULL)
INSERT [dbo].[Qualifications] ([qualification_code], [qualification_title], [qualification_desc]) VALUES (N'7              ', N'management', NULL)
INSERT [dbo].[Readiness_for_Procure_Categories] ([readiness_for_procure_category_code], [readiness_for_procure_category_name]) VALUES (N'1              ', N'Legal Documents')
INSERT [dbo].[Readiness_for_Procure_Categories] ([readiness_for_procure_category_code], [readiness_for_procure_category_name]) VALUES (N'2              ', N'Property Documens')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (1, N'2              ', N'Sale Deed')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (2, N'2              ', N'Extracts')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (3, N'2              ', N'Mutation Register Extract')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (4, N'2              ', N'General Power of Attorney')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (5, N'2              ', N'Copy of Building Plan')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (6, N'2              ', N'No Objection Certificate (NOC)')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (7, N'2              ', N'Allotment Letter')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (8, N'2              ', N'Sale Agreement')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (9, N'2              ', N'Possession Letter')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (10, N'2              ', N'Payment Receipts')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (11, N'2              ', N'Property Tax Receipts')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (12, N'2              ', N'Encumbrance Certificate')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (13, N'2              ', N'Completion Certificate')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (14, N'2              ', N'Occupancy Certificate')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (15, N'1              ', N'Agreement to Sell')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (16, N'1              ', N'Absolute sale deed and title deed')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (17, N'1              ', N'Title search and report')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (18, N'1              ', N'Khata Certificate')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (19, N'1              ', N'Receipt of Property tax')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (20, N'1              ', N'Encumbrance Certificate')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (21, N'1              ', N'Occupancy Certificate')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (22, N'1              ', N'Satement from bank if loan outstanding')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (23, N'1              ', N'Non-Objection Certificate')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (24, N'1              ', N'Sanctioned building plan by Statutory Authority')
INSERT [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id], [readiness_for_procure_category_code], [readiness_for_procure_checklist_item_name]) VALUES (25, N'1              ', N'Power of Attorney')
INSERT [dbo].[Relationship_Types] ([relationship_type_code], [relationship_type_desc]) VALUES (N'1              ', N'FATHER')
INSERT [dbo].[Relationship_Types] ([relationship_type_code], [relationship_type_desc]) VALUES (N'2              ', N'MOTHER')
INSERT [dbo].[Relationship_Types] ([relationship_type_code], [relationship_type_desc]) VALUES (N'3              ', N'GAUDIAN')
INSERT [dbo].[Relationship_Types] ([relationship_type_code], [relationship_type_desc]) VALUES (N'4              ', N'FRIEND')
INSERT [dbo].[Relationship_Types] ([relationship_type_code], [relationship_type_desc]) VALUES (N'5              ', N'PARENT')
INSERT [dbo].[Relationship_Types] ([relationship_type_code], [relationship_type_desc]) VALUES (N'6              ', N'FAMILY MEMBER')
INSERT [dbo].[Relationship_Types] ([relationship_type_code], [relationship_type_desc]) VALUES (N'7              ', N'CLASS MATE')
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([role_id], [role_name], [active_yn]) VALUES (1, N'Board of Directors (BOD)', 1)
INSERT [dbo].[Roles] ([role_id], [role_name], [active_yn]) VALUES (2, N'CEO/CBO', 1)
INSERT [dbo].[Roles] ([role_id], [role_name], [active_yn]) VALUES (3, N'Country Head (Operations)', 1)
INSERT [dbo].[Roles] ([role_id], [role_name], [active_yn]) VALUES (4, N'Cluster Head (Operations)', 1)
INSERT [dbo].[Roles] ([role_id], [role_name], [active_yn]) VALUES (5, N'Sr. Manager Operations', 1)
INSERT [dbo].[Roles] ([role_id], [role_name], [active_yn]) VALUES (6, N'Property Manager', 1)
INSERT [dbo].[Roles] ([role_id], [role_name], [active_yn]) VALUES (7, N'Warden / Caretaker', 1)
SET IDENTITY_INSERT [dbo].[Roles] OFF
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'1', N'Private Room')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'10', N'Dorms without Bunk Beds')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'11', N'Dorms with Shared Bathroom')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'12', N'Dorms with Ensuite Facilities')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'13', N'Pod-Styled dorms')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'2', N'Ensuite Room')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'3', N'Tent')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'4', N'Dorm Room')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'5', N'Twin Room')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'6', N'Double Room')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'7', N'Single Room')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'8', N'Female Dorm')
INSERT [dbo].[Room_Types] ([room_type_code], [room_type]) VALUES (N'9', N'Dorm with regular bunk beds')
SET IDENTITY_INSERT [dbo].[Suppliers] ON 

INSERT [dbo].[Suppliers] ([supplier_id], [supplier_code], [supplier_name], [supplier_email], [supplier_phone], [supplier_cellphone]) VALUES (1, N'SUP-0001       ', N'SUPPLIER-0001', N'SUPPLIER-0001@gmail.com', N'1234567891', N'1234567891')
INSERT [dbo].[Suppliers] ([supplier_id], [supplier_code], [supplier_name], [supplier_email], [supplier_phone], [supplier_cellphone]) VALUES (2, N'SUP-0002       ', N'SUPPLIER-0002', N'SUPPLIER-0002@gmail.com', N'2469135782', N'1234567891')
INSERT [dbo].[Suppliers] ([supplier_id], [supplier_code], [supplier_name], [supplier_email], [supplier_phone], [supplier_cellphone]) VALUES (3, N'SUP-0003       ', N'SUPPLIER-0003', N'SUPPLIER-0003@gmail.com', N'3703703673', N'1234567891')
INSERT [dbo].[Suppliers] ([supplier_id], [supplier_code], [supplier_name], [supplier_email], [supplier_phone], [supplier_cellphone]) VALUES (4, N'SUP-0004       ', N'SUPPLIER-0004', N'SUPPLIER-0004@gmail.com', N'4938271564', N'1234567891')
INSERT [dbo].[Suppliers] ([supplier_id], [supplier_code], [supplier_name], [supplier_email], [supplier_phone], [supplier_cellphone]) VALUES (5, N'SUP-0005       ', N'SUPPLIER-0005', N'SUPPLIER-0005@gmail.com', N'6172839455', N'1234567891')
INSERT [dbo].[Suppliers] ([supplier_id], [supplier_code], [supplier_name], [supplier_email], [supplier_phone], [supplier_cellphone]) VALUES (6, N'SUP-0006       ', N'SUPPLIER-0006', N'SUPPLIER-0006@gmail.com', N'7407407346', N'1234567891')
INSERT [dbo].[Suppliers] ([supplier_id], [supplier_code], [supplier_name], [supplier_email], [supplier_phone], [supplier_cellphone]) VALUES (7, N'SUP-0007       ', N'SUPPLIER-0007', N'SUPPLIER-0007@gmail.com', N'8641975237', N'1234567891')
INSERT [dbo].[Suppliers] ([supplier_id], [supplier_code], [supplier_name], [supplier_email], [supplier_phone], [supplier_cellphone]) VALUES (8, N'SUP-0008       ', N'SUPPLIER-0008', N'SUPPLIER-0008@gmail.com', N'9876543128', N'1234567891')
INSERT [dbo].[Suppliers] ([supplier_id], [supplier_code], [supplier_name], [supplier_email], [supplier_phone], [supplier_cellphone]) VALUES (9, N'SUP-0009       ', N'SUPPLIER-0009', N'SUPPLIER-0009@gmail.com', N'11111111019', N'1234567891')
INSERT [dbo].[Suppliers] ([supplier_id], [supplier_code], [supplier_name], [supplier_email], [supplier_phone], [supplier_cellphone]) VALUES (10, N'SUP-0010       ', N'SUPPLIER-0010', N'SUPPLIER-0010@gmail.com', N'12345678910', N'1234567891')
SET IDENTITY_INSERT [dbo].[Suppliers] OFF
INSERT [dbo].[Types_of_Food] ([food_type_code], [food_type_description]) VALUES (N'ARABIAN        ', NULL)
INSERT [dbo].[Types_of_Food] ([food_type_code], [food_type_description]) VALUES (N'CHINESE        ', NULL)
INSERT [dbo].[Types_of_Food] ([food_type_code], [food_type_description]) VALUES (N'DESSERT        ', NULL)
INSERT [dbo].[Types_of_Food] ([food_type_code], [food_type_description]) VALUES (N'HYDERABADI     ', NULL)
INSERT [dbo].[Types_of_Food] ([food_type_code], [food_type_description]) VALUES (N'INDIAN         ', NULL)
INSERT [dbo].[Types_of_Food] ([food_type_code], [food_type_description]) VALUES (N'LEBANESE       ', NULL)
INSERT [dbo].[Types_of_Food] ([food_type_code], [food_type_description]) VALUES (N'MUGHLAI        ', NULL)
INSERT [dbo].[Types_of_Food] ([food_type_code], [food_type_description]) VALUES (N'MULTI-CUISINE  ', NULL)
INSERT [dbo].[Types_of_Food] ([food_type_code], [food_type_description]) VALUES (N'NORTH INDIAN   ', NULL)
INSERT [dbo].[Types_of_Food] ([food_type_code], [food_type_description]) VALUES (N'SOUTH INDIAN   ', NULL)
ALTER TABLE [dbo].[AQ_Property] ADD  CONSTRAINT [DF_PROPERTY_IsNewProperty]  DEFAULT ((1)) FOR [IsNewProperty]
GO
ALTER TABLE [dbo].[AQ_Property] ADD  CONSTRAINT [DF__PROPERTY__Room_w__314D4EA8]  DEFAULT ((0)) FOR [Room_wise_Power_Meter_ID]
GO
ALTER TABLE [dbo].[AQ_Property] ADD  CONSTRAINT [DF__PROPERTY__Recept__324172E1]  DEFAULT ((0)) FOR [Reception_Area_yn]
GO
ALTER TABLE [dbo].[AQ_Property] ADD  CONSTRAINT [DF__PROPERTY__IsCant__3335971A]  DEFAULT ((0)) FOR [IsCanteenExists]
GO
ALTER TABLE [dbo].[AQ_Property] ADD  CONSTRAINT [DF_Property_IsCheckListDone]  DEFAULT ((0)) FOR [IsCheckListDone]
GO
ALTER TABLE [dbo].[AQ_Property] ADD  CONSTRAINT [DF_AQ_Property_Published_yn]  DEFAULT ((0)) FOR [Published_yn]
GO
ALTER TABLE [dbo].[C_Property] ADD  DEFAULT ((1)) FOR [IsCompetatorProperty]
GO
ALTER TABLE [dbo].[Complaint_Types] ADD  CONSTRAINT [DF_Complaint_Types_active_yn]  DEFAULT ((1)) FOR [active_yn]
GO
ALTER TABLE [dbo].[Complaints] ADD  DEFAULT (getdate()) FOR [Date_of_Submission]
GO
ALTER TABLE [dbo].[Complaints_Details] ADD  DEFAULT ((0)) FOR [escalated_yn]
GO
ALTER TABLE [dbo].[Complaints_Details] ADD  DEFAULT ((0)) FOR [resolved_yn]
GO
ALTER TABLE [dbo].[Employee_Source_Types] ADD  CONSTRAINT [DF_Employee_Source_Types_active_yn]  DEFAULT ((1)) FOR [active_yn]
GO
ALTER TABLE [dbo].[expense_category] ADD  DEFAULT (NULL) FOR [employee_id]
GO
ALTER TABLE [dbo].[expense_category] ADD  DEFAULT (NULL) FOR [category_name]
GO
ALTER TABLE [dbo].[expenses] ADD  DEFAULT (NULL) FOR [employee_id]
GO
ALTER TABLE [dbo].[expenses] ADD  DEFAULT (NULL) FOR [expense_date]
GO
ALTER TABLE [dbo].[expenses] ADD  DEFAULT (NULL) FOR [description]
GO
ALTER TABLE [dbo].[expenses] ADD  DEFAULT (NULL) FOR [amount]
GO
ALTER TABLE [dbo].[expenses] ADD  DEFAULT (NULL) FOR [expense_category_id]
GO
ALTER TABLE [dbo].[expenses] ADD  DEFAULT (NULL) FOR [upload_receipt]
GO
ALTER TABLE [dbo].[Food_Menu] ADD  CONSTRAINT [DF_Food_Menu_active_yn]  DEFAULT ((1)) FOR [active_yn]
GO
ALTER TABLE [dbo].[Income] ADD  DEFAULT (NULL) FOR [employee_id]
GO
ALTER TABLE [dbo].[Income] ADD  DEFAULT (NULL) FOR [income_date]
GO
ALTER TABLE [dbo].[Income] ADD  DEFAULT (NULL) FOR [description]
GO
ALTER TABLE [dbo].[Income] ADD  DEFAULT (NULL) FOR [amount]
GO
ALTER TABLE [dbo].[Income] ADD  DEFAULT (NULL) FOR [income_category_id]
GO
ALTER TABLE [dbo].[Income] ADD  DEFAULT (NULL) FOR [upload_receipt]
GO
ALTER TABLE [dbo].[Income_Category] ADD  DEFAULT (NULL) FOR [employee_id]
GO
ALTER TABLE [dbo].[Income_Category] ADD  DEFAULT (NULL) FOR [category_name]
GO
ALTER TABLE [dbo].[Property_Amenities] ADD  DEFAULT ((1)) FOR [active_yn]
GO
ALTER TABLE [dbo].[Property_Employees] ADD  CONSTRAINT [DF_Property_Employees_active_yn]  DEFAULT ((1)) FOR [active_yn]
GO
ALTER TABLE [dbo].[Property_Lookup_Details] ADD  DEFAULT ((1)) FOR [active_yn]
GO
ALTER TABLE [dbo].[Property_Lookups] ADD  DEFAULT ((1)) FOR [active_yn]
GO
ALTER TABLE [dbo].[Property_Revenue_Sharing_Types] ADD  DEFAULT ((0)) FOR [active_yn]
GO
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_active_yn]  DEFAULT ((1)) FOR [active_yn]
GO
ALTER TABLE [dbo].[AQ_Property_Diesel_Generator_Photos]  WITH NOCHECK ADD  CONSTRAINT [FK_AQ_Property_Diesel_Generator_Photos_AQ_Property_Diesel_Generator] FOREIGN KEY([Diesel_Generator_ID])
REFERENCES [dbo].[AQ_Property_Diesel_Generator] ([Diesel_Generator_ID])
GO
ALTER TABLE [dbo].[AQ_Property_Diesel_Generator_Photos] NOCHECK CONSTRAINT [FK_AQ_Property_Diesel_Generator_Photos_AQ_Property_Diesel_Generator]
GO
ALTER TABLE [dbo].[AQ_Property_Lift_Photos]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Lift_Photos_Property_Lift_Inputs] FOREIGN KEY([Lift_ID])
REFERENCES [dbo].[AQ_Property_Lift_Inputs] ([Lift_ID])
GO
ALTER TABLE [dbo].[AQ_Property_Lift_Photos] NOCHECK CONSTRAINT [FK_Property_Lift_Photos_Property_Lift_Inputs]
GO
ALTER TABLE [dbo].[Category_Items]  WITH NOCHECK ADD  CONSTRAINT [FK_CATEGORY_ITEMS_ITEM_CATEGORIES] FOREIGN KEY([ITEM_CATEGORY_ID])
REFERENCES [dbo].[Item_Categories] ([ITEM_CATEGORY_ID])
GO
ALTER TABLE [dbo].[Category_Items] NOCHECK CONSTRAINT [FK_CATEGORY_ITEMS_ITEM_CATEGORIES]
GO
ALTER TABLE [dbo].[Category_Items]  WITH NOCHECK ADD  CONSTRAINT [FK_CATEGORY_ITEMS_ITEM_SUB_CATEGORIES] FOREIGN KEY([ITEM_CATEGORY_SUB_ID])
REFERENCES [dbo].[Item_Sub_Categories] ([ITEM_CATEGORY_SUB_ID])
GO
ALTER TABLE [dbo].[Category_Items] NOCHECK CONSTRAINT [FK_CATEGORY_ITEMS_ITEM_SUB_CATEGORIES]
GO
ALTER TABLE [dbo].[Complaints]  WITH NOCHECK ADD  CONSTRAINT [FK_Complaints_Complaint_Types] FOREIGN KEY([Complaint_Type_ID])
REFERENCES [dbo].[Complaint_Types] ([complaint_type_id])
GO
ALTER TABLE [dbo].[Complaints] NOCHECK CONSTRAINT [FK_Complaints_Complaint_Types]
GO
ALTER TABLE [dbo].[Complaints_Details]  WITH NOCHECK ADD  CONSTRAINT [FK_Complaints_Details_Complaints] FOREIGN KEY([complaint_ID])
REFERENCES [dbo].[Complaints] ([Complaint_ID])
GO
ALTER TABLE [dbo].[Complaints_Details] NOCHECK CONSTRAINT [FK_Complaints_Details_Complaints]
GO
ALTER TABLE [dbo].[Complaints_Details]  WITH NOCHECK ADD  CONSTRAINT [FK_Complaints_Details_Property] FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Complaints_Details] NOCHECK CONSTRAINT [FK_Complaints_Details_Property]
GO
ALTER TABLE [dbo].[Employee_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_Addresses_Address_Types] FOREIGN KEY([address_type_code])
REFERENCES [dbo].[Address_Types] ([address_type_code])
GO
ALTER TABLE [dbo].[Employee_Addresses] NOCHECK CONSTRAINT [FK_Employee_Addresses_Address_Types]
GO
ALTER TABLE [dbo].[Employee_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_Addresses_Addresses] FOREIGN KEY([address_id])
REFERENCES [dbo].[Addresses] ([address_id])
GO
ALTER TABLE [dbo].[Employee_Addresses] NOCHECK CONSTRAINT [FK_Employee_Addresses_Addresses]
GO
ALTER TABLE [dbo].[Employee_Qualifications]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_Qualifications_Qualifications] FOREIGN KEY([qualification_code])
REFERENCES [dbo].[Qualifications] ([qualification_code])
GO
ALTER TABLE [dbo].[Employee_Qualifications] NOCHECK CONSTRAINT [FK_Employee_Qualifications_Qualifications]
GO
ALTER TABLE [dbo].[Employee_Titles]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_Titles_Job_Titles] FOREIGN KEY([job_title_code])
REFERENCES [dbo].[Job_Titles] ([job_title_code])
GO
ALTER TABLE [dbo].[Employee_Titles] NOCHECK CONSTRAINT [FK_Employee_Titles_Job_Titles]
GO
ALTER TABLE [dbo].[expenses]  WITH NOCHECK ADD  CONSTRAINT [FK_expenses_expense_category] FOREIGN KEY([expense_category_id])
REFERENCES [dbo].[expense_category] ([expense_category_id])
GO
ALTER TABLE [dbo].[expenses] NOCHECK CONSTRAINT [FK_expenses_expense_category]
GO
ALTER TABLE [dbo].[Income]  WITH NOCHECK ADD  CONSTRAINT [FK_income_income_category] FOREIGN KEY([income_category_id])
REFERENCES [dbo].[Income_Category] ([income_category_id])
GO
ALTER TABLE [dbo].[Income] NOCHECK CONSTRAINT [FK_income_income_category]
GO
ALTER TABLE [dbo].[Inventory_Item_Details]  WITH NOCHECK ADD  CONSTRAINT [FK_Inventory_Item_Details_Inventory_Items] FOREIGN KEY([item_id])
REFERENCES [dbo].[Inventory_Items] ([item_id])
GO
ALTER TABLE [dbo].[Inventory_Item_Details] NOCHECK CONSTRAINT [FK_Inventory_Item_Details_Inventory_Items]
GO
ALTER TABLE [dbo].[Inventory_Items]  WITH NOCHECK ADD  CONSTRAINT [FK_Inventory_Items_Brands] FOREIGN KEY([brand_id])
REFERENCES [dbo].[Brands] ([brand_id])
GO
ALTER TABLE [dbo].[Inventory_Items] NOCHECK CONSTRAINT [FK_Inventory_Items_Brands]
GO
ALTER TABLE [dbo].[Item_Stock_Levels]  WITH NOCHECK ADD  CONSTRAINT [FK_Item_Stock_Levels_Inventory_Items] FOREIGN KEY([item_id])
REFERENCES [dbo].[Inventory_Items] ([item_id])
GO
ALTER TABLE [dbo].[Item_Stock_Levels] NOCHECK CONSTRAINT [FK_Item_Stock_Levels_Inventory_Items]
GO
ALTER TABLE [dbo].[Item_Sub_Categories]  WITH NOCHECK ADD  CONSTRAINT [FK_ITEM_SUB_CATEGORIES_ITEM_CATEGORIES] FOREIGN KEY([ITEM_CATEGORY_ID])
REFERENCES [dbo].[Item_Categories] ([ITEM_CATEGORY_ID])
GO
ALTER TABLE [dbo].[Item_Sub_Categories] NOCHECK CONSTRAINT [FK_ITEM_SUB_CATEGORIES_ITEM_CATEGORIES]
GO
ALTER TABLE [dbo].[Item_SubCategory]  WITH CHECK ADD  CONSTRAINT [FK_item_subcategory_financial_item_categories] FOREIGN KEY([financial_category_code])
REFERENCES [dbo].[Financial_Item_Categories] ([financial_category_code])
GO
ALTER TABLE [dbo].[Item_SubCategory] CHECK CONSTRAINT [FK_item_subcategory_financial_item_categories]
GO
ALTER TABLE [dbo].[Item_Suppliers]  WITH NOCHECK ADD  CONSTRAINT [FK_Item_Suppliers_Inventory_Items] FOREIGN KEY([Item_id])
REFERENCES [dbo].[Inventory_Items] ([item_id])
GO
ALTER TABLE [dbo].[Item_Suppliers] NOCHECK CONSTRAINT [FK_Item_Suppliers_Inventory_Items]
GO
ALTER TABLE [dbo].[Item_Suppliers]  WITH NOCHECK ADD  CONSTRAINT [FK_Item_Suppliers_Suppliers] FOREIGN KEY([supplier_code])
REFERENCES [dbo].[Suppliers] ([supplier_code])
GO
ALTER TABLE [dbo].[Item_Suppliers] NOCHECK CONSTRAINT [FK_Item_Suppliers_Suppliers]
GO
ALTER TABLE [dbo].[Properties_Readiness_for_Procure_Checklist_Items]  WITH NOCHECK ADD  CONSTRAINT [FK_Properties_Readiness_for_Procure_Checklist_Items_PR_Procure_Checklist_Status] FOREIGN KEY([property_id], [readiness_for_procure_checklist_item_id])
REFERENCES [dbo].[PR_Procure_Checklist_Status] ([property_id], [readiness_for_procure_checklist_item_id])
GO
ALTER TABLE [dbo].[Properties_Readiness_for_Procure_Checklist_Items] NOCHECK CONSTRAINT [FK_Properties_Readiness_for_Procure_Checklist_Items_PR_Procure_Checklist_Status]
GO
ALTER TABLE [dbo].[Properties_Readiness_for_Procure_Checklist_Items]  WITH NOCHECK ADD  CONSTRAINT [FK_Properties_Readiness_for_Procure_Checklist_Items_Readiness_for_Procure_Checklist_Items] FOREIGN KEY([readiness_for_procure_checklist_item_id])
REFERENCES [dbo].[Readiness_for_Procure_Checklist_Items] ([readiness_for_procure_checklist_item_id])
GO
ALTER TABLE [dbo].[Properties_Readiness_for_Procure_Checklist_Items] NOCHECK CONSTRAINT [FK_Properties_Readiness_for_Procure_Checklist_Items_Readiness_for_Procure_Checklist_Items]
GO
ALTER TABLE [dbo].[Property_Amenities]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Amenities_Amenities] FOREIGN KEY([amenity_id])
REFERENCES [dbo].[Amenities] ([amenity_id])
GO
ALTER TABLE [dbo].[Property_Amenities] NOCHECK CONSTRAINT [FK_Property_Amenities_Amenities]
GO
ALTER TABLE [dbo].[Property_Block]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Block_Property] FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Block] NOCHECK CONSTRAINT [FK_Property_Block_Property]
GO
ALTER TABLE [dbo].[Property_Block_Floor]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Block_Floor_Property] FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Block_Floor] NOCHECK CONSTRAINT [FK_Property_Block_Floor_Property]
GO
ALTER TABLE [dbo].[Property_Block_Floor]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Block_Floor_Property_Block] FOREIGN KEY([property_block_id])
REFERENCES [dbo].[Property_Block] ([property_block_id])
GO
ALTER TABLE [dbo].[Property_Block_Floor] NOCHECK CONSTRAINT [FK_Property_Block_Floor_Property_Block]
GO
ALTER TABLE [dbo].[Property_Canteen]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Canteen_Property] FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Canteen] NOCHECK CONSTRAINT [FK_Property_Canteen_Property]
GO
ALTER TABLE [dbo].[Property_Canteen_Menu]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Canteen_Menu_Food_Menu] FOREIGN KEY([food_menu_id])
REFERENCES [dbo].[Food_Menu] ([food_menu_id])
GO
ALTER TABLE [dbo].[Property_Canteen_Menu] NOCHECK CONSTRAINT [FK_Property_Canteen_Menu_Food_Menu]
GO
ALTER TABLE [dbo].[Property_Canteen_Menu]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Canteen_Menu_Property] FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Canteen_Menu] NOCHECK CONSTRAINT [FK_Property_Canteen_Menu_Property]
GO
ALTER TABLE [dbo].[Property_Canteen_Owner]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Canteen_Owner_Property] FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Canteen_Owner] NOCHECK CONSTRAINT [FK_Property_Canteen_Owner_Property]
GO
ALTER TABLE [dbo].[Property_Canteen_Owner_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Canteen_Owner_Addresses_Address_Types] FOREIGN KEY([address_type_code])
REFERENCES [dbo].[Address_Types] ([address_type_code])
GO
ALTER TABLE [dbo].[Property_Canteen_Owner_Addresses] NOCHECK CONSTRAINT [FK_Property_Canteen_Owner_Addresses_Address_Types]
GO
ALTER TABLE [dbo].[Property_Canteen_Owner_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Canteen_Owner_Addresses_Addresses] FOREIGN KEY([address_id])
REFERENCES [dbo].[Addresses] ([address_id])
GO
ALTER TABLE [dbo].[Property_Canteen_Owner_Addresses] NOCHECK CONSTRAINT [FK_Property_Canteen_Owner_Addresses_Addresses]
GO
ALTER TABLE [dbo].[Property_Canteen_Owner_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Canteen_Owner_Addresses_Property] FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Canteen_Owner_Addresses] NOCHECK CONSTRAINT [FK_Property_Canteen_Owner_Addresses_Property]
GO
ALTER TABLE [dbo].[Property_Details]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Details_Property] FOREIGN KEY([Property_ID])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Details] NOCHECK CONSTRAINT [FK_Property_Details_Property]
GO
ALTER TABLE [dbo].[Property_Floor_Rooms]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Floor_Rooms_Property] FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Floor_Rooms] NOCHECK CONSTRAINT [FK_Property_Floor_Rooms_Property]
GO
ALTER TABLE [dbo].[Property_Floor_Rooms]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Floor_Rooms_Property_Block_Floor] FOREIGN KEY([property_block_floor_id])
REFERENCES [dbo].[Property_Block_Floor] ([Property_block_floor_id])
GO
ALTER TABLE [dbo].[Property_Floor_Rooms] NOCHECK CONSTRAINT [FK_Property_Floor_Rooms_Property_Block_Floor]
GO
ALTER TABLE [dbo].[Property_Inventory]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Inventory_Inventory_Items] FOREIGN KEY([item_ID])
REFERENCES [dbo].[Inventory_Items] ([item_id])
GO
ALTER TABLE [dbo].[Property_Inventory] NOCHECK CONSTRAINT [FK_Property_Inventory_Inventory_Items]
GO
ALTER TABLE [dbo].[Property_Inventory]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Inventory_Property] FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Inventory] NOCHECK CONSTRAINT [FK_Property_Inventory_Property]
GO
ALTER TABLE [dbo].[Property_Onboard_Opex_Capex_FAR]  WITH CHECK ADD  CONSTRAINT [FK_Property_Onboard_Opex_Capex_FAR_financial_item_categories] FOREIGN KEY([financial_category_code])
REFERENCES [dbo].[Financial_Item_Categories] ([financial_category_code])
GO
ALTER TABLE [dbo].[Property_Onboard_Opex_Capex_FAR] CHECK CONSTRAINT [FK_Property_Onboard_Opex_Capex_FAR_financial_item_categories]
GO
ALTER TABLE [dbo].[Property_Owner]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Owner_Property] FOREIGN KEY([property_id])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Owner] NOCHECK CONSTRAINT [FK_Property_Owner_Property]
GO
ALTER TABLE [dbo].[Property_Owner_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Owner_Addresses_Address_Types] FOREIGN KEY([address_type_code])
REFERENCES [dbo].[Address_Types] ([address_type_code])
GO
ALTER TABLE [dbo].[Property_Owner_Addresses] NOCHECK CONSTRAINT [FK_Property_Owner_Addresses_Address_Types]
GO
ALTER TABLE [dbo].[Property_Owner_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Owner_Addresses_Addresses] FOREIGN KEY([address_id])
REFERENCES [dbo].[Addresses] ([address_id])
GO
ALTER TABLE [dbo].[Property_Owner_Addresses] NOCHECK CONSTRAINT [FK_Property_Owner_Addresses_Addresses]
GO
ALTER TABLE [dbo].[Property_Owner_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Owner_Addresses_Property_Owner] FOREIGN KEY([proerty_owner_id])
REFERENCES [dbo].[Property_Owner] ([property_owner_id])
GO
ALTER TABLE [dbo].[Property_Owner_Addresses] NOCHECK CONSTRAINT [FK_Property_Owner_Addresses_Property_Owner]
GO
ALTER TABLE [dbo].[Property_Owner_Bank_Details]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Owner_Bank_Details_Property_Owner] FOREIGN KEY([property_owner_id])
REFERENCES [dbo].[Property_Owner] ([property_owner_id])
GO
ALTER TABLE [dbo].[Property_Owner_Bank_Details] NOCHECK CONSTRAINT [FK_Property_Owner_Bank_Details_Property_Owner]
GO
ALTER TABLE [dbo].[Property_Owner_Companies]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Owner_Companies_Property_Owner] FOREIGN KEY([property_owner_id])
REFERENCES [dbo].[Property_Owner] ([property_owner_id])
GO
ALTER TABLE [dbo].[Property_Owner_Companies] NOCHECK CONSTRAINT [FK_Property_Owner_Companies_Property_Owner]
GO
ALTER TABLE [dbo].[Property_Photos]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Photos_Property] FOREIGN KEY([Property_ID])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Property_Photos] NOCHECK CONSTRAINT [FK_Property_Photos_Property]
GO
ALTER TABLE [dbo].[Property_Revenue_Sharing]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Revenue_Sharing_Property_Owner] FOREIGN KEY([property_owner_id])
REFERENCES [dbo].[Property_Owner] ([property_owner_id])
GO
ALTER TABLE [dbo].[Property_Revenue_Sharing] NOCHECK CONSTRAINT [FK_Property_Revenue_Sharing_Property_Owner]
GO
ALTER TABLE [dbo].[Property_Revenue_Sharing]  WITH NOCHECK ADD  CONSTRAINT [FK_Property_Revenue_Sharing_Property_Revenue_Sharing_Types] FOREIGN KEY([revenue_sharing_Type_ID])
REFERENCES [dbo].[Property_Revenue_Sharing_Types] ([revenue_sharing_type_id])
GO
ALTER TABLE [dbo].[Property_Revenue_Sharing] NOCHECK CONSTRAINT [FK_Property_Revenue_Sharing_Property_Revenue_Sharing_Types]
GO
ALTER TABLE [dbo].[Readiness_for_Procure_Checklist_Items]  WITH NOCHECK ADD  CONSTRAINT [FK_Readiness_for_Procure_Checklist_Items_Readiness_for_Procure_Categories] FOREIGN KEY([readiness_for_procure_category_code])
REFERENCES [dbo].[Readiness_for_Procure_Categories] ([readiness_for_procure_category_code])
GO
ALTER TABLE [dbo].[Readiness_for_Procure_Checklist_Items] NOCHECK CONSTRAINT [FK_Readiness_for_Procure_Checklist_Items_Readiness_for_Procure_Categories]
GO
ALTER TABLE [dbo].[Student_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Addresses_Address_Types] FOREIGN KEY([address_type_code])
REFERENCES [dbo].[Address_Types] ([address_type_code])
GO
ALTER TABLE [dbo].[Student_Addresses] NOCHECK CONSTRAINT [FK_Student_Addresses_Address_Types]
GO
ALTER TABLE [dbo].[Student_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Addresses_Addresses] FOREIGN KEY([address_id])
REFERENCES [dbo].[Addresses] ([address_id])
GO
ALTER TABLE [dbo].[Student_Addresses] NOCHECK CONSTRAINT [FK_Student_Addresses_Addresses]
GO
ALTER TABLE [dbo].[Student_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Addresses_Students] FOREIGN KEY([student_id])
REFERENCES [dbo].[Students] ([student_id])
GO
ALTER TABLE [dbo].[Student_Addresses] NOCHECK CONSTRAINT [FK_Student_Addresses_Students]
GO
ALTER TABLE [dbo].[Student_Education]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Education_Departments] FOREIGN KEY([department_id])
REFERENCES [dbo].[Departments] ([department_id])
GO
ALTER TABLE [dbo].[Student_Education] NOCHECK CONSTRAINT [FK_Student_Education_Departments]
GO
ALTER TABLE [dbo].[Student_Education]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Education_Students] FOREIGN KEY([student_id])
REFERENCES [dbo].[Students] ([student_id])
GO
ALTER TABLE [dbo].[Student_Education] NOCHECK CONSTRAINT [FK_Student_Education_Students]
GO
ALTER TABLE [dbo].[Student_Education]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Education_Students1] FOREIGN KEY([student_id])
REFERENCES [dbo].[Students] ([student_id])
GO
ALTER TABLE [dbo].[Student_Education] NOCHECK CONSTRAINT [FK_Student_Education_Students1]
GO
ALTER TABLE [dbo].[Student_Payment_Methods]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Payment_Methods_Payment_Methods] FOREIGN KEY([payment_method_code])
REFERENCES [dbo].[Payment_Methods] ([payment_method_code])
GO
ALTER TABLE [dbo].[Student_Payment_Methods] NOCHECK CONSTRAINT [FK_Student_Payment_Methods_Payment_Methods]
GO
ALTER TABLE [dbo].[Student_Payment_Methods]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Payment_Methods_Students] FOREIGN KEY([student_id])
REFERENCES [dbo].[Students] ([student_id])
GO
ALTER TABLE [dbo].[Student_Payment_Methods] NOCHECK CONSTRAINT [FK_Student_Payment_Methods_Students]
GO
ALTER TABLE [dbo].[Student_Relationships]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Relationships_Parents_and_Guardians] FOREIGN KEY([person_id])
REFERENCES [dbo].[Parents_and_Guardians] ([person_id])
GO
ALTER TABLE [dbo].[Student_Relationships] NOCHECK CONSTRAINT [FK_Student_Relationships_Parents_and_Guardians]
GO
ALTER TABLE [dbo].[Student_Relationships]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Relationships_Relationship_Types] FOREIGN KEY([relationship_type_code])
REFERENCES [dbo].[Relationship_Types] ([relationship_type_code])
GO
ALTER TABLE [dbo].[Student_Relationships] NOCHECK CONSTRAINT [FK_Student_Relationships_Relationship_Types]
GO
ALTER TABLE [dbo].[Student_Relationships]  WITH NOCHECK ADD  CONSTRAINT [FK_Student_Relationships_Students] FOREIGN KEY([student_id])
REFERENCES [dbo].[Students] ([student_id])
GO
ALTER TABLE [dbo].[Student_Relationships] NOCHECK CONSTRAINT [FK_Student_Relationships_Students]
GO
ALTER TABLE [dbo].[Supplier_Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_Addresses_Address_Types] FOREIGN KEY([address_type_code])
REFERENCES [dbo].[Address_Types] ([address_type_code])
GO
ALTER TABLE [dbo].[Supplier_Addresses] CHECK CONSTRAINT [FK_Supplier_Addresses_Address_Types]
GO
ALTER TABLE [dbo].[Supplier_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Supplier_Addresses_Addresses] FOREIGN KEY([address_id])
REFERENCES [dbo].[Addresses] ([address_id])
GO
ALTER TABLE [dbo].[Supplier_Addresses] NOCHECK CONSTRAINT [FK_Supplier_Addresses_Addresses]
GO
ALTER TABLE [dbo].[Supplier_Addresses]  WITH NOCHECK ADD  CONSTRAINT [FK_Supplier_Addresses_Suppliers] FOREIGN KEY([supplier_code])
REFERENCES [dbo].[Suppliers] ([supplier_code])
GO
ALTER TABLE [dbo].[Supplier_Addresses] NOCHECK CONSTRAINT [FK_Supplier_Addresses_Suppliers]
GO
/****** Object:  StoredProcedure [dbo].[AQ_ACQUISITION_LOOKUPS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AQ_ACQUISITION_LOOKUPS]
AS
BEGIN
	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM 
	[dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='PRTY_CATEGORIES'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Location_Score'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Room_Sizes'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Room_Quality_Score'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Room_Cleanliness_Score'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Water_Source'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Water_Storage_Capacity'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Backup_Power'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Common_Area_Cleanliness'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Rent_Deposit_Types'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Gender'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Security_Features'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Fire_Fighting_Equipment_Types'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Food_Availability'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Property_Age'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Amenities'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='Food_Score' 
	--For property Readiness
	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='DetailsOfTie-upsAndBranding'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='PhotoShoot'

	SELECT D.Property_Lookup_Detail_id Category_ID,D.property_lookup_options Category_Name 
	FROM [dbo].Property_Lookups M INNER JOIN [dbo].Property_Lookup_Details D ON M.property_lookup_id=D.property_lookup_id 
	WHERE M.property_lookup_value='BTL_Activities'    
	
	SELECT  DISTINCT  
		Property_Name,
		Property_ID,
		longitute, 
		latitute, 
		'AQ' property_type  
	FROM dbo.AQ_Property
    UNION
    SELECT DISTINCT  
		Property_Name,
		P.Property_ID, 
		longitute, 
		latitute, 
		'PL' property_type 
	FROM dbo.Property P INNER JOIN dbo.Property_Details PD
		ON P.Property_ID = PD.Property_ID
	UNION
	SELECT DISTINCT 
		c_Property_Name Property_Name,
		c_Property_ID Property_ID,
		c_longitute longitute, 
		c_latitute latitute, 
		'CM' property_type  
	FROM   dbo.C_Property
           

END


GO
/****** Object:  StoredProcedure [dbo].[AQ_GET_PROPERTIES_FOR_CHECKLIST]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AQ_GET_PROPERTIES_FOR_CHECKLIST]
AS
BEGIN
	SELECT 
	Property_ID,
	Property_Name				
	FROM [dbo].[AQ_Property]  WHERE IsCheckListDone=0	
END

-- [dbo].[AQ_GET_PROPERTYS_FOR_CHECKLIST]

GO
/****** Object:  StoredProcedure [dbo].[AQ_GET_PROPERTY_DETAILS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AQ_GET_PROPERTY_DETAILS]
AS
BEGIN
	SELECT DISTINCT 
		P.Property_ID,
		P.Property_Name,
		P.Property_Address,
		P.No_of_Floors,
		P.Floor_Beds,
		P.Room_Beds,
		P.latitute,
		P.longitute,
		P.Available_Placio,
		P.Is_Exisiting_Hostel,
		P.IsCanteenExists,
		POD.Owner_Name,
		POD.Phone_Number OwnerPhoneNo
	FROM dbo.AQ_PROPERTY P
		INNER JOIN dbo.AQ_Property_Owner_Details POD 
			ON P.Property_ID=POD.Property_ID
		LEFT JOIN dbo.AQ_Property_Diesel_Generator PDG 
			ON PDG.Property_ID=P.Property_ID
		LEFT JOIN dbo.AQ_Property_Lift_Inputs PLI 
			ON PLI.Property_ID=P.Property_ID
		LEFT JOIN dbo.AQ_Property_Security_Features PSF 
			ON PSF.Property_ID=P.Property_ID

END

-- dbo.AQ_GET_PROPERTY_DETAILS

GO
/****** Object:  StoredProcedure [dbo].[AQ_INSERT_PROPERTY_DETAILS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AQ_INSERT_PROPERTY_DETAILS]
@Property_Name						VARCHAR(200),
@Property_Address					VARCHAR(2000),
@City					            VARCHAR(100) = NULL,
@State					            VARCHAR(100) = NULL,
@Pincode				            VARCHAR(100) = NULL,
@No_of_Floors						SMALLINT,
@Location_Score						SMALLINT,
@Comment_on_Location_Score			VARCHAR(200),
@Floor_Beds							SMALLINT,
@Room_Beds							SMALLINT,
@Room_Size							SMALLINT,
@Room_Quality_Score_ID				SMALLINT,
@Emergency_Staircase				BIT	= 0,
@Room_Cleanliness_Score_ID			SMALLINT,
@Water_Source_ID					SMALLINT,
@Water_Storage_Capacity_ID			SMALLINT,
@Common_Area_Cleanliness_ID			SMALLINT,
@Parking_Availability				BIT,
@Parking_Vehicle_Count				SMALLINT,
@House_Keeping_staff_Rooms			SMALLINT,
@Age_of_the_Property				SMALLINT,
----@Owner_Detail_ID				SMALLINT,
@longitute							NVARCHAR(100),
@latitute							NVARCHAR(100),
@IsNewProperty						BIT,
@IsCompetatorProperty				BIT,
-- [dbo].[AQ_Property_Diesel_Generator] Related Properties
@DG_Age								SMALLINT,
@DG_Brand							VARCHAR(20),
@DG_Warranty						VARCHAR(100),
@DG_AMC_Cost						DECIMAL(15,2),
@DG_storage_capacity				VARCHAR(20),
--Lift_ID int,
--Property_ID int,
@LIFT_Age							SMALLINT,
@LIFT_Brand							VARCHAR(20),
@LIFT_Warranty						VARCHAR(100),
@LIFT_AMC_Cost						DECIMAL(15,2),
-- Property Owner Details 
@Owner_Name							VARCHAR(100),
@Phone_Number						VARCHAR(20),
@Is_owner_Operated					BIT,
-- Photos at various phases
@Property_Photos					VARCHAR(MAX)=NULL,
@Diesel_Generator_Photos			VARCHAR(MAX)=NULL,
@Lift_Photos						VARCHAR(MAX)=NULL,
@Room_Photos						VARCHAR(MAX)=NULL,
@House_Keeping_staff_Rooms_Photos	VARCHAR(MAX)=NULL,
@Area_Photos      		     		VARCHAR(MAX)=NULL,  --- NEWLY ADDED
@Floor_Rooms_Beds					VARCHAR(MAX)=NULL,
@Property_Amenities_IDs				VARCHAR(MAX)=NULL,
/* PART B  */
@Property_Alias_Name                VARCHAR(200),
@B_Rent_Deposit_ID					SMALLINT = NULL,  
@B_No_of_Beds						SMALLINT = NULL,  
@B_Gender							VARCHAR(20) = NULL, 
@B_No_of_Floors						SMALLINT = NULL, 
@B_Property_Occupancy_Estimate		VARCHAR(20) = NULL,  
@B_Room_wise_Power_meter			BIT = 0,  
@B_Room_Cleanliness_Score_ID		SMALLINT = NULL,  
@B_IsReception_AreaExists			BIT = 0,  
@B_wifi_Reception					SMALLINT = NULL,  
@B_Security_Features				VARCHAR(200)= NULL,  
@B_Fire_Fighting_Equipment			VARCHAR(200) = NULL, 
@B_ISP_Provider_Name				VARCHAR(200) = NULL,
@Is_Exisiting_Hostel                BIT = 0,
-- PART C
@B_IsCanteenExists					BIT = 0,   
@B_Canteen_Cleanliness_ID			SMALLINT = NULL, 
@B_Food_Availability_ID				SMALLINT = NULL,  
@B_Food_Score                       SMALLINT = NULL,
@Property_Canteen_Photos			VARCHAR(MAX)=NULL,
-- PART D
@Operator_Number                    VARCHAR(100)=NULL,
@Available_Placio                   BIT = 0
AS 
DECLARE 
@Property_ID						INT,
@Lift_ID							SMALLINT,
@Diesel_Generator_ID				SMALLINT,
@Owner_Detail_ID					INT

BEGIN 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property Details -  Insertion */
	BEGIN 
		SELECT  @Property_ID = NEXT VALUE FOR [dbo].[AQ_Property_ID]
		INSERT INTO [dbo].[AQ_PROPERTY]
				   ([Property_ID]
				   ,[Property_Name]
				   ,[Property_Address]
				   ,[City]
				   ,[State]
				   ,[Pincode]
				   ,[No_of_Floors]
				   ,[Location_Score]
				   ,[Comment_on_Location_Score]
				   ,[Floor_Beds]
				   ,[Room_Beds]
				   ,[Room_Size]
				   ,[Room_Quality_Score_ID]
				   ,[Room_Cleanliness_Score_ID]
				   ,[Water_Source_ID]
				   ,[Water_Storage_Capacity_ID]
				   ,[Emergency_Staircase]
				   ,[Common_Area_Cleanliness_ID]
				   ,[Parking_Availability]
				   ,[Parking_Vehicle_Count]
				   ,[House_Keeping_staff_Rooms]
				   ,[Age_of_the_Property]
				   ,[longitute]
				   ,[latitute]
				   ,[IsCompetatorProperty]
				   ,[Gender]
				   ,[Rent_Deposit_ID]
				   ,[No_of_Beds]
				   ,[Property_Occupancy_Estimate]
				   ,[Room_wise_Power_Meter_ID]
				   ,[Room_Cleanliness_ID]
				   ,[Reception_Area_yn]
				   ,[Wifi_Reception]
				   ,[Fire_Fighting_Equipment_IDs]
				   ,[ISP_Provider_Name]
				   ,Is_Exisiting_Hostel
				   ,IsCanteenExists
				   ,Canteen_Cleanliness_ID
				   ,Food_Availability_ID			   
				   ,Food_Score
				   ,Operator_Number
				   ,Available_Placio
				   ,Property_Alias_Name
				   ,Property_Amenities_ID
				   )
		SELECT		
					@Property_ID,
					@Property_Name,
					@Property_Address,
					@City,
					@State,
					@Pincode,
					@No_of_Floors,
					@Location_Score,
					@Comment_on_Location_Score,
					@Floor_Beds,
					@Room_Beds,
					@Room_Size,
					@Room_Quality_Score_ID,
					@Room_Cleanliness_Score_ID,
					@Water_Source_ID,
					@Water_Storage_Capacity_ID,
					@Emergency_Staircase,
					@Common_Area_Cleanliness_ID,
					@Parking_Availability,
					@Parking_Vehicle_Count,
					@House_Keeping_staff_Rooms,
					@Age_of_the_Property,
					--@Owner_Detail_ID,
					@longitute,
					@latitute,
					@IsCompetatorProperty,
			/* PART B */
					@B_Gender,
					@B_Rent_Deposit_ID,
					@B_No_of_Beds,
					@B_Property_Occupancy_Estimate,
					@B_Room_wise_Power_meter,
					@B_Room_Cleanliness_Score_ID,
					@B_IsReception_AreaExists,
					@B_wifi_Reception,
					--@B_Security_Features,
					@B_Fire_Fighting_Equipment,
					@B_ISP_Provider_Name,
					@Is_Exisiting_Hostel,
			/* PART C */
					@B_IsCanteenExists,
					@B_Canteen_Cleanliness_ID,
					@B_Food_Availability_ID,
					@B_Food_Score,
			/* PART D */
					@Operator_Number,
					@Available_Placio,
					@Property_Alias_Name,
					@Property_Amenities_IDs
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property Owner Details -  Insertion */
	BEGIN 
		SELECT @Owner_Detail_ID = NEXT VALUE FOR [dbo].[AQ_Owner_Detail_ID]
		INSERT INTO [dbo].[AQ_Property_Owner_Details]
					   (Owner_Detail_ID
					   ,[Property_ID]
					   ,[Owner_Name]
					   ,[Phone_Number]
					   ,[Is_owner_Operated])
			SELECT		@Owner_Detail_ID,
						@Property_ID,
						@Owner_Name,
						@Phone_Number,
						@Is_owner_Operated
		UPDATE [dbo].[AQ_PROPERTY] SET [Owner_Detail_ID] = @Owner_Detail_ID
				WHERE [Property_ID] = @Property_ID
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property Photo Details -  Insertion */
	BEGIN 
		DECLARE @l_Property_Photos TABLE ( ID INT IDENTITY(1,1), Property_Photo NVARCHAR(MAX))
		INSERT INTO @l_Property_Photos SELECT * FROM DBO.USP_SplitString(@Property_Photos,';')
		IF EXISTS(SELECT * FROM @l_Property_Photos)
			BEGIN 
				;WITH Split_Property_Photos (ID, Name, xmlname) AS
				( SELECT ID,Property_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Property_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Property_Photos )

				INSERT INTO [dbo].[AQ_Property_Photos]
					   ([Property_ID]
					   ,[PhotoLocation]
					   ,[Seq_ID])
				 SELECT      
					 @Property_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Property_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Property_Photos
			END 
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property Diesel Generator Details -  Insertion */
	BEGIN 
		SELECT @Diesel_Generator_ID =  NEXT VALUE FOR [dbo].[AQ_Diesel_Generator_ID]
		INSERT INTO [dbo].[AQ_Property_Diesel_Generator]
				   ([Diesel_Generator_ID]
				   ,[Property_ID]
				   ,[Age]
				   ,[Brand]
				   ,[Warranty]
				   ,[AMC_Cost]
				   ,[storage_capacity]
				   )
		SELECT 
					@Diesel_Generator_ID,
					@Property_ID,
					@DG_Age,
					@DG_Brand,
					@DG_Warranty,
					@DG_AMC_Cost,
					@DG_storage_capacity
		UPDATE [dbo].[AQ_PROPERTY] SET [Diesel_Generator_ID] = @Diesel_Generator_ID
				WHERE [Property_ID] = @Property_ID
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property Diesel Generator Photo Insertion */
	BEGIN 
		DECLARE @l_Diesel_Generator TABLE ( ID INT IDENTITY(1,1), Diesel_Generator_Photo NVARCHAR(MAX))
		INSERT INTO @l_Diesel_Generator SELECT * FROM DBO.USP_SplitString(@Diesel_Generator_Photos,';')
		IF EXISTS(SELECT * FROM @l_Diesel_Generator)
			BEGIN 
				;WITH Split_Diesel_Generator_Photos (ID, Name, xmlname) AS
				( SELECT ID,Diesel_Generator_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Diesel_Generator_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Diesel_Generator )

				INSERT INTO [dbo].[AQ_Property_Diesel_Generator_Photos]
						   ([Property_ID]
						   ,[Diesel_Generator_ID]
						   ,[PhotoLocation]
						   ,[Seq_ID])
				 SELECT      
					 @Property_ID,
					 @Diesel_Generator_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Diesel_Generator_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Diesel_Generator_Photos
			END 
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property Lift Details Insertion */
	BEGIN 
		SELECT @Lift_ID =  NEXT VALUE FOR [dbo].[AQ_Lift_ID]
		INSERT INTO [dbo].[AQ_Property_Lift_Inputs]
				   ([Lift_ID]
				   ,[Property_ID]
				   ,[Age]
				   ,[Brand]
				   ,[Warranty]
				   ,[AMC_Cost])
		SELECT 
					@Lift_ID,
					@Property_ID,
					@LIFT_Age,
					@LIFT_Brand,
					@LIFT_Warranty,
					@LIFT_AMC_Cost
		UPDATE [dbo].[AQ_PROPERTY] SET [Lift_ID] = @Lift_ID
				WHERE [Property_ID] = @Property_ID
	END
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property Lift Photo Insertion */
	BEGIN 
		DECLARE @l_Lift_Photos TABLE ( ID INT IDENTITY(1,1), Lift_Photo NVARCHAR(MAX))
		INSERT INTO @l_Lift_Photos SELECT * FROM DBO.USP_SplitString(@Lift_Photos,';')
		IF EXISTS(SELECT * FROM @l_Lift_Photos)
			BEGIN 
				;WITH Split_Lift_Photos (ID, Name, xmlname) AS
				( SELECT ID,Lift_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Lift_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Lift_Photos )

				INSERT INTO [dbo].[AQ_Property_Lift_Photos]
						   ([Property_ID]
						   ,[Lift_ID]
						   ,[PhotoLocation]
						   ,[Seq_ID])
				 SELECT      
					 @Property_ID,
					 @Lift_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Lift_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Lift_Photos
			 END
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property Room Photo Insertion */
	BEGIN 
		DECLARE @l_Room_Photos TABLE ( ID INT IDENTITY(1,1), Room_Photo NVARCHAR(MAX))
		INSERT INTO @l_Room_Photos SELECT * FROM DBO.USP_SplitString(@Room_Photos,';')
		IF EXISTS(SELECT * FROM @l_Room_Photos)
			BEGIN 
				;WITH Split_Room_Photos (ID, Name, xmlname) AS
				( SELECT ID,Room_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Room_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Room_Photos )

				INSERT INTO [dbo].[AQ_Property_Room_Photos]
						   ([Property_ID]
						   ,[PhotoLocation]
						   ,[Seq_ID])
				 SELECT      
					 @Property_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Room_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Room_Photos
			 END 
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property Property_Common_Area_Cleanliness_Photos  Insertion */
	BEGIN 
		DECLARE @1_Area_Photos TABLE ( ID INT IDENTITY(1,1), Area_Photo NVARCHAR(MAX))
		INSERT INTO @1_Area_Photos SELECT * FROM DBO.USP_SplitString(@Area_Photos,';')
		IF EXISTS(SELECT * FROM @1_Area_Photos)
			BEGIN 
				;WITH Split_Area_Photos (ID, Name, xmlname) AS
				( SELECT ID,Area_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Area_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @1_Area_Photos)

				INSERT INTO [dbo].[AQ_Property_Common_Area_Cleanliness_Photos]
						   ([Property_ID]
						   ,[PhotoLocation]
						   ,[Seq_ID])
				 SELECT      
					 @Property_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Area_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Area_Photos
			 END 
	END
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property Property_House_Keeping_staff_Rooms_Photos  Insertion */
	BEGIN 
		DECLARE @l_House_Keeping_staff_Rooms_Photos TABLE ( ID INT IDENTITY(1,1), HKS_Room_Photo NVARCHAR(MAX))
		INSERT INTO @l_House_Keeping_staff_Rooms_Photos SELECT * FROM DBO.USP_SplitString(@House_Keeping_staff_Rooms_Photos,';')
		IF EXISTS(SELECT * FROM @l_House_Keeping_staff_Rooms_Photos)
			BEGIN 
				;WITH Split_HKS_Room_Photo (ID, Name, xmlname) AS
				( SELECT ID,HKS_Room_Photo, CONVERT(XML,'<Names><name>' + REPLACE(HKS_Room_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_House_Keeping_staff_Rooms_Photos)

				INSERT INTO [dbo].[AQ_Property_House_Keeping_staff_Rooms_Photos]
						   ([Property_ID]
						   ,[PhotoLocation]
						   ,[Seq_ID])
				 SELECT      
					 @Property_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Split_HKS_Room_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_HKS_Room_Photo
			 END 
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/*  PART - B - Property Security Features - Multi Select */
	BEGIN 
		DECLARE @l_Property_Security_Features TABLE ( ID INT IDENTITY(1,1), Security_Features NVARCHAR(MAX))
		INSERT INTO @l_Property_Security_Features SELECT * FROM DBO.USP_SplitString(@B_Security_Features,';')
		;WITH Split_Property_Security_Features (ID, Name, xmlname) AS
		( SELECT ID,Security_Features, CONVERT(XML,'<Names><name>' + REPLACE(Security_Features,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Property_Security_Features )
		INSERT INTO [dbo].[AQ_Property_Security_Features]
					([Property_ID]
					,[Security_Feature_ID])
			SELECT      
				@Property_ID,
				xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Property_Security_Feature_ID
			FROM Split_Property_Security_Features
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* PART - C If Canteen/ Mess is available Part C  */
	/* Property Property_Canteen_Photos  Insertion */
	BEGIN 
		DECLARE @l_Property_Canteen_Photos TABLE ( ID INT IDENTITY(1,1), Canteen_Photos NVARCHAR(MAX))
		INSERT INTO @l_Property_Canteen_Photos SELECT * FROM DBO.USP_SplitString(@Property_Canteen_Photos,';')
		;WITH Split_Canteen_Photos (ID, Name, xmlname) AS
		( SELECT ID,Canteen_Photos, CONVERT(XML,'<Names><name>' + REPLACE(Canteen_Photos,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Property_Canteen_Photos)
		INSERT INTO [dbo].[AQ_Property_Canteen_Photos]
					([Property_ID]
					,[PhotoLocation]
					,[Seq_ID])
			SELECT      
				@Property_ID,
				xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Canteen_Photo_Location, 
				ID AS SEQ_ID
			FROM Split_Canteen_Photos
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property Property_Floor_Rooms_Beds  Insertion */
	BEGIN 
		DECLARE @property_block_id INT = NEXT VALUE FOR [dbo].[AQ_Property_Block_ID] 
		DECLARE @l_Floor_Rooms_Beds TABLE ( ID INT IDENTITY(1,1), Property_Floor_Rooms_Beds NVARCHAR(MAX))
		
		DECLARE @Property_Floor_Rooms_Beds_Data TABLE	(	
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[Property_block_floor_id] INT NULL,
				[property_block_id] INT NULL,
				--[floor_id] INT NULL,
				[floor_name] INT NULL,
				[no_of_rooms] INT NULL,
				[no_of_beds] INT NULL)

		INSERT INTO [dbo].[AQ_Property_Block]
           ([property_block_id]
           ,[property_id]
           ,[block_name]
           ,[no_of_rooms]
		   )

		SELECT @property_block_id
			  ,@Property_ID
			  ,'Block-1'			-- Temporary Name
			  ,50					-- Temporary value
			

		INSERT INTO @l_Floor_Rooms_Beds SELECT * FROM DBO.USP_SplitString(@Floor_Rooms_Beds,';')
		;WITH Split_Property_Floor_Rooms_Beds (ID, Name, xmlname) AS
		( SELECT ID,Property_Floor_Rooms_Beds, 
				CONVERT(XML,'<Names><name>' + REPLACE(Property_Floor_Rooms_Beds,',', '</name><name>') + '</name></Names>') AS xmlname 
				FROM @l_Floor_Rooms_Beds
		)
		INSERT INTO @Property_Floor_Rooms_Beds_Data
		(
			[Property_block_floor_id] ,
			[property_block_id] ,
			--[floor_id] INT NULL,
			[floor_name] ,
			[no_of_rooms],
			[no_of_beds]
		)
		SELECT      
			NEXT VALUE FOR [dbo].[AQ_Property_Block_Floor_ID],
			@property_block_id,
			--@Property_ID,
			xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Floor_Name,
			xmlname.value('/Names[1]/name[2]','NVARCHAR(10)') AS [no_of_rooms],
			xmlname.value('/Names[1]/name[3]','NVARCHAR(10)') AS [no_of_beds] 
		FROM Split_Property_Floor_Rooms_Beds

		DECLARE @FloorTotalCnt INT 
		DECLARE @FloorCnt INT

		SELECT @FloorTotalCnt = COUNT(1) FROM @Property_Floor_Rooms_Beds_Data
		SET @FloorCnt = 1

		WHILE @FloorCnt <= @FloorTotalCnt
			BEGIN
				INSERT INTO [dbo].[AQ_Property_Block_Floor]
					([Property_block_floor_id]
					,[property_id]
					,[property_block_id]
					--,[floor_id]
					,[floor_name]
					,[no_of_rooms])

				SELECT   PD.[Property_block_floor_id]
						,@Property_ID
						,PD.[property_block_id]
						,PD.[floor_name]
						,PD.[no_of_rooms]
				FROM @Property_Floor_Rooms_Beds_Data PD WHERE ID = @FloorCnt

				DECLARE @TotalRoomCnt INT
				DECLARE @RoomCnt INT
				SET @RoomCnt = 1
				SELECT  @TotalRoomCnt = PD.[no_of_rooms]  FROM @Property_Floor_Rooms_Beds_Data PD WHERE ID = @FloorCnt
					WHILE @RoomCnt <= @TotalRoomCnt
					BEGIN
						DECLARE @AQ_Property_Floor_Room_ID INT = NEXT VALUE FOR [dbo].[AQ_Property_Floor_Room_ID]
						INSERT INTO [dbo].[AQ_Property_Floor_Rooms]
						   ([property_floor_room_id]
						   ,[property_id]
						   ,[property_block_id]
						   ,[property_block_floor_id]
						   ,[room_no]
						   ,[room_name]
						   ,[no_of_beds]
						   )
						SELECT 
							 @AQ_Property_Floor_Room_ID
							,@Property_ID
							,PD.property_block_id
							,PD.Property_block_floor_id
							,'BLOCK-1-'+ 'FLOOR-' + 'ROOM-' + CAST(@AQ_Property_Floor_Room_ID AS VARCHAR)
							, 'ROOM NAME - ' +  cast(@AQ_Property_Floor_Room_ID as varchar)
							,3 [no_of_beds] -- Static Value
						FROM @Property_Floor_Rooms_Beds_Data PD WHERE ID = @FloorCnt

						DECLARE @TotalBedCnt INT
						DECLARE @BedCnt INT
						DECLARE @Room_no nvarchar(100)
						SET @BedCnt = 1
						SELECT  @TotalBedCnt = [no_of_beds], @Room_no = room_no  FROM [dbo].[AQ_Property_Floor_Rooms] PD 
								WHERE PD.[property_floor_room_id] = @AQ_Property_Floor_Room_ID
							WHILE @BedCnt <= @TotalBedCnt
							BEGIN
								INSERT INTO [dbo].[AQ_Property_Floor_Room_Bed]
									   ([bed_id]
									   ,[property_id]
									   ,[block_id]
									   ,[floor_id]
									   ,[room_no]
									   ,[bed_no]
									   ,[price]
									   ,[status])
								SELECT 
									 NEXT VALUE FOR [dbo].[AQ_Bed_id]  --@AQ_Property_Floor_Room_ID
									,@Property_ID
									,PD.property_block_id
									,PD.Property_block_floor_id
									,PD.[room_no]
									,'BLOCK-1-'+'FLOOR-'+ 'ROOM-'+ CAST(PD.Property_block_floor_id AS VARCHAR)+'BED-'+ CAST(@BedCnt AS VARCHAR)
									,100.00
									,0 -- Static Value
								FROM [dbo].[AQ_Property_Floor_Rooms] PD WHERE [room_no] = @Room_no
							SET @BedCnt = @BedCnt +1
							END
						SET @RoomCnt = @RoomCnt +1
				END 
			SET @FloorCnt = @FloorCnt +1		
			END
	END 

--@Floor_Rooms_Beds
	/* This routine used to insert the records for Floor - Rooms - Beds*/
		--INSERT INTO [dbo].[AQ_Property_Floor_Room_Bed]
		--		   ([bed_id]
		--		   ,[property_id]
		--		   ,[block_id]
		--		   ,[floor_id]
		--		   ,[room_id]
		--		   ,[bed_no]
		--		   ,[price]
		--		   ,[status])
END 




GO
/****** Object:  StoredProcedure [dbo].[AQ_INSERT_PROPERTY_READINESSCHECKLIST]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AQ_INSERT_PROPERTY_READINESSCHECKLIST]
(
            @Property_ID						INT=NULL
           ,@VisibilityonGoogle					BIT=NULL
           ,@CCTVInstallation					BIT=NULL
           ,@HouseKeepingStaff					BIT=NULL
           ,@Security							BIT=NULL
           ,@Foodpatner							BIT=NULL
           ,@ServiceProviders					BIT=NULL
           ,@BiometricInstallation				BIT=NULL
           ,@receptionistInduction				BIT=NULL
           ,@DetailsOfTieupAndBranding			VARCHAR(20)=NULL
           ,@Internet							BIT=NULL
           ,@wifi								BIT=NULL
           ,@DTH								BIT=NULL
           ,@WaterCooler						BIT=NULL
           ,@WaterPurificationPlant				BIT=NULL
           ,@DrinkingWaterSupply				BIT=NULL
           ,@BTLActivities						VARCHAR(20)=NULL
           ,@PhotoShoot							VARCHAR(20)=NULL
           ,@KioskBranding						BIT=NULL
           ,@Landscaping						BIT=NULL
           ,@Comments							VARCHAR(1000)=NULL
		   ,@UserId                             VARCHAR(20)=NULL
)
AS
BEGIN
INSERT INTO [dbo].[AQ_Property_ReadinessChecklist]
          (
		    [Property_ID]
           ,[VisibilityonGoogle]
           ,[CCTVInstallation]
           ,[HouseKeepingStaff]
           ,[Security]
           ,[Foodpatner]
           ,[ServiceProviders]
           ,[BiometricInstallation]
           ,[receptionistInduction]
           ,[DetailsOfTieupAndBranding]
           ,[Internet]
           ,[wifi]
           ,[DTH]
           ,[WaterCooler]
           ,[WaterPurificationPlant]
           ,[DrinkingWaterSupply]
           ,[BTLActivities]
           ,[PhotoShoot]
           ,[KioskBranding]
           ,[Landscaping]
           ,[Comments]
		   ,[UserId]
		   ,[Createddate]
		  )
     VALUES
          (
		    @Property_ID                 
           ,@VisibilityonGoogle         
           ,@CCTVInstallation           
           ,@HouseKeepingStaff          
           ,@Security                   
           ,@Foodpatner                 
           ,@ServiceProviders           
           ,@BiometricInstallation      
           ,@receptionistInduction      
           ,@DetailsOfTieupAndBranding  
           ,@Internet                   
           ,@wifi						
           ,@DTH                        
           ,@WaterCooler                
           ,@WaterPurificationPlant     
           ,@DrinkingWaterSupply        
           ,@BTLActivities				
           ,@PhotoShoot					
           ,@KioskBranding				
           ,@Landscaping				
           ,@Comments
		   ,@UserId
		   ,GETDATE()		   					
		  )
		  UPDATE dbo.AQ_Property SET IsCheckListDone=1,CheckListApprovedDate=GETDATE() WHERE Property_ID=@Property_ID							    
END

GO
/****** Object:  StoredProcedure [dbo].[GET_C_PROPERTY_DETAILS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GET_C_PROPERTY_DETAILS]
AS
BEGIN
SELECT  CP.C_Property_ID,
		C_Property_Name,
		C_Property_Address,
		PL.property_lookup_options Gender,
		C_No_of_Floors,
		C_No_of_Beds,
		C_latitute,
		C_longitute,
		C_IsCanteenExists,
		C_Available_Placio,
		POD.C_Owner_Name,
		POD.C_Phone_Number
FROM dbo.C_Property CP 
	INNER JOIN [dbo].Property_Lookup_Details PL 
		ON PL.Property_Lookup_Detail_id=CP.C_Gender
	INNER JOIN dbo.C_Property_Owner_Details POD 
		ON POD.C_Owner_Detail_ID=CP.C_Owner_Detail_ID
END

GO
/****** Object:  StoredProcedure [dbo].[INSERT_C_PROPERTY_DETAILS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERT_C_PROPERTY_DETAILS]
@Property_Name						VARCHAR(200),
--@Property_Alias_Name                VARCHAR(200),
@Property_Address					VARCHAR(2000),
@longitute							NVARCHAR(100),
@latitute							NVARCHAR(100),
@Gender								VARCHAR(20) = NULL,
@Start_of_Operataion				DATE = NULL, 
@Rent_Deposit_ID					SMALLINT = NULL,  
@Property_Occupancy_Estimate		VARCHAR(20) = NULL,  
@Property_Amenities_IDs				VARCHAR(MAX)=NULL,  
@Property_Category_ID				SMALLINT = NULL,
@Location_Score						SMALLINT = NULL,
@Comment_on_Location_Score			VARCHAR(200) = NULL,
@No_of_Floors						SMALLINT,
@No_of_Rooms						SMALLINT = NULL, 
@No_of_Beds							SMALLINT = NULL,  
@Room_Quality_Score_ID				SMALLINT = NULL,  
@Room_Cleanliness_Score_ID			SMALLINT = NULL,
@Property_Lift_Inputs_yn			BIT	= 0,
@Property_Backup_Power_yn			SMALLINT = NULL,
@Property_Diesel_Generator_yn		BIT	= 0,
@Emergency_Staircase				BIT	= 0,
@Common_Area_Cleanliness_ID			SMALLINT = NULL,
@Age_of_the_Property				SMALLINT = NULL,
@IsReception_Area_yn				BIT = 0,  
@Fire_Fighting_Equipment			VARCHAR(200) = NULL, 
@IsNewProperty						BIT,
@Owner_Name							VARCHAR(100),
@Phone_Number						VARCHAR(20),
@Is_owner_Operated					BIT,
-- PART C
@IsCanteenExists					BIT = 0,   
@Canteen_Cleanliness_ID				SMALLINT = NULL, 
@Food_Availability_ID				SMALLINT = NULL,  
@Food_Score							SMALLINT = NULL,
-- PART D
--@Operated_by						SMALLINT = NULL,
@Operator_Number                    VARCHAR(100)=NULL,
@Available_Placio                   BIT = 0,
-- PHOTOS
@Property_Photos					VARCHAR(MAX)=NULL,
@Diesel_Generator_Photos			VARCHAR(MAX)=NULL,
@Lift_Photos						VARCHAR(MAX)=NULL,
@Area_Photos      		     		VARCHAR(MAX)=NULL, --- NEWLY ADDED
@Property_Canteen_Photos			VARCHAR(MAX)=NULL,
@Room_Photos						VARCHAR(MAX)=NULL,
@Main_Property_ID					SMALLINT = NULL
AS 
DECLARE 
@Property_ID						INT = NULL,
@Lift_ID							SMALLINT = NULL,
@Diesel_Generator_ID				SMALLINT = NULL,
@Owner_Detail_ID					INT = NULL
BEGIN 
	/* Property Details - Insertion */
	BEGIN 
		SELECT  @Property_ID = NEXT VALUE FOR [dbo].[Property_ID]
		INSERT INTO [dbo].[C_Property]
				([C_Property_ID]
				,[C_Property_Name]
				--,[C_Property_Alias_Name]
				,[C_Property_Address]
				,[C_Gender]
				,[C_Start_of_Operation]
				,[C_Rent_Deposit_ID]
				,[C_Property_Occupancy_Estimate]
				,[C_Location_Score]
				,[C_longitute]
				,[C_latitute]
				,[C_Property_Category_ID]
				,[C_Comment_on_Location_Score]
				,[C_No_of_Floors]
				,[C_No_of_Beds]
				,[C_Floor_Beds]
				,[C_Room_Quality_Score_ID]
				,[C_Room_Cleanliness_Score_ID]
				,[C_Backup_Power_ID]
				,[C_Emergency_Staircase]
				,[C_Common_Area_Cleanliness_ID]
				,[C_Age_of_the_Property]
				--,[C_Owner_Detail_ID]
				,[C_Reception_Area_yn]
				,[C_Fire_Fighting_Equipment_IDs]
				,[C_IsCanteenExists]
				,[C_Canteen_Cleanliness_ID]
				,[C_Food_Availability_ID]
				,[C_Food_Score]
				,[C_Operator_By_ID]
				,[C_Operator_Number]
				,[C_Available_Placio]
				,[IsCompetatorProperty]
				,[C_Main_Property_ID])

		SELECT		
				@Property_ID,
				@Property_Name,
				--@Property_Alias_Name,
				@Property_Address,
				@Gender,
				@Start_of_Operataion,
				@Rent_Deposit_ID,
				@Property_Occupancy_Estimate,
				@Location_Score,
				@longitute,
				@latitute,
				@Property_Category_ID,
				@Comment_on_Location_Score,
				@No_of_Floors,
				@No_of_Beds,
				@No_of_Beds,
				@Room_Quality_Score_ID,
				@Room_Cleanliness_Score_ID,
				@Property_Backup_Power_yn,
				@Emergency_Staircase,
				@Common_Area_Cleanliness_ID,
				@Age_of_the_Property,
				@IsReception_Area_yn,
				@Fire_Fighting_Equipment,
				@IsCanteenExists,
				@Canteen_Cleanliness_ID,
				@Food_Availability_ID,
				@Food_Score,
				@Is_owner_Operated,
				@Operator_Number,
				@Available_Placio,
				1, --@IsCompetatorProperty
				@Main_Property_ID

		/* Property Owner Details -  Insertion */
		SELECT @Owner_Detail_ID = NEXT VALUE FOR [dbo].[Owner_Detail_ID]
		INSERT INTO [dbo].[C_Property_Owner_Details]
						(C_Owner_Detail_ID
						,[C_Property_ID]
						,[C_Owner_Name]
						,[C_Phone_Number]
						,[C_Is_owner_Operated])
			SELECT		@Owner_Detail_ID,
						@Property_ID,
						@Owner_Name,
						@Phone_Number,
						@Is_owner_Operated
		UPDATE [dbo].[C_PROPERTY] SET [C_Owner_Detail_ID] = @Owner_Detail_ID
				WHERE [C_Property_ID] = @Property_ID
	END
	/* Property Photo Details - Insertion */
	BEGIN
		DECLARE @l_Property_Photos TABLE ( ID INT IDENTITY(1,1), Property_Photo NVARCHAR(MAX))
		INSERT INTO @l_Property_Photos SELECT * FROM DBO.USP_SplitString(@Property_Photos,';')
		IF EXISTS(SELECT * FROM @l_Property_Photos)
			BEGIN 
				;WITH Split_Property_Photos (ID, Name, xmlname) AS
				( SELECT ID,Property_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Property_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Property_Photos )

				INSERT INTO [dbo].[C_Property_Photos]
					   ([C_Property_ID]
					   ,[C_Photo_Location]
					   ,[C_Seq_ID])
				 SELECT      
					 @Property_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Property_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Property_Photos
			END 
	END
	/* Property Diesel Generator Photo Insertion */
	BEGIN 
		DECLARE @l_Diesel_Generator TABLE ( ID INT IDENTITY(1,1), Diesel_Generator_Photo NVARCHAR(MAX))
		INSERT INTO @l_Diesel_Generator SELECT * FROM DBO.USP_SplitString(@Diesel_Generator_Photos,';')
		IF EXISTS(SELECT * FROM @l_Diesel_Generator)
			BEGIN 
				;WITH Split_Diesel_Generator_Photos (ID, Name, xmlname) AS
				( SELECT ID,Diesel_Generator_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Diesel_Generator_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Diesel_Generator )

				INSERT INTO [dbo].[C_Property_Diesel_Generator_Photos]
						   ([C_Property_ID]
						--   ,[C_Diesel_Generator_ID]
						   ,[C_Photo_Location]
						   ,[C_Seq_ID])
				 SELECT      
					 @Property_ID,
					 --@Diesel_Generator_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Diesel_Generator_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Diesel_Generator_Photos
			END 
	END 
--	/* Property Lift Photo Insertion */
	BEGIN 
		DECLARE @l_Lift_Photos TABLE ( ID INT IDENTITY(1,1), Lift_Photo NVARCHAR(MAX))
		INSERT INTO @l_Lift_Photos SELECT * FROM DBO.USP_SplitString(@Lift_Photos,';')
		IF EXISTS(SELECT * FROM @l_Lift_Photos)
			BEGIN 
				;WITH Split_Lift_Photos (ID, Name, xmlname) AS
				( SELECT ID,Lift_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Lift_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Lift_Photos )

				INSERT INTO [dbo].[C_Property_Lift_Photos]
						   ([C_Property_ID]
	--					   ,[C_Lift_ID]
						   ,[C_Photo_Location]
						   ,[C_Seq_ID])
				 SELECT      
					 @Property_ID,
		--			 @Lift_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Lift_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Lift_Photos
			 END
	END 
	/* Property Property_Common_Area_Cleanliness_Photos Insertion */
	BEGIN 
		DECLARE @1_Area_Photos TABLE ( ID INT IDENTITY(1,1), Area_Photo NVARCHAR(MAX))
		INSERT INTO @1_Area_Photos SELECT * FROM DBO.USP_SplitString(@Area_Photos,';')
		IF EXISTS(SELECT * FROM @1_Area_Photos)
			BEGIN 
				;WITH Split_Area_Photos (ID, Name, xmlname) AS
				( SELECT ID,Area_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Area_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @1_Area_Photos)

				INSERT INTO [dbo].[C_Property_Common_Area_Cleanliness_Photos]
						   ([C_Property_ID]
						   ,[C_Photo_Location]
						   ,[C_Seq_ID])
				 SELECT      
					 @Property_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Area_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Area_Photos
			 END 
	END 
	/* Property Property_Canteen_Photos Insertion */
	BEGIN 
		DECLARE @l_Property_Canteen_Photos TABLE ( ID INT IDENTITY(1,1), Canteen_Photos NVARCHAR(MAX))
		INSERT INTO @l_Property_Canteen_Photos SELECT * FROM DBO.USP_SplitString(@Property_Canteen_Photos,';')

		IF EXISTS(SELECT * FROM @l_Property_Canteen_Photos)
			BEGIN 
				;WITH Split_Canteen_Photos (ID, Name, xmlname) AS
				( SELECT ID,Canteen_Photos, CONVERT(XML,'<Names><name>' + REPLACE(Canteen_Photos,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Property_Canteen_Photos)
				INSERT INTO [dbo].[C_Property_Canteen_Photos]
							([C_Property_ID]
							,[C_Photo_Location]
							,[C_Seq_ID])
					SELECT      
						@Property_ID,
						xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Canteen_Photo_Location, 
						ID AS SEQ_ID
					FROM Split_Canteen_Photos
			END 
	END 
	/* Property Room Photo Insertion */
	BEGIN
		DECLARE @l_Room_Photos TABLE ( ID INT IDENTITY(1,1), Room_Photo NVARCHAR(MAX))
		INSERT INTO @l_Room_Photos SELECT * FROM DBO.USP_SplitString(@Room_Photos,';')
		IF EXISTS(SELECT * FROM @l_Room_Photos)
			BEGIN 
				;WITH Split_Room_Photos (ID, Name, xmlname) AS
				( SELECT ID,Room_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Room_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Room_Photos )

				INSERT INTO [dbo].[C_Property_Room_Photos]
						   ([C_Property_ID]
						   ,[C_Photo_Location]
						   ,[C_Seq_ID])
				 SELECT      
					 @Property_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Room_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Room_Photos
			 END 
	END

END 




GO
/****** Object:  StoredProcedure [dbo].[INSERT_CONTACT_US]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERT_CONTACT_US]
(
	@name					NVARCHAR(100)	= NULL,
	@email					NVARCHAR(100)	= NULL,
	@mobile_no				NVARCHAR(100)	= NULL,
	@referral_name			NVARCHAR(100)	= NULL,
	@referral_email			NVARCHAR(150)	= NULL,
	@referral_mobile_no		NVARCHAR(50)	= NULL,
	@profile				NVARCHAR(50)	= NULL,
	@name_institute			NVARCHAR(255)	= NULL,
	@type_of_property		NVARCHAR(50)	= NULL,
	@no_of_bedroom			NVARCHAR(50)	= NULL,
	@rent					NVARCHAR(50)	= NULL,
	@currently_vacant		NVARCHAR(50)	= NULL,
	@type_of_room			NVARCHAR(50)	= NULL,
	@budget					NVARCHAR(50)	= NULL,
	@city					NVARCHAR(50)	= NULL,
	@area					NVARCHAR(100)	= NULL,
	@address				NVARCHAR(100)	= NULL,
	@message				NVARCHAR(2000)	= NULL,
	@status					INT				= NULL,
	@type					NVARCHAR(100)	= NULL
)
AS 

DECLARE @user_id				INT				= NEXT VALUE FOR [DBO].[USER_ID] 

BEGIN 

	INSERT INTO [dbo].[CONTACT_US]
		([user_id]
		,[name]
		,[email]
		,[mobile_no]
		,[referral_name]
		,[referral_email]
		,[referral_mobile_no]
		,[profile]
		,[name_institute]
		,[type_of_property]
		,[no_of_bedroom]
		,[rent]
		,[currently_vacant]
		,[type_of_room]
		,[budget]
		,[city]
		,[area]
		,[address]
		,[message]
		,[status]
		,[type])
	SELECT 
		@user_id				,
		@name					,
		@email					,
		@mobile_no				,
		@referral_name			,
		@referral_email			,
		@referral_mobile_no		,
		@profile				,
		@name_institute			,
		@type_of_property		,
		@no_of_bedroom			,
		@rent					,
		@currently_vacant		,
		@type_of_room			,
		@budget					,
		@city					,
		@area					,
		@address				,
		@message				,
		@status					,
		@type					


END 




GO
/****** Object:  StoredProcedure [dbo].[INSERT_PLACIO_PROPERTY]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERT_PLACIO_PROPERTY]
@Property_Name						VARCHAR(200) = NULL,
@Property_Alias_Name				VARCHAR(200) = NULL,
@Property_Address					VARCHAR(2000) = NULL,
--@Slug								VARCHAR(200)  = NULL, -- Single Lock-up Garage
@Property_Type_ID					INT = NULL,
@Property_Description				VARCHAR(2000) = NULL,
@Property_Status_Code				NCHAR(15)  = NULL,
@Meta_Title							NVARCHAR(200) = NULL,
@Meta_Keywords						NVARCHAR(2000) = NULL,
@Meta_Description					NVARCHAR(2000) = NULL,
@vr_url								NVARCHAR(2000) = NULL,
@Created_by							INT =  NULL,

@No_of_Floors						SMALLINT,
@Location_Score						SMALLINT,
@Comment_on_Location_Score			VARCHAR(200),
@Floor_Beds							SMALLINT,
@Room_Beds							SMALLINT,
@Room_Size							SMALLINT,
@Room_Quality_Score_ID				SMALLINT,
@Emergency_Staircase				BIT	= 0,
@Room_Cleanliness_Score_ID			SMALLINT,
@Water_Source_ID					SMALLINT,
@Water_Storage_Capacity_ID			SMALLINT,
@Common_Area_Cleanliness_ID			SMALLINT,
@Parking_Availability				BIT,
@Parking_Vehicle_Count				SMALLINT,
@House_Keeping_staff_Rooms			SMALLINT,
@Age_of_the_Property				SMALLINT,
----@Owner_Detail_ID				SMALLINT,
@longitute							NVARCHAR(100),
@latitute							NVARCHAR(100),
@IsNewProperty						BIT,
@IsCompetatorProperty				BIT,
-- [dbo].[AQ_Property_Diesel_Generator] Related Properties
@DG_Age								SMALLINT,
@DG_Brand							VARCHAR(20),
@DG_Warranty						VARCHAR(100),
@DG_AMC_Cost						INT,
@DG_storage_capacity				VARCHAR(20),
--Lift_ID int,
--Property_ID int,
@LIFT_Age							SMALLINT,
@LIFT_Brand							VARCHAR(20),
@LIFT_Warranty						VARCHAR(100),
@LIFT_AMC_Cost						INT,
-- Property Owner Details 
@Owner_Name							VARCHAR(100),
@Phone_Number						VARCHAR(20),
@Is_owner_Operated					BIT,

@Property_Addresses					VARCHAR(MAX)=NULL,
@Property_Owners					VARCHAR(MAX)=NULL,
@Property_Owner_Addresses			VARCHAR(MAX)=NULL,
--[dbo].[Property_Owner_Address_ID] 
-- Photos at various phases
@Property_Photos					VARCHAR(MAX)=NULL,
@Property_Amenities					VARCHAR(MAX)=NULL,
@Diesel_Generator_Photos			VARCHAR(MAX)=NULL,
@Lift_Photos						VARCHAR(MAX)=NULL,
@Room_Photos						VARCHAR(MAX)=NULL,
@House_Keeping_staff_Rooms_Photos	VARCHAR(MAX)=NULL,
@Area_Photos      		     		VARCHAR(MAX)=NULL,  --- NEWLY ADDED
@Property_Canteen_Photos			VARCHAR(MAX)=NULL,
@Property_Owner_Bank_Info			VARCHAR(MAX)=NULL,
@Property_Blocks					VARCHAR(MAX)=NULL,
@Property_Block_Floors				VARCHAR(MAX)=NULL,
@Property_Block_Floor_rooms			VARCHAR(MAX)=NULL,
/* PART B  */
--@Property_Alias_Name                VARCHAR(200),
@B_Rent_Deposit_ID					SMALLINT = NULL,  
@B_No_of_Beds						SMALLINT = NULL,  
@B_Gender							VARCHAR(20) = NULL, 
@B_No_of_Floors						SMALLINT = NULL, 
@B_Property_Occupancy_Estimate		VARCHAR(20) = NULL,  
@B_Room_wise_Power_meter			BIT = 0,  
@B_Room_Cleanliness_Score_ID		SMALLINT = NULL,  
@B_IsReception_AreaExists			BIT = 0,  
@B_wifi_Reception					SMALLINT = NULL,  
@B_Security_Features				VARCHAR(200)= NULL,  
@B_Fire_Fighting_Equipment			VARCHAR(200) = NULL, 
@B_ISP_Provider_Name				VARCHAR(200) = NULL,
@Is_Exisiting_Hostel                BIT = 0,
-- PART C
@B_IsCanteenExists					BIT = 0,   
@B_Canteen_Cleanliness_ID			SMALLINT = NULL, 
@B_Food_Availability_ID				SMALLINT = NULL,  
@B_Food_Score                       SMALLINT = NULL,
-- PART D
@Operator_Number                    VARCHAR(100)=NULL,
@Available_Placio                   BIT = 0
AS 
DECLARE 
@Property_ID						INT,
--@Property_Detail_ID					INT,
@Lift_ID							INT,
@Diesel_Generator_ID				INT,
@Owner_Detail_ID					INT,
@RowCnt								INT,
@Cnt								INT 

BEGIN 

	/* Property Details -  Insertion */
	SELECT  @Property_ID = NEXT VALUE FOR [dbo].[Property_ID]
	/* Property  -  Insertion */
	BEGIN 
		INSERT INTO [dbo].[Property]
				   ([Property_ID]
				   ,[Property_Name]
				   ,[Property_Alias_Name]
				   ,[Property_Type_ID]
				   --,[Property_Address_ID]
				   ,[Property_Description]
				   ,[Property_Status_Code]
				   --,[Cluster_Manager_ID]
				   --,[Property_Manager_ID]
				   ,[Meta_Title]
				   ,[Meta_Keywords]
				   ,[Meta_Description]
				   ,[vr_url]
				   ,[longitute]
				   ,[latitute]				   
				   ,[Created_at]
				   --,[Updated_at]
				   ,[Created_by]
				   --,[Updated_by]
				   )
		SELECT		
					@Property_ID			,
					@Property_Name			,
					@Property_Alias_Name	,
					@Property_Type_ID		,
					@Property_Description	,
					@Property_Status_Code	,
					@Meta_Title				,
					@Meta_Keywords			,
					@Meta_Description		,
					@vr_url					,
					@longitute				,
					@latitute				,
					GETDATE()				,
					@Created_by
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property  Other Details -  Insertion */
	BEGIN 
		INSERT INTO [dbo].[Property_Details]
					([Property_Detail_ID]
					,[Property_ID]
					,[No_of_Floors]
					,[Location_Score]
					,[Comment_on_Location_Score]
					,[Room_Quality_Score_ID]
					,[Room_Cleanliness_Score_ID]
					,[Water_Source_ID]
					,[Water_Storage_Capacity_ID]
					,[Emergency_Staircase]
					--,[Diesel_Generator_ID]
					,[Common_Area_Cleanliness_ID]
					,[Parking_Availability]
					,[Parking_Vehicle_Count]
					,[House_Keeping_staff_Rooms]
					,[Age_of_the_Property]
					--,[IsNewProperty]
					,[Gender]
					,[No_of_Beds]
					,[Property_Occupancy_Estimate]
					,[Room_wise_Power_Meter_ID]
					,[Room_Cleanliness_ID]
					,[Reception_Area_yn]
					,[Wifi_Reception]
					--,[Security_Feature_IDs]
					,[Fire_Fighting_Equipment_IDs]
					,[ISP_Provider_Name]
					,[IsCanteenExists]
					,[Canteen_Cleanliness_ID]
					,[Food_Availability_ID]
					,[Food_Score]
					,[Operator_Number])

		SELECT		
					NEXT VALUE FOR [dbo].[Property_Detail_ID],
					@Property_ID,
					@No_of_Floors,
					@Location_Score,
					@Comment_on_Location_Score,
					@Room_Quality_Score_ID,
					@Room_Cleanliness_Score_ID,
					@Water_Source_ID,
					@Water_Storage_Capacity_ID,
					@Emergency_Staircase,
					@Common_Area_Cleanliness_ID,
					@Parking_Availability,
					@Parking_Vehicle_Count,
					@House_Keeping_staff_Rooms,
					@Age_of_the_Property,
					@B_Gender,
					@B_No_of_Beds,
					@B_Property_Occupancy_Estimate,
					@B_Room_wise_Power_meter,
					@B_Room_Cleanliness_Score_ID,
					@B_IsReception_AreaExists,
					@B_wifi_Reception,
					@B_Fire_Fighting_Equipment,
					@B_ISP_Provider_Name,
					@B_IsCanteenExists,
					@B_Canteen_Cleanliness_ID,
					@B_Food_Availability_ID,
					@B_Food_Score,
					@Operator_Number
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property_Amenities Details -  Insertion */
	BEGIN 
		DECLARE @l_Property_Amenities TABLE ( ID INT IDENTITY(1,1), Property_Amenities NVARCHAR(MAX))
		DECLARE @Property_Amenities_Data TABLE	
			(	
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[amenity_id] [int] NOT NULL,
				[parent_id] [int] NOT NULL
			)

		INSERT INTO @l_Property_Amenities SELECT * FROM DBO.USP_SplitString(@Property_Amenities,';')
		;WITH Split_Property_Amenities(ID,Property_Amenities,xmlname)
			AS ( SELECT ID, Property_Amenities, CONVERT(XML,'<Names><name>'  
						   + REPLACE(REPLACE(Property_Amenities,',', '</name><name>') ,'&','&amp;') + '</name></Names>') AS xmlname
				  FROM @l_Property_Amenities )

		INSERT INTO @Property_Amenities_Data
		SELECT      
			 --xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')		AS [property_owner_id],    
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(10)')))		AS [amenity_id],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[2]','NVARCHAR(10)')))		AS [parent_id]
		 FROM Split_Property_Amenities

		SELECT @RowCnt = COUNT(1) FROM @Property_Amenities_Data
		SET @Cnt = 1

		WHILE @Cnt <= @RowCnt
			BEGIN
				INSERT INTO [dbo].[Property_Amenities]
					([property_id]
					,[amenity_id]
					,[parent_id]
					--,[crated_date]
					--,[crated_by]
					--,[active_yn]
					)				
				SELECT @Property_ID
					,PD.[amenity_id]
					,PD.[parent_id]
				FROM @Property_Amenities_Data PD WHERE ID = @Cnt
			END
		SET @Cnt = @Cnt +1
	END
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Address Details -  Insertion */
	BEGIN 
		DECLARE @l_Property_Addresses TABLE ( ID INT IDENTITY(1,1), Property_Addresses NVARCHAR(MAX))
		DECLARE @Property_Addresses_Data TABLE	
			(	
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[address_type_code] [nchar](15) NOT NULL,
				[address] [nvarchar](1000) NOT NULL,
				[city] [nvarchar](50) NOT NULL,
				[state] [nvarchar](50) NOT NULL,
				[country] [nvarchar](50) NOT NULL,
				[pincode] [nvarchar](50) NOT NULL,
				[email] [nvarchar](100) NOT NULL,
				[mobile_no] [nvarchar](50) NOT NULL,
				[other_address_details] [nvarchar](250) NULL,
				[date_address_from] [datetime] NULL,
				[date_address_to] [datetime] NULL,
				[property_name] [nvarchar](200) NULL
			)

		INSERT INTO @l_Property_Addresses SELECT * FROM DBO.USP_SplitString(@Property_Addresses,';')
		;WITH Split_Property_Addresses(ID,Property_Addresses,xmlname)
			AS ( SELECT ID, Property_Addresses, CONVERT(XML,'<Names><name>'  
						   + REPLACE(REPLACE(Property_Addresses,',', '</name><name>') ,'&','&amp;') + '</name></Names>') AS xmlname
				  FROM @l_Property_Addresses )

		INSERT INTO @Property_Addresses_Data
		SELECT      
			 --xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')		AS [property_owner_id],    
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')))		AS [address_type_code],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[2]','NVARCHAR(200)')))		AS [address],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[3]','NVARCHAR(50)')))		AS [city],  
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[4]','NVARCHAR(50)')))		AS [state],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[5]','NVARCHAR(50)')))		AS [country], 
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[6]','NVARCHAR(50)')))		AS [pincode],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[7]','NVARCHAR(50)')))		AS [email],  
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[8]','NVARCHAR(50)')))		AS [mobile_no],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[9]','NVARCHAR(2000)')))		AS [other_address_details],
			 CAST(xmlname.value('/Names[1]/name[10]','NVARCHAR(50)') AS DATE)		AS [date_address_from],	
			 CAST(xmlname.value('/Names[1]/name[11]','NVARCHAR(50)') AS DATE)		AS [date_address_to], 	
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[12]','NVARCHAR(50)')))		AS [property_name] 	
		 FROM Split_Property_Addresses

		SELECT @RowCnt = COUNT(1) FROM @Property_Addresses_Data
		SET @Cnt = 1

		WHILE @Cnt <= @RowCnt
			BEGIN
				DECLARE @P_Address_ID INT
				SELECT @P_Address_ID = NEXT VALUE FOR [dbo].[Address_ID]
			
				INSERT INTO [dbo].[Addresses]
				   ([address_id]
				   ,[address]
				   ,[city]
				   ,[state]
				   ,[country]
				   ,[pincode]
				   ,[email]
				   ,[mobile_no]
				   ,[other_address_details])
				SELECT @P_Address_ID
					,PD.[address]
					,PD.[city]
					,PD.[state]
					,PD.[country]
					,PD.[pincode]
					,PD.[email]
					,PD.[mobile_no]
					,PD.[other_address_details]
				FROM @Property_Addresses_Data PD WHERE ID = @Cnt

				INSERT INTO [dbo].[Property_Addresses]
						   ([Property_address_id]
						   ,[address_id]
						   ,[address_type_code]
						   ,[property_id]
						   ,[date_address_from]
						   ,[date_address_to])
				SELECT NEXT VALUE FOR [dbo].[Property_Address_ID]
						,@P_Address_ID
						,PD.[address_type_code]
						,@Property_ID
						,PD.[date_address_from]
						,PD.[date_address_to]
				FROM @Property_Addresses_Data PD WHERE ID = @Cnt
				SET @Cnt = @Cnt + 1;
			END;
		
		SET @RowCnt = 0
		SET @Cnt = 0	
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property-Owner Details -  Insertion */
	BEGIN 
		DECLARE @l_Property_Owners TABLE ( ID INT IDENTITY(1,1), Property_Owners NVARCHAR(MAX))
		DECLARE @Property_Owners_Data TABLE	
			(	
				[ID] [int] IDENTITY(1,1) NOT NULL,
				--[property_id] [int] NULL,
				[owner_name] [nvarchar](150) NOT NULL,
				[Gender] [nvarchar](10) NOT NULL,
				[aggrement_tenure] [nvarchar](100) NULL,
				[agreement_expire] [nvarchar](50) NULL,
				[aadhar_file] [nvarchar](255) NULL,
				[aadhar_no] [nvarchar](50) NULL,
				[pan_file] [nvarchar](255) NULL,
				[pan_no] [nvarchar](50) NULL,
				[cancel_check] [nvarchar](100) NULL,
				[lease_agreement] [nvarchar](100) NULL--,
				--[created_at] [nvarchar](50) NOT NULL
			)

		INSERT INTO @l_Property_Owners SELECT * FROM DBO.USP_SplitString(@Property_Owners,';')

	   ;WITH Split_Property_Owners(ID,Property_Owners,xmlname)
		AS ( SELECT ID, Property_Owners, CONVERT(XML,'<Names><name>'  
					   + REPLACE(REPLACE(Property_Owners,',', '</name><name>') ,'&','&amp;') + '</name></Names>') AS xmlname
			  FROM @l_Property_Owners )

		INSERT INTO @Property_Owners_Data
		SELECT      
			 --xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')		AS [property_owner_id],    
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')))		AS [owner_name],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[2]','NVARCHAR(100)')))		AS [Gender],  
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[3]','NVARCHAR(100)')))		AS [aggrement_tenure],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[4]','NVARCHAR(100)')))		AS [agreement_expire], 
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[5]','NVARCHAR(100)')))		AS [aadhar_file],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[6]','NVARCHAR(100)')))		AS [aadhar_no],  
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[7]','NVARCHAR(100)')))		AS [pan_file],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[8]','NVARCHAR(2000)')))		AS [pan_no],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[9]','NVARCHAR(2000)')))		AS [cancel_check],	
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[10]','NVARCHAR(2000)')))		AS [lease_agreement] 	
		 FROM Split_Property_Owners

		SELECT @RowCnt = COUNT(1) FROM @Property_Owners_Data
		SET @Cnt = 1

		WHILE @Cnt <= @RowCnt
			BEGIN
				INSERT INTO [dbo].[Property_Owner]
					   ([property_owner_id]
					   ,[property_id]
					   ,[owner_name]
					   ,[Gender]
					   ,[aggrement_tenure]
					   ,[agreement_expire]
					   ,[aadhar_file]
					   ,[aadhar_no]
					   ,[pan_file]
					   ,[pan_no]
					   ,[cancel_check]
					   ,[lease_agreement]
					   --,[created_at]
					   )
				SELECT NEXT VALUE FOR [dbo].[Property_Owner_ID]
					   ,@Property_ID
					   ,PD.[owner_name]
					   ,PD.[Gender]
					   ,PD.[aggrement_tenure]
					   ,PD.[agreement_expire]
					   ,PD.[aadhar_file]
					   ,PD.[aadhar_no]
					   ,PD.[pan_file]
					   ,PD.[pan_no]
					   ,PD.[cancel_check]
					   ,PD.[lease_agreement]
					   --,GETDATE()
				FROM @Property_Owners_Data PD WHERE ID = @Cnt
				SET @Cnt = @Cnt + 1;
			END;
		
		SET @RowCnt = 0
		SET @Cnt = 0		
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property-Owner-Address Details -  Insertion */
	BEGIN 
		DECLARE @l_Property_Owner_Addresses TABLE ( ID INT IDENTITY(1,1), Property_Owner_Addresses NVARCHAR(MAX))
		DECLARE @Property_Owner_Addresses_Data TABLE	
			(	
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[address_type_code] [nchar](15) NOT NULL,
				[address] [nvarchar](1000) NOT NULL,
				[city] [nvarchar](50) NOT NULL,
				[state] [nvarchar](50) NOT NULL,
				[country] [nvarchar](50) NOT NULL,
				[pincode] [nvarchar](50) NOT NULL,
				[email] [nvarchar](100) NOT NULL,
				[mobile_no] [nvarchar](50) NOT NULL,
				[other_address_details] [nvarchar](250) NULL,
				[date_address_from] [datetime] NULL,
				[date_address_to] [datetime] NULL,
				[pan_no] [nvarchar](50) NULL
			)

		INSERT INTO @l_Property_Owner_Addresses SELECT * FROM DBO.USP_SplitString(@Property_Owner_Addresses,';')
		;WITH Split_Property_Owner_Addresses(ID,Property_Owner_Addresses,xmlname)
			AS ( SELECT ID, Property_Owner_Addresses, CONVERT(XML,'<Names><name>'  
						   + REPLACE(REPLACE(Property_Owner_Addresses,',', '</name><name>') ,'&','&amp;') + '</name></Names>') AS xmlname
				  FROM @l_Property_Owner_Addresses )

		INSERT INTO @Property_Owner_Addresses_Data
		SELECT      
			 --xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')		AS [property_owner_id],    
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')))		AS [address_type_code],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[2]','NVARCHAR(200)')))		AS [address],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[3]','NVARCHAR(50)')))		AS [city],  
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[4]','NVARCHAR(50)')))		AS [state],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[5]','NVARCHAR(50)')))		AS [country], 
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[6]','NVARCHAR(50)')))		AS [pincode],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[7]','NVARCHAR(50)')))		AS [email],  
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[8]','NVARCHAR(50)')))		AS [mobile_no],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[9]','NVARCHAR(2000)')))		AS [other_address_details],
			 CAST(xmlname.value('/Names[1]/name[10]','NVARCHAR(50)') AS DATE)		AS [date_address_from],	
			 CAST(xmlname.value('/Names[1]/name[11]','NVARCHAR(50)') AS DATE)		AS [date_address_to], 	
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[12]','NVARCHAR(50)')))		AS [pan_no] 	
		 FROM Split_Property_Owner_Addresses

		SELECT @RowCnt = COUNT(1) FROM @Property_Owner_Addresses_Data
		SET @Cnt = 1

		WHILE @Cnt <= @RowCnt
			BEGIN
				DECLARE @Address_ID INT
				SELECT @Address_ID = NEXT VALUE FOR [dbo].[Address_ID]
			
				INSERT INTO [dbo].[Addresses]
				   ([address_id]
				   ,[address]
				   ,[city]
				   ,[state]
				   ,[country]
				   ,[pincode]
				   ,[email]
				   ,[mobile_no]
				   ,[other_address_details])
				SELECT @Address_ID
					,PD.[address]
					,PD.[city]
					,PD.[state]
					,PD.[country]
					,PD.[pincode]
					,PD.[email]
					,PD.[mobile_no]
					,PD.[other_address_details]
				FROM @Property_Owner_Addresses_Data PD WHERE ID = @Cnt
			
				INSERT INTO [dbo].[Property_Owner_Addresses]
						   ([Property_Owner_address_id]
						   ,[address_id]
						   ,[address_type_code]
						   ,[property_id]
						   ,[proerty_owner_id]
						   ,[date_address_from]
						   ,[date_address_to])
				SELECT NEXT VALUE FOR [dbo].[Property_Owner_Address_ID]
						,@Address_ID
						,PD.[address_type_code]
						,@Property_ID
						,(SELECT TOP 1 [property_owner_id]  FROM [dbo].[Property_Owner] 
								WHERE pan_no = PD.[pan_no] AND [property_id] = @Property_ID 
									AND [address_type_code] = PD.[address_type_code])
						--,@Property_Owner_Id
						,PD.[date_address_from]
						,PD.[date_address_to]
				FROM @Property_Owner_Addresses_Data PD WHERE ID = @Cnt
				SET @Cnt = @Cnt + 1;
			END;
		
		SET @RowCnt = 0
		SET @Cnt = 0	
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property-Owner-bank Details -  Insertion */
	BEGIN 
		DECLARE @l_Property_Owner_Bank_Info TABLE ( ID INT IDENTITY(1,1), Property_Owner_Bank_Info NVARCHAR(MAX))
		DECLARE @Property_Owner_Bank_Info_Data TABLE	
			(	
				[ID] [int] IDENTITY(1,1) NOT NULL,
--				[property_owner_id] [int] NOT NULL,
				[beneficiary_name] [nvarchar](100) NOT NULL,
				[bank_name] [nvarchar](100) NOT NULL,
				[branch_name] [nvarchar](100) NOT NULL,
				[ac_no] [nvarchar](100) NOT NULL,
				[ifsc_code] [nvarchar](50) NOT NULL,
				[city] [nvarchar](50) NOT NULL,
				[state] [nvarchar](50) NOT NULL,
				[country] [nvarchar](50) NOT NULL,
				[pincode] [nvarchar](50) NOT NULL,
				[bank_phone_number] [nvarchar](20) NOT NULL,
				[pan_no] [nvarchar](20) NOT NULL
			)

		INSERT INTO @l_Property_Owner_Bank_Info SELECT * FROM DBO.USP_SplitString(@Property_Owner_Bank_Info,';')
		;WITH Split_Property_Owner_Bank_Info(ID,Property_Owner_Bank_Info,xmlname)
			AS ( SELECT ID, Property_Owner_Bank_Info, CONVERT(XML,'<Names><name>'  
						   + REPLACE(REPLACE(Property_Owner_Bank_Info,',', '</name><name>') ,'&','&amp;') + '</name></Names>') AS xmlname
				  FROM @l_Property_Owner_Bank_Info )

		INSERT INTO @Property_Owner_Bank_Info_Data
		SELECT      
			 --xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')		AS [property_owner_id],    
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')))		AS [beneficiary_name],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[2]','NVARCHAR(100)')))		AS [bank_name],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[3]','NVARCHAR(100)')))		AS [branch_name],  
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[4]','NVARCHAR(100)')))		AS [ac_no],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[5]','NVARCHAR(100)')))		AS [ifsc_code], 
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[6]','NVARCHAR(50)')))		AS [city],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[7]','NVARCHAR(50)')))		AS [state],  
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[8]','NVARCHAR(50)')))		AS [country],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[9]','NVARCHAR(20)')))		AS [pincode],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[10]','NVARCHAR(20)')))		AS [bank_phone_number],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[11]','NVARCHAR(20)')))		AS [pan_no] 	
		 FROM Split_Property_Owner_Bank_Info

		SELECT @RowCnt = COUNT(1) FROM @Property_Owner_Bank_Info_Data
		SET @Cnt = 1

		WHILE @Cnt <= @RowCnt
			BEGIN
				INSERT INTO [dbo].[Property_Owner_Bank_Details]
						   (
							[property_owner_bank_det_id],
							[property_owner_id],
							[beneficiary_name],
							[bank_name],
							[branch_name],
							[ac_no],
							[ifsc_code],
							[city],
							[state],
							[country],
							[pincode],
							[bank_phone_number]
						   )
				SELECT NEXT VALUE FOR [dbo].[property_owner_bank_det_id]
						,(SELECT TOP 1 [property_owner_id]  FROM [dbo].[Property_Owner] 
								WHERE pan_no = PD.[pan_no] AND [property_id] = @Property_ID),
							PD.[beneficiary_name],
							PD.[bank_name],
							PD.[branch_name],
							PD.[ac_no],
							PD.[ifsc_code],
							PD.[city],
							PD.[state],
							PD.[country],
							PD.[pincode],
							PD.[bank_phone_number]
				FROM @Property_Owner_Bank_Info_Data PD WHERE ID = @Cnt
				SET @Cnt = @Cnt + 1;
			END;
		
		SET @RowCnt = 0
		SET @Cnt = 0	
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property Photos Details -  Insertion */
	BEGIN 
		DECLARE @l_Property_Photos TABLE ( ID INT IDENTITY(1,1), Property_Photo NVARCHAR(MAX))
		INSERT INTO @l_Property_Photos SELECT * FROM DBO.USP_SplitString(@Property_Photos,';')
		IF EXISTS(SELECT * FROM @l_Property_Photos)
			BEGIN 
				;WITH Split_Property_Photos (ID, Name, xmlname) AS
				( SELECT ID,Property_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Property_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Property_Photos )

				INSERT INTO [dbo].[Property_Photos]
					   ([Property_ID]
					   ,[Photo_Location]
					   ,[Seq_ID])
				 SELECT      
					 @Property_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Property_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Property_Photos
			END
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Blocks Insertion */
	BEGIN 
		DECLARE @l_Property_Blocks TABLE ( ID INT IDENTITY(1,1), Property_Block NVARCHAR(MAX))
		INSERT INTO @l_Property_Blocks SELECT * FROM DBO.USP_SplitString(@Property_Blocks,';')
		IF EXISTS(SELECT * FROM @l_Property_Blocks)
			BEGIN 
				;WITH Split_Property_Blocks (ID, Name, xmlname) AS
				( SELECT ID,Property_Block, CONVERT(XML,'<Names><name>' + REPLACE(Property_Block,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Property_Blocks )

				INSERT INTO [dbo].[Property_Block]
						   ([property_block_id]
						   ,[property_id]
						   ,[block_name]
						   ,[no_of_rooms])
				 SELECT  NEXT VALUE FOR [dbo].[Property_Block_ID],    
					 @Property_ID,
					 LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(100)'))) AS Property_Block,
					 LTRIM(RTRIM(xmlname.value('/Names[1]/name[2]','NVARCHAR(20)'))) AS [no_of_rooms]
				 FROM Split_Property_Blocks
			 END 
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Blocks-Floor Insertion */
	BEGIN 
		DECLARE @l_Property_Block_Floors TABLE ( ID INT IDENTITY(1,1), Property_Block_Floor NVARCHAR(MAX))
		INSERT INTO @l_Property_Block_Floors SELECT * FROM DBO.USP_SplitString(@Property_Block_Floors,';')
		IF EXISTS(SELECT * FROM @l_Property_Block_Floors)
			BEGIN 
				
				;WITH Split_Property_Block_Floors (ID, Name, xmlname) AS
				( SELECT ID,Property_Block_Floor, CONVERT(XML,'<Names><name>' + REPLACE(Property_Block_Floor,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Property_Block_Floors )

				INSERT INTO [dbo].[Property_Block_Floor]
						   ([Property_block_floor_id]
						   ,[property_id]
						   ,[property_block_id]
						   --,[floor_id]
						   ,[floor_name]
						   ,[no_of_rooms])
				 SELECT  NEXT VALUE FOR [dbo].[Property_Block_Floor_ID],    
					 @Property_ID,
					 (SELECT TOP 1 [property_block_id]  FROM [dbo].[Property_Block]  WHERE 
							[block_name] = LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')))
							AND  [property_id] = @Property_ID),
					 LTRIM(RTRIM(xmlname.value('/Names[1]/name[2]','NVARCHAR(50)'))) AS [floor_name],
					 LTRIM(RTRIM(xmlname.value('/Names[1]/name[3]','NVARCHAR(20)'))) AS [no_of_rooms]
				 FROM Split_Property_Block_Floors
			 END 
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Blocks-Floor-Rooms Insertion */
	BEGIN 
		DECLARE @l_Property_Block_Floor_rooms TABLE ( ID INT IDENTITY(1,1), Property_Block_Floor_rooms NVARCHAR(MAX))
		INSERT INTO @l_Property_Block_Floor_rooms SELECT * FROM DBO.USP_SplitString(@Property_Block_Floor_rooms,';')
		IF EXISTS(SELECT * FROM @l_Property_Block_Floor_rooms)
			BEGIN 
				
				;WITH Split_Property_Block_Floor_rooms (ID, Name, xmlname) AS
				( SELECT ID,Property_Block_Floor_rooms, CONVERT(XML,'<Names><name>' + REPLACE(Property_Block_Floor_rooms,',', '</name><name>') + '</name></Names>') AS xmlname 
					FROM @l_Property_Block_Floor_rooms )

				INSERT INTO [dbo].[Property_Floor_Rooms]
						   ([property_floor_room_id]
						   ,[property_id]
						   ,[property_block_id]
						   ,[property_block_floor_id]
						   ,[room_no]
						   ,[room_name]
						   ,[no_of_beds])
				 SELECT  NEXT VALUE FOR [dbo].[Property_Floor_Room_ID],    
					 @Property_ID,
					 (SELECT TOP 1 [property_block_id]  FROM [dbo].[Property_Block]  WHERE 
							[block_name] = LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')))   -- Block Name -- Block ID
							AND  [property_id] = @Property_ID
					),
					(SELECT TOP 1 [Property_block_floor_id]  FROM [dbo].[Property_Block_Floor]  WHERE 
							[floor_name] = LTRIM(RTRIM(xmlname.value('/Names[1]/name[2]','NVARCHAR(100)')))   -- [floor_name] - 
							AND  [property_id] = @Property_ID
							AND [property_block_id] = 	
								(SELECT TOP 1 [property_block_id]  FROM [dbo].[Property_Block]  WHERE 
										[block_name] = LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')))   
										AND  [property_id] = @Property_ID)
					),
					LTRIM(RTRIM(xmlname.value('/Names[1]/name[3]','NVARCHAR(50)')))  AS [room_no],
					LTRIM(RTRIM(xmlname.value('/Names[1]/name[4]','NVARCHAR(150)'))) AS [room_name],
					LTRIM(RTRIM(xmlname.value('/Names[1]/name[5]','NVARCHAR(50)')))  AS [no_of_beds]
				 FROM Split_Property_Block_Floor_rooms
			 END 
	END 

/*
	/* Property-Diesel Generator Details -  Insertion */
	BEGIN 
		SELECT @Diesel_Generator_ID =  NEXT VALUE FOR [dbo].[Diesel_Generator_ID]
		INSERT INTO [dbo].[Property_Diesel_Generator]
				   ([Diesel_Generator_ID]
				   ,[Property_ID]
				   ,[Age]
				   ,[Brand]
				   ,[Warranty]
				   ,[AMC_Cost]
				   ,[storage_capacity]
				   )
		SELECT 
					@Diesel_Generator_ID,
					@Property_ID,
					@DG_Age,
					@DG_Brand,
					@DG_Warranty,
					@DG_AMC_Cost,
					@DG_storage_capacity
		UPDATE [dbo].[Property_Details] SET [Diesel_Generator_ID] = @Diesel_Generator_ID
				WHERE [Property_ID] = @Property_ID
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Diesel Generator  Photo Insertion */
	BEGIN 
		DECLARE @l_Diesel_Generator TABLE ( ID INT IDENTITY(1,1), Diesel_Generator_Photo NVARCHAR(MAX))
		INSERT INTO @l_Diesel_Generator SELECT * FROM DBO.USP_SplitString(@Diesel_Generator_Photos,';')
		IF EXISTS(SELECT * FROM @l_Diesel_Generator)
			BEGIN 
				;WITH Split_Diesel_Generator_Photos (ID, Name, xmlname) AS
				( SELECT ID,Diesel_Generator_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Diesel_Generator_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Diesel_Generator )

				INSERT INTO [dbo].[Property_Diesel_Generator_Photos]
						   ([Property_ID]
						   ,[Diesel_Generator_ID]
						   ,[Photo_Location]
						   ,[Seq_ID])
				 SELECT      
					 @Property_ID,
					 @Diesel_Generator_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Diesel_Generator_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Diesel_Generator_Photos
			END 
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Lift  Details Insertion */
	BEGIN 
		SELECT @Lift_ID =  NEXT VALUE FOR [dbo.[Lift_ID]
		INSERT INTO [dbo].[Property_Lift_Inputs]
				   ([Lift_ID]
				   ,[Property_ID]
				   ,[Age]
				   ,[Brand]
				   ,[Warranty]
				   ,[AMC_Cost])
		SELECT 
					@Lift_ID,
					@Property_ID,
					@LIFT_Age,
					@LIFT_Brand,
					@LIFT_Warranty,
					@LIFT_AMC_Cost
		UPDATE [dbo].[Property_Details] SET [Lift_ID] = @Lift_ID
				WHERE [Property_ID] = @Property_ID
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Lift Photo Insertion */
	BEGIN 
		DECLARE @l_Lift_Photos TABLE ( ID INT IDENTITY(1,1), Lift_Photo NVARCHAR(MAX))
		INSERT INTO @l_Lift_Photos SELECT * FROM DBO.USP_SplitString(@Lift_Photos,';')
		IF EXISTS(SELECT * FROM @l_Lift_Photos)
			BEGIN 
				;WITH Split_Lift_Photos (ID, Name, xmlname) AS
				( SELECT ID,Lift_Photo, CONVERT(XML,'<Names><name>' + REPLACE(Lift_Photo,',', '</name><name>') + '</name></Names>') AS xmlname FROM @l_Lift_Photos )

				INSERT INTO [dbo].[Property_Lift_Photos]
						   ([Property_ID]
						   ,[Lift_ID]
						   ,[Photo_Location]
						   ,[Seq_ID])
				 SELECT      
					 @Property_ID,
					 @Lift_ID,
					 xmlname.value('/Names[1]/name[1]','NVARCHAR(100)') AS Lift_Photo_Location, 
					 ID AS SEQ_ID
				 FROM Split_Lift_Photos
			 END 
	END 

/*===============================================================================================================*/
/*===============================================================================================================*/
*/

END 


GO
/****** Object:  StoredProcedure [dbo].[INSERT_PROPERTY_REVENU_SHARING]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERT_PROPERTY_REVENU_SHARING]
( @property_revenu_sharing		VARCHAR(MAX)=NULL )
AS 
BEGIN 
	DECLARE @RowCnt								INT
	DECLARE @Cnt								INT
	DECLARE @l_property_revenu_sharing TABLE ( ID INT IDENTITY(1,1), property_revenu_sharing NVARCHAR(MAX))
	DECLARE @property_revenu_sharing_Data TABLE	
		(	
			[ID]						INT IDENTITY(1,1) NOT NULL,
			property_owner_id			INT NULL,
			property_id					INT NULL,
			owner_name					NVARCHAR(150)  NULL,
			revenue_sharing_Type_ID		SMALLINT NULL,
			percent_of_Sharing			DECIMAL(6,2) NULL,
			number_of_beds				INT NULL
		)

	INSERT INTO @l_property_revenu_sharing SELECT * FROM DBO.USP_SplitString(@property_revenu_sharing,';')
	;WITH Split_property_revenu_sharing(ID,property_revenu_sharing,xmlname)
		AS ( SELECT ID, property_revenu_sharing, CONVERT(XML,'<Names><name>'  
						+ REPLACE(REPLACE(property_revenu_sharing,',', '</name><name>') ,'&','&amp;') + '</name></Names>') AS xmlname
				FROM @l_property_revenu_sharing )

	INSERT INTO @property_revenu_sharing_Data
	SELECT      
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(10)')))		AS [property_owner_id],
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[2]','NVARCHAR(10)')))		AS [property_id],
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[3]','NVARCHAR(150)')))	AS [owner_name],  
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[4]','NVARCHAR(10)')))		AS [revenue_sharing_Type_ID],
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[5]','NVARCHAR(50)')))		AS [percent_of_Sharing], 
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[6]','NVARCHAR(10)')))		AS [number_of_beds] 	
		FROM Split_property_revenu_sharing

	SELECT @RowCnt = COUNT(1) FROM @property_revenu_sharing_Data
	SET @Cnt = 1

	WHILE @Cnt <= @RowCnt
		BEGIN
			INSERT INTO [dbo].[Property_Revenue_Sharing]
					   ([property_owner_id]
					   ,[property_id]
					   ,[owner_name]
					   ,[revenue_sharing_Type_ID]
					   ,[percent_of_Sharing]
					   ,[number_of_beds])
			SELECT		[property_owner_id]
					   ,[property_id]
					   ,[owner_name]
					   ,[revenue_sharing_Type_ID]
					   ,[percent_of_Sharing]
					   ,[number_of_beds]
			FROM @property_revenu_sharing_Data PD WHERE ID = @Cnt
			SET @Cnt = @Cnt + 1;
		END;
END

GO
/****** Object:  StoredProcedure [dbo].[INSERT_READINESS_FOR_PROCURE_CATEGORIES]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERT_READINESS_FOR_PROCURE_CATEGORIES]
(
	@readiness_for_procure_category_name NVARCHAR(250) = NULL 
) AS 

DECLARE @readiness_for_procure_category_code nchar(15) = NEXT VALUE FOR dbo.readiness_for_procure_category_code
BEGIN 

INSERT INTO [dbo].[Readiness_for_Procure_Categories]
    ([readiness_for_procure_category_code]
    ,[readiness_for_procure_category_name])

SELECT 
	@readiness_for_procure_category_code,
	UPPER(LTRIM(RTRIM(@readiness_for_procure_category_name)))

END 





GO
/****** Object:  StoredProcedure [dbo].[PUBLISH_PLACIO_PROPERTY]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PUBLISH_PLACIO_PROPERTY] (@AQ_Property_ID INT, @loginUserId INT)
AS 
DECLARE 
@Property_ID						INT,
@Lift_ID							INT,
@Diesel_Generator_ID				INT,
@Owner_Detail_ID					INT,
@RowCnt								INT,
@Cnt								INT,
@No_of_Blocks						INT,
@No_Of_Floors						INT,
@No_Of_Beds							INT,
@Canteen_ID							INT

BEGIN 
	SELECT  @Property_ID = NEXT VALUE FOR [dbo].[Property_ID]
	/* Property  -  Insertion */
	BEGIN 
		INSERT INTO [dbo].[Property]
			([Property_ID]
			,[Property_Name]
			,[Property_Alias_Name]
			,[Property_Type_ID]
			--,[Property_Address_ID]
			,[Property_Description]
			,[Property_Status_Code]
			--,[Cluster_Manager_ID]
			--,[Property_Manager_ID]
			,[Meta_Title]
			,[Meta_Keywords]
			,[Meta_Description]
			,[vr_url]
			,[longitute]
			,[latitute]				   
			,[Created_at]
			--,[Updated_at]
			,[Created_by]
			--,[Updated_by]
			)

		SELECT 
			 @Property_ID
			,[Property_Name]
			,Property_Alias_Name
			,NULL
			--,NULL
			,NULL
			,'PUBLISHED'
			--,NULL
			--,NULL
			,NULL
			,NULL
			,NULL
			,NULL
			,[longitute]
			,[latitute]
			,GETDATE()
			,@loginUserId
		FROM [dbo].[AQ_PROPERTY] WHERE [Property_ID] = @AQ_Property_ID
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property  Other Details -  Insertion */
	BEGIN 
		INSERT INTO [dbo].[Property_Details]
			([Property_Detail_ID]
			,[Property_ID]
			,[No_of_Floors]
			,[Location_Score]
			,[Comment_on_Location_Score]
			,[Room_Quality_Score_ID]
			,[Room_Cleanliness_Score_ID]
			,[Water_Source_ID]
			,[Water_Storage_Capacity_ID]
			,[Emergency_Staircase]
			--,[Diesel_Generator_ID]
			,[Common_Area_Cleanliness_ID]
			,[Parking_Availability]
			,[Parking_Vehicle_Count]
			,[House_Keeping_staff_Rooms]
			,[Age_of_the_Property]
			--,[IsNewProperty]
			,[Gender]
			,[No_of_Beds]
			,[Property_Occupancy_Estimate]
			,[Room_wise_Power_Meter_ID]
			,[Room_Cleanliness_ID]
			,[Reception_Area_yn]
			,[Wifi_Reception]
			--,[Security_Feature_IDs]
			,[Fire_Fighting_Equipment_IDs]
			,[ISP_Provider_Name]
			,[IsCanteenExists]
			,[Canteen_Cleanliness_ID]
			,[Food_Availability_ID]
			,[Food_Score]
			,[Operator_Number])

		SELECT		
			NEXT VALUE FOR [dbo].[Property_Detail_ID]
			,@Property_ID
			,[No_of_Floors]
			,[Location_Score]
			,[Comment_on_Location_Score]
			,[Room_Quality_Score_ID]
			,[Room_Cleanliness_Score_ID]
			,[Water_Source_ID]
			,[Water_Storage_Capacity_ID]
			,[Emergency_Staircase]

			,[Common_Area_Cleanliness_ID]
			,[Parking_Availability]
			,[Parking_Vehicle_Count]
			,[House_Keeping_staff_Rooms]
			,[Age_of_the_Property]

			,[Gender]
			,[No_of_Beds]
			,[Property_Occupancy_Estimate]
			,[Room_wise_Power_Meter_ID]
			,[Room_Cleanliness_ID]
			,[Reception_Area_yn]
			,[Wifi_Reception]

			,[Fire_Fighting_Equipment_IDs]
			,[ISP_Provider_Name]
			,IsCanteenExists
			,Canteen_Cleanliness_ID
			,Food_Availability_ID
			,Food_Score
			,Operator_Number
		FROM [dbo].[AQ_PROPERTY] WHERE [Property_ID] = @AQ_Property_ID

		SELECT 
			@No_of_Blocks	= 1,
			@No_Of_Floors	= [No_of_Floors],
			@No_Of_Beds		= [No_of_Beds]
		FROM [dbo].[AQ_PROPERTY] WHERE [Property_ID] = @AQ_Property_ID

	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Address Details -  Insertion */
	BEGIN 
		DECLARE @P_Address_ID INT
		SELECT @P_Address_ID = NEXT VALUE FOR [dbo].[Address_ID]
			
		INSERT INTO [dbo].[Addresses]
			([address_id]
			,[address]
			--,[city]
			--,[state]
			--,[country]
			--,[pincode]
			--,[email]
			--,[mobile_no]
			--,[other_address_details]
			)
		SELECT @P_Address_ID
			 ,[Property_Address]
			--,PD.[city]
			--,PD.[state]
			--,PD.[country]
			--,PD.[pincode]
			--,PD.[email]
			--,PD.[mobile_no]
			--,PD.[other_address_details]
		FROM [dbo].[AQ_PROPERTY] WHERE [Property_ID] = @AQ_Property_ID

		INSERT INTO [dbo].[Property_Addresses]
					([Property_address_id]
					,[address_id]
					,[address_type_code]
					,[property_id]
					,[date_address_from]
					,[date_address_to])
		SELECT NEXT VALUE FOR [dbo].[Property_Address_ID]
				,@P_Address_ID
				,'OFFICE' --PD.[address_type_code]
				,@Property_ID
				,NULL --PD.[date_address_from]
				,NULL --PD.[date_address_to]
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property-Owner Details -  Insertion */
	BEGIN 
				INSERT INTO [dbo].[Property_Owner]
					   ([property_owner_id]
					   ,[property_id]
					   ,[owner_name]
					   --,[Gender]
					   --,[aggrement_tenure]
					   --,[agreement_expire]
					   --,[aadhar_file]
					   --,[aadhar_no]
					   --,[pan_file]
					   --,[pan_no]
					   --,[cancel_check]
					   --,[lease_agreement]
					   --,[created_at]
					   )
				SELECT NEXT VALUE FOR [dbo].[Property_Owner_ID]
					   ,@Property_ID
					   ,[owner_name]
					   --,PD.[Gender]
					   --,PD.[aggrement_tenure]
					   --,PD.[agreement_expire]
					   --,PD.[aadhar_file]
					   --,PD.[aadhar_no]
					   --,PD.[pan_file]
					   --,PD.[pan_no]
					   --,PD.[cancel_check]
					   --,PD.[lease_agreement]
					   --,GETDATE()
				FROM [dbo].[AQ_Property_Owner_Details] WHERE [Property_ID] = @AQ_Property_ID
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/	
	/* Property Photos Details -  Insertion */
	BEGIN 
		INSERT INTO [dbo].[Property_Photos]
				([Property_ID]
				,[Photo_Location]
				,[Seq_ID])
			SELECT 
				@Property_ID
				,[PhotoLocation]
				,[Seq_ID]
			FROM [dbo].[AQ_Property_Photos] WHERE [Property_ID] =  @AQ_Property_ID
			ORDER BY [Seq_ID]
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Blocks Insertion */

	/* As of now, the block information is not providing for acquistion property.  Assued there is only one block availble and kept it in the respective table. */
	BEGIN 
			INSERT INTO [dbo].[Property_Block]
						([property_block_id]
						,[property_id]
						,[block_name]
						,[no_of_rooms])
			SELECT   [property_block_id]
					,@Property_ID
					,[block_name]
					,[no_of_rooms]
			FROM [dbo].[AQ_Property_Block] WHERE [Property_ID] =  @AQ_Property_ID
	END 
	/* Property-Block-Floor Insertion */
	BEGIN 
		INSERT INTO [dbo].[Property_Block_Floor]
								([Property_block_floor_id]
								,[property_id]
								,[property_block_id]
								--,[floor_id]
								,[floor_name]
								,[no_of_rooms])

		SELECT [Property_block_floor_id]
				,@Property_ID
				,[property_block_id]
				--,[floor_id]
				,[floor_name]
				,[no_of_rooms]
			FROM [dbo].[AQ_Property_Block_Floor] WHERE [Property_ID] =  @AQ_Property_ID

		INSERT INTO [dbo].[Property_Floor_Rooms]
			([property_floor_room_id]
			,[property_id]
			,[property_block_id]
			,[property_block_floor_id]
			,[room_no]
			,[room_name]
			,[no_of_beds])

		SELECT [property_floor_room_id]
				,@Property_ID
				,[property_block_id]
				,[property_block_floor_id]
				,[room_no]
				,[room_name]
				,[no_of_beds]
			FROM [dbo].[AQ_Property_Floor_Rooms] WHERE [Property_ID] =  @AQ_Property_ID

	END
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Diesel Generator Details -  Insertion */
	BEGIN 
		SELECT @Diesel_Generator_ID =  NEXT VALUE FOR [dbo].[Diesel_Generator_ID]
		INSERT INTO [dbo].[Property_Diesel_Generator]
				   ([Diesel_Generator_ID]
				   ,[Property_ID]
				   ,[Age]
				   ,[Brand]
				   ,[Warranty]
				   ,[AMC_Cost]
				   ,[storage_capacity]
				   )
		SELECT @Diesel_Generator_ID 
			  ,@Property_ID
			  ,[Age]
			  ,[Brand]
			  ,[Warranty]
			  ,[AMC_Cost]
			  ,[storage_capacity]
		  FROM [dbo].[AQ_Property_Diesel_Generator] WHERE [Property_ID] = @AQ_Property_ID

		UPDATE [dbo].[Property_Details] SET [Diesel_Generator_ID] = @Diesel_Generator_ID
				WHERE [Property_ID] = @Property_ID
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Diesel Generator  Photo Insertion */
	BEGIN 
		INSERT INTO [dbo].[Property_Diesel_Generator_Photos]
			([Property_ID]
			,[Diesel_Generator_ID]
			,[Photo_Location]
			,[Seq_ID])
		SELECT 
			@Property_ID
			,@Diesel_Generator_ID
			,[PhotoLocation]
			,[Seq_ID]
		FROM [dbo].[AQ_Property_Diesel_Generator_Photos] WHERE [Property_ID] = @AQ_Property_ID
		
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Lift  Details Insertion */
	BEGIN 
		SELECT @Lift_ID =  NEXT VALUE FOR [dbo].[Lift_ID]
		INSERT INTO [dbo].[Property_Lift_Inputs]
			([Lift_ID]
			,[Property_ID]
			,[Age]
			,[Brand]
			,[Warranty]
			,[AMC_Cost])
		SELECT 
			 @Lift_ID
			,@Property_ID
			,[Age]
			,[Brand]
			,[Warranty]
			,[AMC_Cost]
		FROM [dbo].[AQ_Property_Lift_Inputs] WHERE [Property_ID] = @AQ_Property_ID

		UPDATE [dbo].[Property_Details] SET [Lift_ID] = @Lift_ID
				WHERE [Property_ID] = @Property_ID
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Lift Photo Insertion */
	BEGIN 
		INSERT INTO [dbo].[Property_Lift_Photos]
			([Property_ID]
			,[Lift_ID]
			,[Photo_Location]
			,[Seq_ID])
		SELECT 
			 @Property_ID
			,@Lift_ID
			,[PhotoLocation]
			,[Seq_ID]
		FROM [dbo].[AQ_Property_Lift_Photos] WHERE [Property_ID] = @AQ_Property_ID

	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Canteen Details Insertion */
	BEGIN 
		--SELECT @Canteen_ID =  NEXT VALUE FOR dbo.[canteen_ID]
		INSERT INTO [dbo].[Property_Canteen_Photos]
				   ([Property_ID]
				   ,[PhotoLocation]
				   ,[Seq_ID])
		 SELECT @Property_ID
			  ,[PhotoLocation]
			  ,[Seq_ID]
		  FROM [dbo].[AQ_Property_Canteen_Photos]  WHERE [Property_ID] = @AQ_Property_ID
		
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property-Security_Features Insertion */
	BEGIN 
		INSERT INTO [dbo].[Property_Security_Features]
				   ([Property_ID]
				   ,[Security_Feature_ID])

		SELECT 
			   @Property_ID
			  ,[Security_Feature_ID]
		  FROM [dbo].[AQ_Property_Security_Features] WHERE [Property_ID] = @AQ_Property_ID

	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	-- Updating [dbo].[AQ_Property] with published details.
	BEGIN 
		UPDATE [dbo].[AQ_Property]
			SET  [Published_yn] = 1
			  ,[Published_on] = GETDATE()
			  ,[Published_by] = @loginUserId
			  ,Placio_Property_ID = @Property_ID
		WHERE [Property_ID] = @AQ_Property_ID
		-- Inserting Onboarding Standard Templete check list after acquiring property into Placio.
		-- This table [dbo].[Property_Onboard_TimeLines_Template] will hold standard list of items Placio typically holds.
		INSERT INTO [dbo].[Property_Onboard_TimeLines] 
			SELECT [property_timeline_id]
			  ,@Property_ID
			  ,[item_code]
			  ,[item_name]
			  ,[delivery_date]
			  ,[installation]
			  ,[commissioning]
			  ,[dependency]
			  ,[landlord_completion_date]
			  ,[comment_1]
			  ,[comment_2]
			  ,[placio_targeted_date]
		  FROM [dbo].[Property_Onboard_TimeLines_Template]
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property_Floor_Room_Bed  Insertion */
	BEGIN 
		INSERT INTO [dbo].[Property_Floor_Room_Bed]
			([bed_id]
			,[property_id]
			,[block_id]
			,[floor_id]
			,[room_no]
			,[bed_no]
			,[price]
			,[status])

		SELECT [bed_id]
			  ,@Property_ID
			  ,[block_id]
			  ,[floor_id]
			  ,[room_no]
			  ,[bed_no]
			  ,[price]
			  ,[status]
		  FROM [dbo].[AQ_Property_Floor_Room_Bed] WHERE [Property_ID] = @AQ_Property_ID
	END 
/*===============================================================================================================*/
/*===============================================================================================================*/
	/* Property Amenities  Insertion */
	DECLARE @l_Property_Amenities TABLE ( ID INT IDENTITY(1,1), Property_Amenities NVARCHAR(MAX))
	DECLARE @Property_Amenities NVARCHAR(MAX) = NULL

	SELECT @Property_Amenities = Property_Amenities_ID FROM [dbo].[AQ_Property] WHERE [Property_ID] = @AQ_Property_ID

		DECLARE @Property_Amenities_Data TABLE	
			(	
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[amenity_id] [int] NOT NULL,
				[parent_id] [int] NOT NULL
			)

		INSERT INTO @l_Property_Amenities SELECT * FROM DBO.USP_SplitString(@Property_Amenities,';')
		;WITH Split_Property_Amenities(ID,Property_Amenities,xmlname)
			AS ( SELECT ID, Property_Amenities, CONVERT(XML,'<Names><name>'  
						   + REPLACE(REPLACE(Property_Amenities,',', '</name><name>') ,'&','&amp;') + '</name></Names>') AS xmlname
				  FROM @l_Property_Amenities )

		INSERT INTO @Property_Amenities_Data
		SELECT      
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(10)')))		AS [amenity_id],
			 LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(10)')))		AS [parent_id]
		 FROM Split_Property_Amenities

		SELECT @RowCnt = COUNT(1) FROM @Property_Amenities_Data
		SET @Cnt = 1

		WHILE @Cnt <= @RowCnt
			BEGIN
				INSERT INTO [dbo].[Property_Amenities]
					([property_id]
					,[amenity_id]
					,[parent_id]
					,[crated_date]
					--,[crated_by]
					,[active_yn]
					)				
				SELECT @Property_ID
					,PD.[amenity_id]
					,PD.[parent_id]
					,GETDATE()
					,1
				FROM @Property_Amenities_Data PD WHERE ID = @Cnt
			END
		SET @Cnt = @Cnt +1
/*===============================================================================================================*/
/*===============================================================================================================*/

	END





GO
/****** Object:  StoredProcedure [dbo].[UDP_GET_COMPLAINT_TYPES]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_GET_COMPLAINT_TYPES]
AS 
BEGIN
SELECT [complaint_type_id]
      ,[complaint_type_cd]
      ,[complaint_type]
      ,[active_yn]
  FROM [dbo].[Complaint_Types]
  WHERE [active_yn] = 1
  ORDER BY [complaint_type]
  
END 




GO
/****** Object:  StoredProcedure [dbo].[UDP_GET_HK_CHECK_LIST]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_GET_HK_CHECK_LIST] AS 
BEGIN 
SELECT [ITEM_CATEGORY_ID]
      ,[ITEM_CATEOGRY_CODE]
      ,[ITEM_CATEGORY_NAME]
      ,[ITEM_CATEGORY_SUB_ID]
      ,[ITEM_CATEOGRY_SUB_CODE]
      ,[SUB_CATEGORY_NAME]
      ,[CATEGORY_ITEM_ID]
      ,[CATEGORY_ITEM_CODE]
      ,[CATEGORY_ITEM_NAME]
  FROM [dbo].[VW_HK_CATEGORYWISE_ITEMS]
  ORDER BY [ITEM_CATEGORY_ID], [ITEM_CATEGORY_SUB_ID], [CATEGORY_ITEM_ID]
END 

GO
/****** Object:  StoredProcedure [dbo].[UDP_GET_PROPERTY_HK_CHECKLIST]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_GET_PROPERTY_HK_CHECKLIST](
@Property_ID INT = NULL,
@Employee_ID INT = NULL,
@date_of_submission DATE = NULL
)
AS 
BEGIN 


	IF EXISTS ( SELECT 1 FROM dbo.PR_Procure_Checklist_Status
				 WHERE [property_id] = @Property_ID )
		BEGIN 
			SELECT PHKC.[property_hk_check_list_id]
				  ,PHKC.[property_id]
				  ,PHKC.[category_item_id]
				  ,PHKC.[item_category_id]
				  ,PHKC.[item_category_sub_id]
				  ,PHKC.[category_item_name]
				  ,PHKC.[applicable_yn]
				  ,PHKC.[available_yn]
				  ,PHKC.[completed_yn]
				  ,PHKCS.[employee_id]
				  ,PHKCS.[date_of_submission]
				  ,PHKCS.[remarks]
				  ,PHKCS.[updated_by]
				  ,PHKCS.[updated_on]
			  FROM [dbo].[Property_HK_CheckList] PHKC
				LEFT JOIN [dbo].[PR_HK_Checklist_Status] PHKCS 
					ON PHKCS.[property_hk_check_list_id]  = PHKC.property_hk_check_list_id
						AND PHKCS.[property_id]  = PHKC.[property_id]
			  WHERE PHKC.[property_id] = @Property_ID
			  ORDER BY PHKC.[item_category_sub_id], PHKC.[category_item_id]
		END 
	ELSE 
		BEGIN
		  SELECT TOP 1 PHKCS.[property_hk_check_list_id] 
			  ,PHKCS.[property_id]
			  ,PHKCS.[category_item_id]
			  ,PHKC.[item_category_id]
			  ,PHKC.[item_category_sub_id]
			  ,PHKC.[category_item_name]
			  ,PHKCS.[applicable_yn]
			  ,PHKCS.[available_yn]
			  ,PHKCS.[completed_yn]
			  ,PHKCS.[employee_id]
			  ,PHKCS.[date_of_submission]
			  ,PHKCS.[remarks]
			  ,PHKCS.[updated_by]
			  ,PHKCS.[updated_on]
		  FROM [dbo].[PR_HK_Checklist_Status] PHKCS 
				INNER JOIN [dbo].[Property_HK_CheckList] PHKC
					ON PHKCS.[property_hk_check_list_id]  = PHKC.property_hk_check_list_id
						AND PHKCS.[property_id]  = PHKC.[property_id]
		WHERE PHKCS.[property_id] = @Property_ID
				AND PHKCS.employee_id = @Employee_ID
				AND PHKCS.date_of_submission = @date_of_submission
		ORDER BY PHKCS.[property_id], PHKC.[item_category_sub_id], PHKC.[category_item_id], PHKCS.employee_id, PHKCS.date_of_submission

		END



END 



GO
/****** Object:  StoredProcedure [dbo].[UDP_GET_PROPERTY_PROCURE_CHECK_ITEMS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_GET_PROPERTY_PROCURE_CHECK_ITEMS]
(
	@Property_ID INT = NULL
)
AS 
BEGIN 

/* This procedure will get standard Property Procure check List items against a property once 
the property is get approved for onboard from country head.

The respective ground teams have to reach out to the property owners to collect the checklist documents and provide respective 
inputs to the application.

*/
	IF EXISTS ( SELECT 1 FROM dbo.PR_Procure_Checklist_Status
				 WHERE [property_id] = @Property_ID)
		BEGIN 
			SELECT TOP 1 PPCS.[property_id]
					,PPCS.[readiness_for_procure_checklist_item_id]
					,PC.[readiness_for_procure_category_code]
					,[readiness_for_procure_category_name]
					,[readiness_for_procure_checklist_item_name]
					,PPCS.[applicable_yn]
					,PPCS.[available_yn]
					,PPCS.[completed_yn]
					,PPCS.[employee_id]
					,PPCS.[date_of_visit]
					,PPCS.[remarks]
					,PPCS.[updated_by]
					,PPCS.[updated_on]
	  				FROM [dbo].[Readiness_for_Procure_Categories] PC
				INNER JOIN [dbo].[Readiness_for_Procure_Checklist_Items] PCI
					ON PC.[readiness_for_procure_category_code] = PCI.[readiness_for_procure_category_code]
				LEFT JOIN [dbo].[PR_Procure_Checklist_Status] PPCS 
					ON PPCS.readiness_for_procure_checklist_item_id = PCI.readiness_for_procure_checklist_item_id
			WHERE PPCS.[property_id] = @Property_ID
				--AND PPCS.[updated_on] = SELECT MAX(PPCS.[updated_on]) FROM 
				--		[dbo].[PR_Procure_Checklist_Status] WHERE property_id = PPCS.[property_id]
			ORDER BY PPCS.[updated_on], PC.[readiness_for_procure_category_code]
		END	  
	ELSE 
		BEGIN
			SELECT PRPCI.[property_id],
					PCI.readiness_for_procure_checklist_item_id,
					PC.[readiness_for_procure_category_code],
					[readiness_for_procure_category_name],
					[readiness_for_procure_checklist_item_name],
					PRPCI.[applicable_yn],
					PRPCI.[available_yn],
					PRPCI.[completed_yn]
					,PPCS.[employee_id]
					,PPCS.[date_of_visit]
					,PPCS.[remarks]
					,PPCS.[updated_by]
					,PPCS.[updated_on]
				FROM [dbo].[Readiness_for_Procure_Categories] PC
				INNER JOIN [dbo].[Readiness_for_Procure_Checklist_Items] PCI
					ON PC.[readiness_for_procure_category_code] = PCI.[readiness_for_procure_category_code]
				LEFT JOIN [dbo].[Properties_Readiness_for_Procure_Checklist_Items] PRPCI
					ON PCI.readiness_for_procure_checklist_item_id = PRPCI.[readiness_for_procure_checklist_item_id]
				LEFT JOIN [dbo].[PR_Procure_Checklist_Status] PPCS 
					ON PPCS.readiness_for_procure_checklist_item_id = PCI.readiness_for_procure_checklist_item_id
							AND PPCS.property_id = PRPCI.property_id
			WHERE PRPCI.[property_id] = @Property_ID
			ORDER BY PC.[readiness_for_procure_category_code]
		END 
END 

GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_COMPANY_DETAILS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_COMPANY_DETAILS] (
	--@company_id					int				= NULL 
	@logo						nvarchar(200)	= NULL , 
	@fevicon					nvarchar(100)	= NULL , 
	@admin_name					nvarchar(100)	= NULL , 
	@site_name					nvarchar(300)	= NULL , 
	@site_url					nvarchar(300)	= NULL , 
	@front_url					nvarchar(1000)	= NULL , 
	@email						nvarchar(100)	= NULL , 
	@mobile_no					nvarchar(50)	= NULL , 
	@sms_api					nvarchar(1000)	= NULL , 
	@sms_send_status			nchar(10)		= NULL , 
	@meta_title					nvarchar(100)	= NULL , 
	@meta_keywords				nvarchar(1000)	= NULL , 
	@meta_description			nvarchar(255)	= NULL , 
	@google_analytics_code		nvarchar(1000)	= NULL , 
	@html_meta_code				nvarchar(100)	= NULL , 
	@footer_copyright			nvarchar(100)	= NULL , 
	@facebook_url				nvarchar(100)	= NULL , 
	@twitter_url				nvarchar(100)	= NULL , 
	@youtube_url				nvarchar(100)	= NULL , 
	@google_plus_url			nvarchar(100)	= NULL , 
	@pinterest_url				nvarchar(100)	= NULL , 
	@linkedin_url				nvarchar(100)	= NULL , 
	@instagram_url				nvarchar(100)	= NULL , 
	@facebook_app_id			nvarchar(100)	= NULL , 
	@facebook_app_secret		nvarchar(100)	= NULL , 
	@google_secret_key			nvarchar(100)	= NULL , 
	@google_developer_key		nvarchar(100)	= NULL 
) 
AS 

BEGIN 


INSERT INTO [dbo].[Company_Settings]
        ([company_id]
        ,[logo]
        ,[fevicon]
        ,[admin_name]
        ,[site_name]
        ,[site_url]
        ,[front_url]
        ,[email]
        ,[mobile_no]
        ,[sms_api]
        ,[sms_send_status]
        ,[meta_title]
        ,[meta_keywords]
        ,[meta_description]
        ,[google_analytics_code]
        ,[html_meta_code]
        ,[footer_copyright]
        ,[facebook_url]
        ,[twitter_url]
        ,[youtube_url]
        ,[google_plus_url]
        ,[pinterest_url]
        ,[linkedin_url]
        ,[instagram_url]
        ,[facebook_app_id]
        ,[facebook_app_secret]
        ,[google_secret_key]
        ,[google_developer_key])
	SELECT 
   		NEXT VALUE FOR dbo.Company_ID,
		@logo						 , 
		@fevicon					 , 
		@admin_name					 , 
		@site_name					 , 
		@site_url					 , 
		@front_url					 , 
		@email						 , 
		@mobile_no					 , 
		@sms_api					 , 
		@sms_send_status			 , 
		@meta_title					 , 
		@meta_keywords				 , 
		@meta_description			 , 
		@google_analytics_code		 , 
		@html_meta_code				 , 
		@footer_copyright			 , 
		@facebook_url				 , 
		@twitter_url				 , 
		@youtube_url				 , 
		@google_plus_url			 , 
		@pinterest_url				 , 
		@linkedin_url				 , 
		@instagram_url				 , 
		@facebook_app_id			 , 
		@facebook_app_secret		 , 
		@google_secret_key			 , 
		@google_developer_key		 
END 
GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_COMPANY_EMPLOYEES]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDP_INSERT_COMPANY_EMPLOYEES]
(    
    @employee_id			INT=NULL,
	@company_id				INT=NULL,
	@job_title_code			NCHAR(15) = NULL,
    @date_from				DATE = NULL,
    @date_to				DATE = NULL
)
AS
BEGIN
   INSERT INTO [dbo].[Company_Employees]
		([employee_id]
		,[company_id]
		,[job_title_code]
		,[date_from]
		,[date_to])

	SELECT 
		@employee_id			,
		@company_id				,
		@job_title_code			,
		@date_from				,
		@date_to				    

END




GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_COMPANY_SETTINGS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_COMPANY_SETTINGS] 
(
--company_id, int,>
@logo							nvarchar(200)	= NULL,
@fevicon						nvarchar(100)	= NULL,
@admin_name						nvarchar(100)	= NULL,
@site_name						nvarchar(300)	= NULL,
@site_url						nvarchar(300)	= NULL,
@front_url						nvarchar(1000)	= NULL,
@email							nvarchar(100)	= NULL,
@mobile_no						nvarchar(50)	= NULL,
@sms_api						nvarchar(1000)	= NULL,
@sms_send_status				nchar(10)		= NULL,
@meta_title						nvarchar(100)	= NULL,
@meta_keywords					nvarchar(1000)	= NULL,
@meta_description				nvarchar(255)	= NULL,
@google_analytics_code			nvarchar(1000)	= NULL,
@html_meta_code					nvarchar(100)	= NULL,
@footer_copyright				nvarchar(100)	= NULL,
@facebook_url					nvarchar(100)	= NULL,
@twitter_url					nvarchar(100)	= NULL,
@youtube_url					nvarchar(100)	= NULL,
@google_plus_url				nvarchar(100)	= NULL,
@pinterest_url					nvarchar(100)	= NULL,
@linkedin_url					nvarchar(100)	= NULL,
@instagram_url					nvarchar(100)	= NULL,
@facebook_app_id				nvarchar(100)	= NULL,
@facebook_app_secret			nvarchar(100)	= NULL,
@google_secret_key				nvarchar(100)	= NULL,
@google_developer_key			nvarchar(100)	= NULL

)
AS 

BEGIN 

	INSERT INTO [dbo].[Company]
		([company_id]
		,[logo]
		,[fevicon]
		,[admin_name]
		,[site_name]
		,[site_url]
		,[front_url]
		,[email]
		,[mobile_no]
		,[sms_api]
		,[sms_send_status]
		,[meta_title]
		,[meta_keywords]
		,[meta_description]
		,[google_analytics_code]
		,[html_meta_code]
		,[footer_copyright]
		,[facebook_url]
		,[twitter_url]
		,[youtube_url]
		,[google_plus_url]
		,[pinterest_url]
		,[linkedin_url]
		,[instagram_url]
		,[facebook_app_id]
		,[facebook_app_secret]
		,[google_secret_key]
		,[google_developer_key])

	SELECT 
		NEXT VALUE FOR [dbo].[company_id],
		@logo							,
		@fevicon						,
		@admin_name						,
		@site_name						,
		@site_url						,
		@front_url						,
		@email							,
		@mobile_no						,
		@sms_api						,
		@sms_send_status				,
		@meta_title						,
		@meta_keywords					,
		@meta_description				,
		@google_analytics_code			,
		@html_meta_code					,
		@footer_copyright				,
		@facebook_url					,
		@twitter_url					,
		@youtube_url					,
		@google_plus_url				,
		@pinterest_url					,
		@linkedin_url					,
		@instagram_url					,
		@facebook_app_id				,
		@facebook_app_secret			,
		@google_secret_key				,
		@google_developer_key			

END

GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_CONTACT_FOLLOWUP_ACTION]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_CONTACT_FOLLOWUP_ACTION]
( @followup_action_desc	NVARCHAR(250)	= NULL) 
AS 
BEGIN 
INSERT INTO [dbo].[Contact_Followup_Action]
    ([followup_action_code]
    ,[followup_action_desc])
SELECT 
	NEXT VALUE FOR dbo.followup_action_code,
	@followup_action_desc

END

GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_DEPARTMENT_EMPLOYEES]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_DEPARTMENT_EMPLOYEES]
(
     @department_id				INT=NULL
    ,@employee_id				INT=NULL
    ,@from_date					DATE=NULL
    ,@to_date					DATE=NULL
)
AS
BEGIN
INSERT INTO [dbo].[Department_Employees]
         (
		    [department_id]
           ,[employee_id]
           ,[from_date]
           ,[to_date]
		 )
     VALUES
         (
		    @department_id
           ,@employee_id
           ,@from_date
           ,@to_date
		 )

UPDATE dbo.Employees  SET [department_id] = @department_id
	WHERE [employee_id] = @employee_id

END

--department_id	department_name
--1	Administration
--2	  Health &  Recreation
--3	  Sanitation & Maintenance
--4	  Maintenance
--5	  Sanitation
--6	  Mess
--7	  Managing Committee
--8	  Hostel Committee
--9	  Mess Committee
--10	  Student(s) Caretaker(s)
--11	  Legal
--12	  Accounts
--13	  Billing
--14	  General
--15	  Complaints & Grievances
--16	  Inventory
--17	  Sales
--18	  Purchases
--19	  House Keeping
--20	  Food & Bewarages
--21	  Stores
--22	  Services
--23	  Auditing
--24	  Human Resource(HR)

--[dbo].[UDP_INSERT_DEPARTMENT_EMPLOYEES]
--     @department_id	= 8,
--    @employee_id	= 102,
--    @from_date	= '2018-03-23',
--    @to_date		=NULL



GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_EMPLOYEE_QUALIFICATIONS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDP_INSERT_EMPLOYEE_QUALIFICATIONS]
(
	@employee_id			INT= NULL,
	@qualification_code		NVARCHAR(15)= NULL,
	@date_obtained			DATE= NULL
)
AS
BEGIN
    INSERT INTO [dbo].[Employee_Qualifications]
    (
      [employee_id]
     ,[qualification_code]
     ,[date_obtained]
    )
    VALUES
    (
       @employee_id
      ,@qualification_code
      ,@date_obtained
    )

UPDATE dbo.Employees  SET [qualification_code] = @qualification_code
	WHERE [employee_id] =  @employee_id

END 


--qualification_code	qualification_title
--1              	business studies
--2              	health and safety
--3              	hospitality management
--4              	hospitality and tourism management
--5              	hotel and catering
--6              	human resources
--7              	management

--[dbo].[UDP_INSERT_EMPLOYEE_QUALIFICATIONS] 
--      @employee_id = 102
--     ,@qualification_code = '6'
--     ,@date_obtained = '2012-03-23'
GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_EMPLOYEE_TITLES]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_EMPLOYEE_TITLES]
(
	@employee_id			INT= NULL,
	@job_title_code			NVARCHAR(15)= NULL,
	@date_from				DATE= NULL,
	@date_to				DATE= NULL
)
AS
BEGIN
     INSERT INTO [dbo].[Employee_Titles]
      (
	    [employee_id]
       ,[job_title_code]
       ,[date_from]
       ,[date_to]
	  )
     VALUES
      (
		 @employee_id
        ,@job_title_code
        ,@date_from
        ,@date_to
      )

UPDATE dbo.Employees  SET [job_title_code] = @job_title_code,
						[updated_date] = GETDATE()
	WHERE [employee_id] = @employee_id
	

END

--[dbo].[UDP_INSERT_EMPLOYEE_TITLES]
--	@employee_id			= 102,
--	@job_title_code			='15',
--	@date_from				='2018-03-23',
--	@date_to				=NULL

--job_title_code	job_title_desc
--1              	Clerk
--10             	Chef
--100            	Board of Directors (BOD)
--11             	Cook
--12             	Food and Beverage Manager
--13             	Kitchen Manager
--14             	Pastry Chef
--15             	Property Manager
--16             	General Manager
--17             	Sales and Marketing Manager
--18             	Shift Manager
--19             	House Keeper
--2              	Receptionist
--20             	Maid
--200            	CEO/CBO
--21             	Maintenance Worker
--22             	Maintenance Supervisor
--23             	Director of Operations
--24             	Director of Maintenance
--25             	Driver
--26             	Parking Lot Attendant
--27             	Cluster Manager
--3              	Relations Manager
--300            	Country Head (Operations)
--4              	Services Associate
--400            	Cluster Head (Operations)
--5              	Services Supervisor
--500            	Sr. Manager Operations
--6              	Reservation Agent
--600            	Warden / Caretaker
--7              	Attendant
--8              	Cafe Manager
--9              	Catering Manager

GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_EMPLOYEES]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_EMPLOYEES]
(
	@employee_details		NVARCHAR(250)= NULL,
	@nick_name				NVARCHAR(50)= NULL,
	@first_name				NVARCHAR(50)= NULL,
	@middle_name			NVARCHAR(50)= NULL,
	@last_name				NVARCHAR(50)= NULL,
	@gender					NVARCHAR(10)= NULL,
	@date_of_birth			DATE= NULL,
	@date_joined			DATE= NULL,
	@date_left				DATE= NULL,
	@employee_Addresses		VARCHAR(MAX)=NULL

)
AS
DECLARE 
@employee_id						INT,
@RowCnt								INT,
@Cnt								INT 

BEGIN

	SET @employee_id = NEXT VALUE FOR  dbo.employee_id
	INSERT INTO dbo.Employees 
	(
	   employee_id
      ,employee_details
      ,nick_name
      ,first_name
      ,middle_name
      ,last_name
      ,gender
      ,date_of_birth
      ,date_joined
      ,date_left
	)
	VALUES
	(
		 @employee_id
		,@employee_details
		,@nick_name
		,@first_name
		,@middle_name
		,@last_name
		,@gender
		,@date_of_birth
		,@date_joined
		,@date_left
	)

	DECLARE @l_employee_Addresses TABLE ( ID INT IDENTITY(1,1), employee_Addresses NVARCHAR(MAX))
	DECLARE @employee_Addresses_Data TABLE	
		(	
			[ID] [int] IDENTITY(1,1) NOT NULL,
			[address_type_code] [nchar](15) NOT NULL,
			[address] [nvarchar](1000) NOT NULL,
			[city] [nvarchar](50) NOT NULL,
			[state] [nvarchar](50) NOT NULL,
			[country] [nvarchar](50) NOT NULL,
			[pincode] [nvarchar](50) NOT NULL,
			[email] [nvarchar](100) NOT NULL,
			[mobile_no] [nvarchar](50) NOT NULL,
			[other_address_details] [nvarchar](250) NULL,
			[date_address_from] [datetime] NULL,
			[date_address_to] [datetime] NULL,
			[employee_name] [nvarchar](200) NULL
		)

	INSERT INTO @l_employee_Addresses SELECT * FROM DBO.USP_SplitString(@employee_Addresses,';')
	;WITH Split_employee_Addresses(ID,employee_Addresses,xmlname)
		AS ( SELECT ID, employee_Addresses, CONVERT(XML,'<Names><name>'  
						+ REPLACE(REPLACE(employee_Addresses,',', '</name><name>') ,'&','&amp;') + '</name></Names>') AS xmlname
				FROM @l_employee_Addresses )

	INSERT INTO @employee_Addresses_Data
	SELECT      
			--xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')		AS [property_owner_id],    
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[1]','NVARCHAR(100)')))	AS [address_type_code],
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[2]','NVARCHAR(200)')))	AS [address],
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[3]','NVARCHAR(50)')))		AS [city],  
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[4]','NVARCHAR(50)')))		AS [state],
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[5]','NVARCHAR(50)')))		AS [country], 
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[6]','NVARCHAR(50)')))		AS [pincode],
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[7]','NVARCHAR(50)')))		AS [email],  
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[8]','NVARCHAR(50)')))		AS [mobile_no],
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[9]','NVARCHAR(2000)')))	AS [other_address_details],
			CAST(xmlname.value('/Names[1]/name[10]','NVARCHAR(50)') AS DATE)	AS [date_address_from],	
			CAST(xmlname.value('/Names[1]/name[11]','NVARCHAR(50)') AS DATE)	AS [date_address_to], 	
			LTRIM(RTRIM(xmlname.value('/Names[1]/name[12]','NVARCHAR(50)')))	AS [employee_name] 	
		FROM Split_employee_Addresses

	SELECT @RowCnt = COUNT(1) FROM @employee_Addresses_Data
	SET @Cnt = 1

	WHILE @Cnt <= @RowCnt
		BEGIN
			DECLARE @P_Address_ID INT
			SELECT @P_Address_ID = NEXT VALUE FOR [dbo].[Address_ID]
			
			INSERT INTO [dbo].[Addresses]
				([address_id]
				,[address]
				,[city]
				,[state]
				,[country]
				,[pincode]
				,[email]
				,[mobile_no]
				,[other_address_details])
			SELECT @P_Address_ID
				,PD.[address]
				,PD.[city]
				,PD.[state]
				,PD.[country]
				,PD.[pincode]
				,PD.[email]
				,PD.[mobile_no]
				,PD.[other_address_details]
			FROM @employee_Addresses_Data PD WHERE ID = @Cnt

			INSERT INTO [dbo].[Employee_Addresses]
						([employee_address_id]
						,[address_id]
						,[address_type_code]
						,[employee_id]
						,[date_address_from]
						,[date_address_to])
			SELECT NEXT VALUE FOR [dbo].[Employee_Address_ID]
					,@P_Address_ID
					,PD.[address_type_code]
					,@employee_id
					,PD.[date_address_from]
					,PD.[date_address_to]
			FROM @employee_Addresses_Data PD WHERE ID = @Cnt
			SET @Cnt = @Cnt + 1;
		END;
		
	SET @RowCnt = 0
	SET @Cnt = 0	
END

	--[dbo].[UDP_INSERT_EMPLOYEES]
	--@employee_details= 'Having previous experience in this domain, worked 2 years on the similar position',
	--@nick_name	= 'Sai Raj',
	--@first_name = 'Venkata',
	--@middle_name = 'Raju',
	--@last_name = 'Sai',
	--@gender= 'Male',
	--@date_of_birth = '1990-10-23',
	--@date_joined = '2018-02-23',
	--@date_left	= NULL,
	--@employee_Addresses ='HOME,PLOT NO 284 STREET NO 8 MAHADIPATNAM,HYDERABAD,TELANGANA,INDIA,500034,sairaj@gamil.com,998989898,none,2000-10-23,,Venkata SaiRaj'
GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_INVENTORY_ITEMS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_INVENTORY_ITEMS] 
(
	@item_code				NVARCHAR(20)	= NULL,
	@brand_id				INT				= NULL,
	@item_category_code		NVARCHAR(20)	= NULL,
	@item_description		NVARCHAR(200)	= NULL,
	@average_monthly_usage	NVARCHAR(20)	= NULL,
	@reorder_level			NVARCHAR(20)	= NULL,
	@reorder_quantity		NVARCHAR(20)	= NULL,

--item_id int
	@manufacturer_name		NVARCHAR(200)	= NULL,
	@mfg_date				DATE			= NULL,
	@item_expiry_date		DATE			= NULL,
	@item_age				INT				= NULL,
	@warranty				NVARCHAR(100)	= NULL,
	@amc_cost				DECIMAL(15,2)	= NULL
)
AS 
BEGIN 
	INSERT INTO [dbo].[Inventory_Items]
		([item_code]
		,[brand_id]
		,[item_category_code]
		,[item_description]
		,[average_monthly_usage]
		,[reorder_level]
		,[reorder_quantity])
	SELECT
		@item_code				,
		@brand_id				,
		@item_category_code		,
		@item_description		,
		@average_monthly_usage	,
		@reorder_level			,
		@reorder_quantity		


	INSERT INTO [dbo].[Inventory_Item_Details]
		([item_id]
		,[manufacturer_name]
		,[mfg_date]
		,[item_expiry_date]
		,[item_age]
		,[warranty]
		,[amc_cost])
	SELECT 
		@item_code				,
		@manufacturer_name		,
		@mfg_date				,
		@item_expiry_date		,
		@item_age				,
		@warranty				,
		@amc_cost				
END 




GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_ITEM_SUPPLIERS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_ITEM_SUPPLIERS]  
(
	@Item_id								INT				= NULL,
	@supplier_code							NCHAR(15)		= NULL,
	@value_supplied_to_date					INT				= NULL,
	@total_quantity_supplied_to_date		INT				= NULL,
	@first_item_supplied_date				DATE			= NULL,
	@last_item_supplied_date				DATE			= NULL,
	@delivery_lead_time						NVARCHAR(20)	= NULL,
	@standard_price							DECIMAL(15,2)	= NULL,
	@percenage_discount						DECIMAL(15,2)	= NULL,
	@minimum_order_quantity					INT				= NULL,
	@maximum_order_quantity					INT				= NULL
)
AS 
BEGIN 
	INSERT INTO [dbo].[Item_Suppliers]
		([Item_id]
		,[supplier_code]
		,[value_supplied_to_date]
		,[total_quantity_supplied_to_date]
		,[first_item_supplied_date]
		,[last_item_supplied_date]
		,[delivery_lead_time]
		,[standard_price]
		,[percenage_discount]
		,[minimum_order_quantity]
		,[maximum_order_quantity])
	
	SELECT
		@Item_id								,
		@supplier_code							,
		@value_supplied_to_date					,
		@total_quantity_supplied_to_date		,
		@first_item_supplied_date				,
		@last_item_supplied_date				,
		@delivery_lead_time						,
		@standard_price							,
		@percenage_discount						,
		@minimum_order_quantity					,
		@maximum_order_quantity	

END 



GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_PROPERTIES_READINESS_FOR_PROCURE_CHECKLIST_ITEMS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_PROPERTIES_READINESS_FOR_PROCURE_CHECKLIST_ITEMS]
(
@property_id								int = NULL,
@readiness_for_procure_checklist_item_id	int	= NULL,
@applicable_yn								bit	= 0,
@available_yn								bit	= 0,
@completed_yn								bit	= 0
) 
AS 
BEGIN 

INSERT INTO [dbo].[Properties_Readiness_for_Procure_Checklist_Items]
    ([property_id]
    ,[readiness_for_procure_checklist_item_id]
    ,[applicable_yn]
    ,[available_yn]
    ,[completed_yn])
SELECT 
	@property_id								,
	@readiness_for_procure_checklist_item_id	,
	@applicable_yn								,
	@available_yn								,
	@completed_yn								

END 




GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_PROPERTY_CANTEEN]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_PROPERTY_CANTEEN]
(
	@property_id				INT				=	NULL,
	@canteen_name				NVARCHAR(200)	=	NULL,
	@seating_capacity			INT				=	NULL,
	@canteen_start_time			TIME(7)			=	NULL,
	@canteen_end_time			TIME(7)			=	NULL,
	@canteen_incharge			INT				=	NULL,
	@contact_number				NVARCHAR(20)	=	NULL,
	@cell_phone					NVARCHAR(20)	=	NULL

)

AS 

BEGIN 

	INSERT INTO [dbo].[Property_Canteen]
		([property_id]
		,[canteen_name]
		,[seating_capacity]
		,[canteen_start_time]
		,[canteen_end_time]
		,[canteen_incharge]
		,[contact_number]
		,[cell_phone])

	SELECT
		@property_id				,
		@canteen_name				,
		@seating_capacity			,
		@canteen_start_time			,
		@canteen_end_time			,
		@canteen_incharge			,
		@contact_number				,
		@cell_phone					

END
GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_PROPERTY_CANTEEN_MENU]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_PROPERTY_CANTEEN_MENU]
(
	@property_id			INT					=NULL,
	@food_menu_id			INT					=NULL,
	@week_day				NCHAR(15)			=NULL,
	@break_fast				NVARCHAR(2000)		=NULL,
	@lunch					NVARCHAR(2000)		=NULL,
	@dinner					NVARCHAR(2000)		=NULL,
	@active_yn				BIT					=NULL

) AS 

BEGIN 

	INSERT INTO [dbo].[Property_Canteen_Menu]
		([property_id]
		,[food_menu_id]
		,[week_day]
		,[break_fast]
		,[lunch]
		,[dinner]
		,[active_yn])

	SELECT
		@property_id			,
		@food_menu_id			,
		@week_day				,
		@break_fast				,
		@lunch					,
		@dinner					,
		@active_yn				
							

END 
GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_PROPERTY_EMPLOYEES]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDP_INSERT_PROPERTY_EMPLOYEES]
(    
    @employee_id			INT=NULL,
	@property_id			INT=NULL,
	@loggedin_employee_id	INT=NULL,
	@from_period			DATE = NULL,
	@to_period				DATE = NULL
)
AS
BEGIN
INSERT INTO [dbo].[Property_Employees]
           ([employee_id]
           ,[property_id]
           ,[active_yn]
           ,[assigned_date]
           ,[assigned_by]
           ,[from_period]
           ,[to_period])
     VALUES
         (
			@employee_id ,
			@property_id ,
			1,
			GETDATE(),
			@loggedin_employee_id ,
			@from_period ,
			@to_period        
		 )

END




GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_PROPERTY_INVENTORY]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_PROPERTY_INVENTORY] 
(
	@property_id				INT			= NULL,
	@item_ID					INT			= NULL,
	@item_tagged_by				INT			= NULL,
	@item_tagged_date			DATE		= NULL,
	@item_tagged_level_code		NCHAR(20)	= NULL,
	@item_tagged_to				INT			= NULL,
	@active_yn					BIT			= 1
) AS 
BEGIN 
	INSERT INTO [dbo].[Property_Inventory]
		([property_id]
		,[item_ID]
		,[item_tagged_by]
		,[item_tagged_date]
		,[item_tagged_level_code]
		,[item_tagged_to]
		,[active_yn])
	SELECT 
		@property_id				,
		@item_ID					,
		@item_tagged_by				,
		@item_tagged_date			,
		@item_tagged_level_code		,
		@item_tagged_to				,
		@active_yn					

END




GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_READINESS_FOR_PROCURE_CHECKLIST_ITEMS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_READINESS_FOR_PROCURE_CHECKLIST_ITEMS]
(
@readiness_for_procure_category_code			nchar(15)		= NULL, 
@readiness_for_procure_checklist_item_name		nvarchar(250)	= NULL
) 
AS 
BEGIN 

	INSERT INTO [dbo].[Readiness_for_Procure_Checklist_Items]
		([readiness_for_procure_checklist_item_id]
		,[readiness_for_procure_category_code]
		,[readiness_for_procure_checklist_item_name])

	SELECT
		NEXT VALUE FOR dbo.readiness_for_procure_checklist_item_id,
		@readiness_for_procure_category_code			, 
		@readiness_for_procure_checklist_item_name		
END




GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_READINESS_FOR_PROCURE_PERCENT]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_READINESS_FOR_PROCURE_PERCENT]
(
	@property_id							INT				= NULL,
	@readiness_for_procure_percent_details	NVARCHAR(250)	= NULL
) 
AS 
--DECLARE @readiness_for_procure_percent_id		INT  = NEXT VALUE FOR [dbo].[readiness_for_procure_percent_id]
BEGIN 
	INSERT INTO [dbo].[Readiness_for_Procure_Percent]
		([readiness_for_procure_percent_id]
		,[property_id]
		,[readiness_for_procure_percent_details])
	SELECT 
		NEXT VALUE FOR [dbo].[readiness_for_procure_percent_id]	,
		@property_id											,
		@readiness_for_procure_percent_details			
END 




GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_STANDARD_DOCUMENT_TYPES]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_STANDARD_DOCUMENT_TYPES]
(
--@document_type_code			nchar(15)		= NULL,
@document_type_name			nvarchar(150)	= NULL,
@document_type_desc			nvarchar(250)	= NULL
)
AS 
BEGIN 

	INSERT INTO [dbo].[Standard_Document_Types]
		([document_type_code]
		,[document_type_name]
		,[document_type_desc])
	SELECT 
		NEXT VALUE FOR [dbo].document_type_code,
		@document_type_name			,
		@document_type_desc			

END 


GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_STANDARD_DOCUMENTS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_STANDARD_DOCUMENTS]
(
	@document_type_code		NCHAR(15)		= NULL,
	@data_profile_id		INT				= NULL,
	@document_details		VARCHAR(8000)	= NULL
)
AS 
BEGIN 
	INSERT INTO [dbo].[Standard_Documents]
		([document_id]
		,[document_type_code]
		,[data_profile_id]
		,[document_details])
	SELECT
		NEXT VALUE FOR [dbo].[document_id]	,
		@document_type_code					,
		@data_profile_id					,
		@document_details		
END 




GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_STUDENT_DETAILS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_STUDENT_DETAILS]
@Student_Details					VARCHAR(MAX)=NULL,
@Student_Addresses					VARCHAR(MAX)=NULL,
@Student_Education					VARCHAR(MAX)=NULL,
@Student_Relationships				VARCHAR(MAX)=NULL,
@Student_Departments				VARCHAR(MAX)=NULL,
@Student_PaymentMethods				VARCHAR(MAX)=NULL
AS 
BEGIN

SELECT 1 


SET  @Student_Details = 'FIRST-BLOCK : PROPERTY 01,BLOCK-1:FLOOR-1,10;FIRST-BLOCK : PROPERTY 01,BLOCK-1:FLOOR-2,20;SECOND BLOCK : PROPERTY 01,BLOCK-2:FLOOR-1,10;SECOND BLOCK : PROPERTY 01,BLOCK-2:FLOOR-2,15;SECOND BLOCK : PROPERTY 01,BLOCK-2:FLOOR-3,15' 

	--'Rajesh,Rajesh,Sharma,V,
           --(<student_id, int,>
           --,<password, nvarchar(50),>
           --,<firstname, nvarchar(50),>
           --,<middlename, nvarchar(50),>
           --,<dateofbirth, date,>
           --,<gender, nchar(2),>
           --,<place_of_birth, nvarchar(50),>
           --,<hometown, nvarchar(50),>
           --,<nationality, nvarchar(50),>
           --,<mobilenumber, nvarchar(20),>
           --,<ssn, nvarchar(50),>
           --,<picture, nvarchar(400),>
           --,<Isundergrad, bit,>
           --,<EnrollDate, date,>)

--INSERT INTO [dbo].[Students]
--    ([student_id]
--    ,[password]
--    ,[firstname]
--    ,[middlename]
--    ,[dateofbirth]
--    ,[gender]
--    ,[place_of_birth]
--    ,[hometown]
--    ,[nationality]
--    ,[mobilenumber]
--    ,[ssn]
--    ,[picture]
--    ,[Isundergrad]
--    ,[EnrollDate])
--     VALUES
           --(<student_id, int,>
           --,<password, nvarchar(50),>
           --,<firstname, nvarchar(50),>
           --,<middlename, nvarchar(50),>
           --,<dateofbirth, date,>
           --,<gender, nchar(2),>
           --,<place_of_birth, nvarchar(50),>
           --,<hometown, nvarchar(50),>
           --,<nationality, nvarchar(50),>
           --,<mobilenumber, nvarchar(20),>
           --,<ssn, nvarchar(50),>
           --,<picture, nvarchar(400),>
           --,<Isundergrad, bit,>
           --,<EnrollDate, datetime,>)
END 

GO
/****** Object:  StoredProcedure [dbo].[UDP_INSERT_SUPPLIERS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_INSERT_SUPPLIERS] 
(
    @supplier_name				NVARCHAR(50) = NULL,
    @supplier_email				NVARCHAR(50) = NULL,
    @supplier_phone				NVARCHAR(50) = NULL,
    @supplier_cellphone			NVARCHAR(50) = NULL,

	@address_type_code			NCHAR(15)	   = NULL,
	@address					NVARCHAR(1000) = NULL,
	@city						NVARCHAR(50)   = NULL,
	@state						NVARCHAR(50)   = NULL,
	@country					NVARCHAR(50)   = NULL,
	@pincode					NVARCHAR(50)   = NULL,
	@other_address_details		NVARCHAR(250)  = NULL,
	@date_address_from			DATE		   = NULL,
	@date_address_to			DATE		   = NULL

) AS 

    DECLARE @supplier_code				NCHAR(15)	 = NEXT VALUE FOR [dbo].[Supplier_ID]
    DECLARE @address_id					INT			 = NEXT VALUE FOR [dbo].[Address_ID]

BEGIN 
	-- Supplier Information
	INSERT INTO [dbo].[Suppliers]
		([supplier_code]
		,[supplier_name]
		,[supplier_email]
		,[supplier_phone]
		,[supplier_cellphone])
	SELECT     
		@supplier_code,
		@supplier_name,
		@supplier_email,
		@supplier_phone,
		@supplier_cellphone

	-- Supplier Address Information
	INSERT INTO [dbo].[Addresses]
		([address_id]
		,[address]
		,[city]
		,[state]
		,[country]
		,[pincode]
		,[email]
		,[mobile_no]
		,[other_address_details])
	SELECT 
		@address_id,
		@address,
		@city,
		@state,
		@country,
		@pincode,
		@supplier_email,
		@supplier_cellphone,
		@other_address_details

	-- Supplier Address Information
	INSERT INTO [dbo].[Supplier_Addresses]
		([supplier_code]
		,[address_id]
		,[address_type_code]
		,[date_address_from]
		,[date_address_to])
	SELECT 
		@supplier_code,
		@address_id,
		@address_type_code,
		@date_address_from,
		@date_address_to

END 


GO
/****** Object:  StoredProcedure [dbo].[UDP_P_INSERT_PROCURE_CHECK_ITEMS]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_P_INSERT_PROCURE_CHECK_ITEMS]
(
	@Property_ID INT = NULL
)
AS 
BEGIN 

/* This procedure will insert standard Property Procure List against a property once 
the property is get approved for onboard from country head.

The respective ground teams have to reach out to the property owners to collect the checklist documents and provide respective 
inputs to the application.

*/


INSERT INTO [dbo].[Properties_Readiness_for_Procure_Checklist_Items]
    ([property_id]
    ,[readiness_for_procure_checklist_item_id]
    ,[applicable_yn]
    ,[available_yn]
    ,[completed_yn])

SELECT @Property_ID,
	   PCI.readiness_for_procure_checklist_item_id,
	   1 [applicable_yn],
	   0 [available_yn],
	   0 [completed_yn]
	  --,PC.[readiness_for_procure_category_code]
   --   ,[readiness_for_procure_category_name]
   --   ,[readiness_for_procure_checklist_item_name]
 FROM [dbo].[Readiness_for_Procure_Categories] PC
    INNER JOIN [dbo].[Readiness_for_Procure_Checklist_Items] PCI
        ON PC.[readiness_for_procure_category_code] = PCI.[readiness_for_procure_category_code]
ORDER BY PC.[readiness_for_procure_category_code]

END 

GO
/****** Object:  StoredProcedure [dbo].[UDP_UPDATE_PR_HK_Checklist_Status]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_UPDATE_PR_HK_Checklist_Status] 
(
	@property_id								INT				= NULL,
	@property_hk_check_list_id					INT				= NULL,
	@category_item_id							int				= NULL,
	@applicable_yn								BIT				= NULL,
	@available_yn								BIT				= NULL,
	@completed_yn								BIT				= NULL,
	@employee_id								INT				= NULL,
	@date_of_submission							DATE			= NULL,
	@remarks									NVARCHAR(1000)	= NULL,
	@updated_by									INT				= NULL,
	@updated_on									DATE			= NULL
) AS 
BEGIN 

INSERT INTO [dbo].[PR_HK_Checklist_Status]
           ([property_id]
           ,[property_hk_check_list_id]
           ,[category_item_id]
           ,[applicable_yn]
           ,[available_yn]
           ,[completed_yn]
           ,[employee_id]
           ,[date_of_submission]
           ,[remarks]
           ,[updated_by]
           ,[updated_on])
	SELECT 
		@property_id								,
		@property_hk_check_list_id					,
		@category_item_id							,
		@applicable_yn								,
		@available_yn								,
		@completed_yn								,
		@employee_id								,
		@date_of_submission							,
		@remarks									,
		@updated_by									,
		@updated_on									

END 

GO
/****** Object:  StoredProcedure [dbo].[UDP_UPDATE_PR_Procure_Checklist_Status]    Script Date: 10/23/2018 5:23:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDP_UPDATE_PR_Procure_Checklist_Status] 
(
	@property_id								INT				= NULL,
	@readiness_for_procure_checklist_item_id	INT				= NULL,
	@applicable_yn								BIT				= NULL,
	@available_yn								BIT				= NULL,
	@completed_yn								BIT				= NULL,
	@employee_id								INT				= NULL,
	@date_of_visit								DATE			= NULL,
	@remarks									NVARCHAR(1000)	= NULL,
	@updated_by									INT				= NULL,
	@updated_on									DATE			= NULL
) AS 
BEGIN 

	INSERT INTO [dbo].[PR_Procure_Checklist_Status]
		([property_id]
		,[readiness_for_procure_checklist_item_id]
		,[applicable_yn]
		,[available_yn]
		,[completed_yn]
		,[employee_id]
		,[date_of_visit]
		,[remarks]
		,[updated_by]
		,[updated_on])
	SELECT 
		@property_id								,
		@readiness_for_procure_checklist_item_id	,
		@applicable_yn								,
		@available_yn								,
		@completed_yn								,
		@employee_id								,
		@date_of_visit								,
		@remarks									,
		@updated_by									,
		@updated_on									

END 

GO
USE [master]
GO
ALTER DATABASE [Placio_Dev] SET  READ_WRITE 
GO
